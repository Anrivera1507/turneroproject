import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:provider/provider.dart';
import 'package:turnero_flutter_app/src/providers/veterano_provider.dart';
import 'package:turnero_flutter_app/src/routes.dart';
import 'package:turnero_flutter_app/src/services/preferencias_usuario.dart';
import 'package:turnero_flutter_app/src/providers/socket_service.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final prefs = PreferenciasUsuario();
  await prefs.initPrefs();
  await FlutterDownloader.initialize(
      debug: (kDebugMode) ? true : false, ignoreSsl: true);
  PackageInfo packageInfo = await PackageInfo.fromPlatform();
  prefs.versionAndroid = packageInfo.version;
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  final pref = PreferenciasUsuario();
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => VeteranoProvider()),
        ChangeNotifierProvider(create: (_) => SocketService()),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Material App',
        initialRoute: pref.ultimaPagina,
        routes: getApplicationRoutes(),
        theme: ThemeData(primaryColor: Color.fromRGBO(17, 29, 78, 1)),
      ),
    );
  }
}
