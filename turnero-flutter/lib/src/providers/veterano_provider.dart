import 'dart:convert';
import 'dart:io';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_login/flutter_login.dart';
import 'package:turnero_flutter_app/src/models/areas_atencion_model.dart';
import 'package:turnero_flutter_app/src/models/citas_model.dart';
import 'package:turnero_flutter_app/src/models/permisos_model.dart';
import 'package:turnero_flutter_app/src/models/requisitos_ticket_model.dart';
import 'package:turnero_flutter_app/src/models/ticket_model.dart';
import 'package:turnero_flutter_app/src/models/ticket_response_model.dart';
import 'package:turnero_flutter_app/src/models/usuario_model.dart';
import 'package:turnero_flutter_app/src/models/usuario_response_model.dart';
import 'package:turnero_flutter_app/src/models/veterano_model.dart';
import 'package:turnero_flutter_app/src/services/preferencias_usuario.dart';
import 'package:turnero_flutter_app/src/services/turnero_service.dart';
import 'package:path/path.dart' as path;
import 'package:path_provider/path_provider.dart' as path_provider;

class VeteranoProvider with ChangeNotifier {


  int cuentaImpresion = 1;
  String? pathImg = "";
  String? nombreVetranoPathImg = "";
  VeteranoService _veteranoService = new VeteranoService();
  VeteranoModel veterano = new VeteranoModel();
  AreasAtencionModel areasAtencionModel = new AreasAtencionModel();
  TicketResponseModel ticketResponseModel = TicketResponseModel();
  UsuarioResponseModel usuarioResponseModel = new UsuarioResponseModel();
  List<DatumRequisitosTicketCheckBox?> listRequisitosTicket = [];
  List<DataCitas?>? citas=[];
  List<DatumPermisos?>? permisos;

  VeteranoProvider(){
    this.obtenerPermisos();
    //this.obtenerRenderImagen();
    
  }

  //variable para identificar si el ticket es de prioridad
  bool esPrioridad = false;

  cambiarPrioridadTicket(bool estado) {
    esPrioridad = estado;
    notifyListeners();
  }

  Future<void> buscarVeterano(String query) async {
    try {
      veterano = await _veteranoService.buscarVeterano(query);
      notifyListeners();
    } catch (e) {
      veterano = VeteranoModel();
    }
  }

  Future<void> obtenerAreas() async {
    try {
      areasAtencionModel = await _veteranoService.obtenerAreas();
      notifyListeners();
    } catch (e) {
      areasAtencionModel = AreasAtencionModel();
    }
  }

  Future<TicketResponseModel> crearTicket(int idArea,
      {bool prioridad = false}) async {
    final pref = PreferenciasUsuario();
    try {
      TicketModel ticketModel = new TicketModel(
          activo: true,
          creadoPor: pref.idUsuario,
          modificadoPor: "-1",
          prioridad: esPrioridad,
          id: "-1",
          idVeterano: (veterano.data != null) ? veterano.data![0].id : "-1",
          idArea: idArea.toString(),
          idSucursal: pref.idSucursal);
      if (veterano.data != null && cuentaImpresion == 1) {
        cuentaImpresion = 1;
        ticketResponseModel = await _veteranoService.crearTicket(ticketModel);
        return ticketResponseModel;
      } else if (veterano.data == null && ticketModel.idVeterano == '-1') {
        ticketResponseModel = await _veteranoService.crearTicket(ticketModel);
        return ticketResponseModel;
      } else {
        return ticketResponseModel;
      }
    } catch (e) {
      return TicketResponseModel();
    }
  }

  Future<String?> downloadImg(String texto, {String fontSize = '75'}) async {
    final resp = await _veteranoService.downloadImg(texto, fontSize: fontSize);
    if (resp.statusCode != 404) {
      final imageName = Random().nextInt(100).toString() +
          path.basename(
              // "https://res.cloudinary.com/syscoredevs/image/upload/l_text:Arial_"+fontSize+"_bold:" +
              //     texto +
              //     "/v1650470029/Inabve/img-white_df6b96.png"
              "https://res.cloudinary.com/syscoredevs/image/upload/l_text:Arial_" +
                  fontSize +
                  "_bold:" +
                  texto +
                  "/v1654802910/Inabve/img-white_jxqftr.png"
              );
      final appDir = await path_provider.getApplicationDocumentsDirectory();
      final localPath = path.join(appDir.path, imageName);
      final imageFile = File(localPath);
      await imageFile.writeAsBytes(resp.bodyBytes);

      // pathImg = localPath;
      return localPath;
    }
    return '';
  }

  Future<String?> decodificarImagen(String base64)async{
    try {
      final decodeBytes=base64Decode(base64);
      final appDir = await path_provider.getApplicationDocumentsDirectory();
      final imageName=Random().nextInt(100).toString()+path.basename("img");
      final localPath = path.join(appDir.path, imageName); 
      final imageFile = File(localPath);
      await imageFile.writeAsBytes(decodeBytes);
      return localPath;
    } catch (e) {
        print(e);
        return null;
    }
  }

  Future<void> writeToFile(ByteData data, String path) {
    final buffer = data.buffer;
    return new File(path).writeAsBytes(
        buffer.asUint8List(data.offsetInBytes, data.lengthInBytes));
  }

  Future<String?> login(LoginData loginData) async {
    final pref = PreferenciasUsuario();
    UsuarioModel usuarioModel =
        new UsuarioModel(usuario: loginData.name, clave: loginData.password);
    //final resp = await _veteranoService.login(usuarioModel);
    usuarioResponseModel = await _veteranoService.login(usuarioModel);

    //notifyListeners();
    if (usuarioResponseModel.data?.usuario != loginData.name ||
        usuarioResponseModel.data == null) {
      return 'El usuario no existe';
    } else {
      pref.usuario = usuarioResponseModel.data!.usuario!;
      pref.idUsuario = usuarioResponseModel.data!.id!;
      pref.token = usuarioResponseModel.token.toString();
      pref.idSucursal=usuarioResponseModel.data!.idSucursal.toString();
    }
    return null;
  }

  Future<List<DatumRequisitosTicketCheckBox?>> obtenerRequisitosTicket(
      String idArea) async {
    List<DatumRequisitosTicketCheckBox?> datum = [];
    final reqTicket = await _veteranoService.obtenerRequisitosTicket(idArea);
    if (reqTicket.isNotEmpty) {
      //listRequisitosTicket=reqTicket;
      for (var item in reqTicket) {
        datum.add(DatumRequisitosTicketCheckBox(checked: false, ticket: item));
      }
      listRequisitosTicket = datum;
    } else {
      return [];
    }
    return datum;
  }

  cambiarRequisitosPorTicket(
      DatumRequisitosTicketCheckBox? ticketCheckBox, bool? estado) {
    listRequisitosTicket
        .firstWhere((element) => element == ticketCheckBox)!
        .checked = estado;
    notifyListeners();
  }

  bool verificarRequisitosTicket() {
    bool estado = true;
    if(this.listRequisitosTicket.isNotEmpty){
      for (var item in listRequisitosTicket) {
        if (item!.checked != item.ticket!.esRequerido && item.ticket!.esRequerido!) {
          estado = false;
          // break;
        } else {
          //estado=false;
          break;
        }
      }
    }else{
      estado=true;
    }
    return estado;
  }

  Future<List<DatumTicket?>?> obtenerTickets(int? id,int? pendiente,int? idArea,int? creadoPor,int? idSucursal,int? idEstado)async{
    final ticketResponse=await _veteranoService.obtenerTicket(id, pendiente, idArea, idSucursal,creadoPor ,idEstado);
    final listaTicket=ticketResponse.data;
    if(listaTicket!=null){
      return listaTicket;
    }
    return null;

  }

  Future<void> obtenerCitas(String? query)async{
    final c=await _veteranoService.obtenerCitas(query);
    final citasModel=c.data;
    if(citasModel!=null){
      citas=citasModel;
      notifyListeners();
    }
  }


  Future<void>obtenerPermisos()async{
    final perm={};
    final resp=await _veteranoService.obtenerPermisos();
    if(resp!=null && resp.length>0){
      permisos=resp;
      permisos!.forEach((e) {
        perm[e!.opcionTransaccion]=true;
      });
      
      if(perm.isNotEmpty){
      perm.forEach((key, value) {
        permisos!.forEach((element) {
          if(element!.opcionTransaccion==key){
            element.accion=value;
          }
         });
       });
    }
      
    }
    
    //print(permisos);

  }
  
}

class DatumRequisitosTicketCheckBox {
  DatumRequisitosTicket? ticket;
  bool? checked;

  DatumRequisitosTicketCheckBox({this.ticket, this.checked});
}
