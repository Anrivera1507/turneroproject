import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:socket_io_client/socket_io_client.dart' as IO;
import 'package:turnero_flutter_app/src/utils.dart';

enum ServerStatus {
  Online,
  Offline,
  Connecting
}

class SocketService with ChangeNotifier{

  ServerStatus _serverStatus=ServerStatus.Connecting;
  late IO.Socket _socket;
  late bool mostrar=true;

  ServerStatus get serverStatus=>this._serverStatus;
  BuildContext? contexto;

  IO.Socket get socket=>this._socket;
  Function get emit =>this._socket.emit;

  SocketService(){
    this._initConfig();
  }

  void _initConfig() {
    this._socket=IO.io((kDebugMode)?'https://dev.socket.inabve.gob.sv':'https://socket.inabve.gob.sv',{
      "transports": ["websocket"],
      'path': '/socket/socket.io'

    });
    this._socket.on('connection-stabilished', 
    (_){
      this._serverStatus=ServerStatus.Online;
      print("Conectado al socket");
      mostrar=true;
      notifyListeners();
    }
    );

    this._socket.on('connect_error', (_){
      this._serverStatus=ServerStatus.Offline;
      notifyListeners();
      if(mostrar){
        if(contexto!=null){
          show(contexto!,'Se perdido la conexión a la base de datos');
          mostrar=false;
        }
        
      }
      
    });
  }
  
}