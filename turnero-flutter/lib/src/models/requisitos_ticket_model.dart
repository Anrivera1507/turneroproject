// To parse this JSON data, do
//
//     final requisitosTicketModel = requisitosTicketModelFromJson(jsonString);

import 'dart:convert';

RequisitosTicketModel requisitosTicketModelFromJson(String str) => RequisitosTicketModel.fromJson(json.decode(str));

String requisitosTicketModelToJson(RequisitosTicketModel data) => json.encode(data.toJson());

class RequisitosTicketModel {
    RequisitosTicketModel({
        this.message,
        this.data,
        this.token,
    });

    String? message;
    List<DatumRequisitosTicket>? data;
    String? token;

    factory RequisitosTicketModel.fromJson(Map<String, dynamic> json) => RequisitosTicketModel(
        message: json["message"] == null ? null : json["message"],
        data: json["data"] == null ? null : List<DatumRequisitosTicket>.from(json["data"].map((x) => DatumRequisitosTicket.fromJson(x))),
        token: json["token"],
    );

    Map<String, dynamic> toJson() => {
        "message": message == null ? null : message,
        "data": data == null ? null : List<dynamic>.from(data!.map((x) => x.toJson())),
        "token": token,
    };
}

class DatumRequisitosTicket {
    DatumRequisitosTicket({
        this.id,
        this.idDocTicket,
        this.idArea,
        this.esRequerido,
        this.creadoEn,
        this.creadoPor,
        this.modificadoEn,
        this.modificadoPor,
        this.activo,
        this.area,
        this.documento
    });

    String? id='-1';
    String? idDocTicket;
    String? idArea;
    bool? esRequerido;
    DateTime? creadoEn;
    String? creadoPor;
    DateTime? modificadoEn;
    String? modificadoPor;
    bool? activo;
    String? documento;
    String? area;

    factory DatumRequisitosTicket.fromJson(Map<String, dynamic> json) => DatumRequisitosTicket(
        id: json["id"] == null ? null : json["id"],
        idDocTicket: json["idDocTicket"] == null ? null : json["idDocTicket"],
        idArea: json["idArea"] == null ? null : json["idArea"],
        esRequerido: json["esRequerido"] == null ? null : json["esRequerido"],
        creadoEn: json["creadoEn"] == null ? null : DateTime.parse(json["creadoEn"]),
        creadoPor: json["creadoPor"] == null ? null : json["creadoPor"],
        modificadoEn: json["modificadoEn"] == null ? null : DateTime.parse(json["modificadoEn"]),
        modificadoPor: json["modificadoPor"] == null ? null : json["modificadoPor"],
        activo: json["activo"] == null ? null : json["activo"],
        area: json['area']==null?null:json['area'],
        documento: json['documento']==null?null:json['documento']
    );

    Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "idDocTicket": idDocTicket == null ? null : idDocTicket,
        "idArea": idArea == null ? null : idArea,
        "esRequerido": esRequerido == null ? null : esRequerido,
        "creadoEn": creadoEn == null ? null : creadoEn?.toIso8601String(),
        "creadoPor": creadoPor == null ? null : creadoPor,
        "modificadoEn": modificadoEn == null ? null : modificadoEn?.toIso8601String(),
        "modificadoPor": modificadoPor == null ? null : modificadoPor,
        "activo": activo == null ? null : activo,
        "area": area == null ? null : area,
        "documento": documento == null ? null : documento,
    };
}
