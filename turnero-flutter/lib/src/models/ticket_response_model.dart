// To parse this JSON data, do
//
//     final ticketResponseModel = ticketResponseModelFromJson(jsonString);

import 'dart:convert';

TicketResponseModel ticketResponseModelFromJson(String str) => TicketResponseModel.fromJson(json.decode(str));

String ticketResponseModelToJson(TicketResponseModel data) => json.encode(data.toJson());

class TicketResponseModel {
    TicketResponseModel({
        this.message,
        this.data,
        this.token,
    });

    String? message;
    List<DatumTicket>? data;
    String? token;

    factory TicketResponseModel.fromJson(Map<String, dynamic> json) => TicketResponseModel(
        message: json["message"],
        data: List<DatumTicket>.from(json["data"].map((x) => DatumTicket.fromJson(x))),
        token: json["token"],
    );

    Map<String, dynamic> toJson() => {
        "message": message,
        "data": List<dynamic>.from(data!.map((x) => x.toJson())),
        "token": token,
    };
}

class DatumTicket {
    DatumTicket({
        this.id,
        this.idEstado,
        this.idArea,
        this.idTecnico,
        this.idVeterano,
        this.codigo,
        this.numeroLlamada,
        this.fechaInicio,
        this.fechaFin,
        this.escritorio,
        this.creadoEn,
        this.creadoPor,
        this.modificadoEn,
        this.modificadoPor,
        this.activo,
        this.nombreCompleto,
        this.area,
        this.nombres,
        this.apellidos,
        this.codigoImagen,
        this.nombresImagen,
        this.apellidosImagen,


    });

    String? id;
    String? idEstado;
    String? idArea;
    String? idTecnico;
    String? idVeterano;
    String? codigo;
    String? numeroLlamada;
    DateTime? fechaInicio;
    DateTime? fechaFin;
    int? escritorio;
    DateTime? creadoEn;
    String? creadoPor;
    DateTime? modificadoEn;
    String? modificadoPor;
    bool? activo;
    String? nombreCompleto;
    String? area;
    String? nombres;
    String? apellidos;
    String? codigoImagen;
    String? nombresImagen;
    String? apellidosImagen;

    factory DatumTicket.fromJson(Map<String, dynamic> json) => DatumTicket(
        id: json["id"],
        idEstado: json["idEstado"],
        idArea: json["idArea"],
        idTecnico: json["idTecnico"],
        idVeterano: json["idVeterano"],
        codigo: json["codigo"],
        numeroLlamada: json["numeroLlamada"],
        fechaInicio: DateTime.parse(json["fechaInicio"]),
        fechaFin: DateTime.parse(json["fechaFin"]),
        escritorio: json["escritorio"],
        creadoEn: DateTime.parse(json["creadoEn"]),
        creadoPor: json["creadoPor"],
        modificadoEn: DateTime.parse(json["modificadoEn"]),
        modificadoPor: json["modificadoPor"],
        activo: json["activo"],
        nombreCompleto: json["nombreCompleto"],
        area: json["area"],
        nombres: json['nombres'],
        apellidos: json['apellidos'],
        codigoImagen: json['codigoImagen'],
        nombresImagen: json['nombresImagen'],
        apellidosImagen:json['apellidosImagen']
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "idEstado": idEstado,
        "idArea": idArea,
        "idTecnico": idTecnico,
        "idVeterano": idVeterano,
        "codigo": codigo,
        "numeroLlamada": numeroLlamada,
        "fechaInicio": fechaInicio!.toIso8601String(),
        "fechaFin": fechaFin!.toIso8601String(),
        "escritorio": escritorio,
        "creadoEn": creadoEn!.toIso8601String(),
        "creadoPor": creadoPor,
        "modificadoEn": modificadoEn!.toIso8601String(),
        "modificadoPor": modificadoPor,
        "activo": activo,
        "nombreCompleto":nombreCompleto,
        "area":area,
        "nombres":nombres,
        "apellidos":apellidos,
        "codigoImagen":codigoImagen,
        "nombresImagen":nombresImagen,
        "apellidosImagen":apellidosImagen

    };
}
