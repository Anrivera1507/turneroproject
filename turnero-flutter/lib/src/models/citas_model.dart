// To parse this JSON data, do
//
//     final citasModel = citasModelFromJson(jsonString);

import 'dart:convert';

CitasModel citasModelFromJson(String str) => CitasModel.fromJson(json.decode(str));

String citasModelToJson(CitasModel data) => json.encode(data.toJson());

class CitasModel {
    CitasModel({
        this.message,
        this.data,
        this.token,
        this.error,
    });

    String? message;
    List<DataCitas?>? data;
    String? token;
    String? error;

    factory CitasModel.fromJson(Map<String, dynamic> json) => CitasModel(
        message: json["message"] == null ? null : json["message"],
        data: json["data"] == null ? null : List<DataCitas>.from(json["data"].map((x) => DataCitas.fromJson(x))),
        token: json["token"] == null ? null : json["token"],
        error: json["error"] == null ? null : json["error"],
    );

    Map<String, dynamic> toJson() => {
        "message": message == null ? null : message,
        "data": data == null ? null : List<DataCitas>.from(data!.map((x) => x!.toJson())),
        "token": token == null ? null : token,
        "error": error == null ? null : error,
    };
}

class DataCitas {
    DataCitas({
        this.id,
        this.idVeterano,
        this.idBeneficiario,
        this.idProceso,
        this.idSubArea,
        this.idSucursal,
        this.referencia,
        this.fechaCita,
        this.descripcion,
        this.ccAgendado,
        this.telefono,
        this.creadoEn,
        this.creadoPor,
        this.modificadoEn,
        this.modificadoPor,
        this.activo,
        this.referenciaCita,
        this.nombres,
        this.apellidos,
        this.idPersona,
        this.dui,
        this.tipo,
        this.subArea,
        this.sucursal,
        this.area,
        this.descripcionProceso,
        this.carnet,
        this.sector,
    });

    String? id;
    String? idVeterano;
    String? idBeneficiario;
    String? idProceso;
    String? idSubArea;
    String? idSucursal;
    String? referencia;
    DateTime? fechaCita;
    String? descripcion;
    bool? ccAgendado;
    String? telefono;
    DateTime? creadoEn;
    String? creadoPor;
    DateTime? modificadoEn;
    String? modificadoPor;
    bool? activo;
    String? referenciaCita;
    String? nombres;
    String? apellidos;
    String? idPersona;
    String? dui;
    String? tipo;
    String? subArea;
    String? sucursal;
    String? area;
    String? descripcionProceso;
    String? carnet;
    String? sector;

    factory DataCitas.fromJson(Map<String, dynamic> json) => DataCitas(
        id: json["id"] == null ? null : json["id"],
        idVeterano: json["idVeterano"] == null ? null : json["idVeterano"],
        idBeneficiario: json["idBeneficiario"] == null ? null : json["idBeneficiario"],
        idProceso: json["idProceso"] == null ? null : json["idProceso"],
        idSubArea: json["idSubArea"] == null ? null : json["idSubArea"],
        idSucursal: json["idSucursal"] == null ? null : json["idSucursal"],
        referencia: json["referencia"] == null ? null : json["referencia"],
        fechaCita: json["fechaCita"] == null ? null : DateTime.parse(json["fechaCita"]),
        descripcion: json["descripcion"] == null ? null : json["descripcion"],
        ccAgendado: json["ccAgendado"] == null ? null : json["ccAgendado"],
        telefono: json["telefono"] == null ? null : json["telefono"],
        creadoEn: json["creadoEn"] == null ? null : DateTime.parse(json["creadoEn"]),
        creadoPor: json["creadoPor"] == null ? null : json["creadoPor"],
        modificadoEn: json["modificadoEn"] == null ? null : DateTime.parse(json["modificadoEn"]),
        modificadoPor: json["modificadoPor"] == null ? null : json["modificadoPor"],
        activo: json["activo"] == null ? null : json["activo"],
        referenciaCita: json["referenciaCita"] == null ? null : json["referenciaCita"],
        nombres: json["nombres"] == null ? null : json["nombres"],
        apellidos: json["apellidos"] == null ? null : json["apellidos"],
        idPersona: json["idPersona"] == null ? null : json["idPersona"],
        dui: json["dui"] == null ? null : json["dui"],
        tipo: json["tipo"] == null ? null : json["tipo"],
        subArea: json["subArea"] == null ? null : json["subArea"],
        sucursal: json["sucursal"] == null ? null : json["sucursal"],
        area: json["area"] == null ? null : json["area"],
        descripcionProceso: json["descripcionProceso"] == null ? null : json["descripcionProceso"],
        carnet: json["carnet"] == null ? null : json["carnet"],
        sector: json["sector"] == null ? null : json["sector"],
    );

    Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "idVeterano": idVeterano == null ? null : idVeterano,
        "idBeneficiario": idBeneficiario == null ? null : idBeneficiario,
        "idProceso": idProceso == null ? null : idProceso,
        "idSubArea": idSubArea == null ? null : idSubArea,
        "idSucursal": idSucursal == null ? null : idSucursal,
        "referencia": referencia == null ? null : referencia,
        "fechaCita": fechaCita == null ? null : fechaCita?.toIso8601String(),
        "descripcion": descripcion == null ? null : descripcion,
        "ccAgendado": ccAgendado == null ? null : ccAgendado,
        "telefono": telefono == null ? null : telefono,
        "creadoEn": creadoEn == null ? null : creadoEn?.toIso8601String(),
        "creadoPor": creadoPor == null ? null : creadoPor,
        "modificadoEn": modificadoEn == null ? null : modificadoEn?.toIso8601String(),
        "modificadoPor": modificadoPor == null ? null : modificadoPor,
        "activo": activo == null ? null : activo,
        "referenciaCita": referenciaCita == null ? null : referenciaCita,
        "nombres": nombres == null ? null : nombres,
        "apellidos": apellidos == null ? null : apellidos,
        "idPersona": idPersona == null ? null : idPersona,
        "dui": dui == null ? null : dui,
        "tipo": tipo == null ? null : tipo,
        "subArea": subArea == null ? null : subArea,
        "sucursal": sucursal == null ? null : sucursal,
        "area": area == null ? null : area,
        "descripcionProceso": descripcionProceso == null ? null : descripcionProceso,
        "carnet": carnet == null ? null : carnet,
        "sector": sector == null ? null : sector,
    };
}
