// To parse this JSON data, do
//
//     final ticketModel = ticketModelFromJson(jsonString);

import 'dart:convert';

TicketModel ticketModelFromJson(String str) => TicketModel.fromJson(json.decode(str));

String ticketModelToJson(TicketModel data) => json.encode(data.toJson());

class TicketModel {
    TicketModel({
        this.id,
        this.idArea,
        this.idVeterano,
        this.creadoPor,
        this.modificadoPor,
        this.activo,
        this.prioridad,
        this.idSucursal='-1'

    });

    String?id;
    String? idArea;
    String? idVeterano;
    String? creadoPor;
    String? modificadoPor;
    bool? activo;
    bool? prioridad;
    String? idSucursal;

    factory TicketModel.fromJson(Map<String, dynamic> json) => TicketModel(
        id: json["id"],
        idArea: json["idArea"],
        idVeterano: json["idVeterano"],
        creadoPor: json["creadoPor"],
        modificadoPor: json["modificadoPor"],
        activo: json["activo"],
        prioridad: json["prioridad"],
        idSucursal: json['idSucursal']
        
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "idArea": idArea,
        "idVeterano": idVeterano,
        "creadoPor": creadoPor,
        "modificadoPor": modificadoPor,
        "activo": activo,
        "prioridad": prioridad,
        "idSucursal":idSucursal
    };
}
