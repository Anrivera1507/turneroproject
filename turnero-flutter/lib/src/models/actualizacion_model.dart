// To parse this JSON data, do
//
//     final actualizacionAppModel = actualizacionAppModelFromJson(jsonString);

import 'dart:convert';

ActualizacionAppModel actualizacionAppModelFromJson(String str) => ActualizacionAppModel.fromJson(json.decode(str));

String actualizacionAppModelToJson(ActualizacionAppModel data) => json.encode(data.toJson());

class ActualizacionAppModel {
    ActualizacionAppModel({
        this.message,
        this.data,
        this.token,
    });

    String? message;
    List<DatumActualizacion>? data;
    String? token;

    factory ActualizacionAppModel.fromJson(Map<String, dynamic> json) => ActualizacionAppModel(
        message: json["message"],
        data: List<DatumActualizacion>.from(json["data"].map((x) => DatumActualizacion.fromJson(x))),
        token: json["token"],
    );

    Map<String, dynamic> toJson() => {
        "message": message,
        "data": List<dynamic>.from(data!.map((x) => x.toJson())),
        "token": token,
    };
}

class DatumActualizacion {
    DatumActualizacion({
        this.id,
        this.androidVersion,
        this.androidMsg,
        this.androidUrl,
        this.creadoEn,
        this.creadoPor,
        this.modificadoEn,
        this.modificadoPor,
        this.activo,
    });

    String? id;
    String? androidVersion;
    String? androidMsg;
    String? androidUrl;
    DateTime? creadoEn;
    String? creadoPor;
    DateTime? modificadoEn;
    String? modificadoPor;
    bool? activo;

    factory DatumActualizacion.fromJson(Map<String, dynamic> json) => DatumActualizacion(
        id: json["id"],
        androidVersion: json["androidVersion"],
        androidMsg: json["androidMsg"],
        androidUrl: json["androidUrl"],
        creadoEn: DateTime.parse(json["creadoEn"]),
        creadoPor: json["creadoPor"],
        modificadoEn: DateTime.parse(json["modificadoEn"]),
        modificadoPor: json["modificadoPor"],
        activo: json["activo"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "androidVersion": androidVersion,
        "androidMsg": androidMsg,
        "androidUrl": androidUrl,
        "creadoEn": creadoEn?.toIso8601String(),
        "creadoPor": creadoPor,
        "modificadoEn": modificadoEn?.toIso8601String(),
        "modificadoPor": modificadoPor,
        "activo": activo,
    };
}
