// To parse this JSON data, do
//
//     final veteranoModel = veteranoModelFromJson(jsonString);

import 'dart:convert';


VeteranoModel veteranoModelFromJson(String str) => VeteranoModel.fromJson(json.decode(str));

String veteranoModelToJson(VeteranoModel data) => json.encode(data.toJson());

class VeteranoModel {
    VeteranoModel({
         this.message,
          this.data,
         this.token,
    });

    String? message;
    List<DatumVeterano>? data;
    dynamic token;

    factory VeteranoModel.fromJson(Map<String, dynamic> json) => VeteranoModel(
        message: json["message"],
        data: List<DatumVeterano>.from(json["data"].map((x) => DatumVeterano.fromJson(x))),
        token: json["token"],
    );

    Map<String, dynamic> toJson() => {
        "message": message,
        "data": List<dynamic>.from(data!.map((x) => x.toJson())),
        "token": token,
    };
}

class DatumVeterano {
    DatumVeterano({
        this.id,
        this.idSector,
        this.idEstado,
        this.carnet,
        this.nombres,
        this.apellidos,
        this.sexo,
        this.dui,
        this.nit,
        this.cip,
        this.fechaEmision,
        this.fechaNacimimento,
        this.telefono,
        this.celular1,
        this.celular2,
        this.correo,
        this.pensionable,
        this.foprolyd,
        this.ipsfa,
        this.afp,
        this.inpep,
        this.universal,
        this.creadoEn,
        this.creadoPor,
        this.modificadoEn,
        this.modificadoPor,
        this.estado,
        this.sector,
        
    });

    String? id;
    String? idSector;
    String? idEstado;
    String? carnet;
    String? nombres;
    String? apellidos;
    String? sexo;
    String? dui;
    String? nit;
    String? cip;
    DateTime? fechaEmision;
    DateTime? fechaNacimimento;
    String? telefono;
    String? celular1;
    String? celular2;
    String? correo;
    bool? pensionable;
    bool? foprolyd;
    bool? ipsfa;
    bool? afp;
    bool? inpep;
    bool? universal;
    DateTime? creadoEn;
    String? creadoPor;
    DateTime? modificadoEn;
    String? modificadoPor;
    String? estado;
    String? sector;
    

    factory DatumVeterano.fromJson(Map<String, dynamic> json) => DatumVeterano(
        id: json["id"],
        idSector: json["idSector"],
        idEstado: json["idEstado"],
        carnet: json["carnet"],
        nombres: json["nombres"],
        apellidos: json["apellidos"],
        sexo: json["sexo"],
        dui: json["dui"],
        nit: json["nit"],
        cip: json["cip"],
        fechaEmision: DateTime.parse(json["fechaEmision"]),
        fechaNacimimento: DateTime.parse(json["fechaNacimimento"]),
        telefono: json["telefono"],
        celular1: json["celular1"],
        celular2: json["celular2"],
        correo: json["correo"],
        pensionable: json["pensionable"],
        foprolyd: json["foprolyd"],
        ipsfa: json["ipsfa"],
        afp: json["afp"],
        inpep: json["inpep"],
        universal: json["universal"],
        creadoEn: DateTime.parse(json["creadoEn"]),
        creadoPor: json["creadoPor"],
        modificadoEn: DateTime.parse(json["modificadoEn"]),
        modificadoPor: json["modificadoPor"],
        estado: json["estado"],
        sector: json["sector"],
        
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "idSector": idSector,
        "idEstado": idEstado,
        "carnet": carnet,
        "nombres": nombres,
        "apellidos": apellidos,
        "sexo": sexo,
        "dui": dui,
        "nit": nit,
        "cip": cip,
        "fechaEmision": fechaEmision?.toIso8601String(),
        "fechaNacimimento": fechaNacimimento?.toIso8601String(),
        "telefono": telefono,
        "celular1": celular1,
        "celular2": celular2,
        "correo": correo,
        "pensionable": pensionable,
        "foprolyd": foprolyd,
        "ipsfa": ipsfa,
        "afp": afp,
        "inpep": inpep,
        "universal": universal,
        "creadoEn": creadoEn?.toIso8601String(),
        "creadoPor": creadoPor,
        "modificadoEn": modificadoEn?.toIso8601String(),
        "modificadoPor": modificadoPor,
        "estado": estado,
        "sector": sector,
       
    };
}

class Sobrevivencia {
    Sobrevivencia({
        this.id,
        this.idVeterano,
        this.idBeneficiario,
        this.idOperativo,
        this.idAsistencia,
        this.idPeligrosidad,
        this.fecha,
        this.duiTercero,
        this.observacion,
        this.creadoEn,
        this.creadoPor,
        this.modificadoEn,
        this.modificadoPor,
    });

    String? id;
    String? idVeterano;
    String? idBeneficiario;
    String? idOperativo;
    String? idAsistencia;
    String? idPeligrosidad;
    DateTime? fecha;
    String? duiTercero;
    String? observacion;
    DateTime? creadoEn;
    String? creadoPor;
    DateTime? modificadoEn;
    String? modificadoPor;

    factory Sobrevivencia.fromJson(Map<String, dynamic> json) => Sobrevivencia(
        id: json["id"],
        idVeterano: json["idVeterano"],
        idBeneficiario: json["idBeneficiario"],
        idOperativo: json["idOperativo"],
        idAsistencia: json["idAsistencia"],
        idPeligrosidad: json["idPeligrosidad"],
        fecha: DateTime.parse(json["fecha"]),
        duiTercero: json["duiTercero"],
        observacion: json["observacion"],
        creadoEn: DateTime.parse(json["creadoEn"]),
        creadoPor: json["creadoPor"],
        modificadoEn: DateTime.parse(json["modificadoEn"]),
        modificadoPor: json["modificadoPor"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "idVeterano": idVeterano,
        "idBeneficiario": idBeneficiario,
        "idOperativo": idOperativo,
        "idAsistencia": idAsistencia,
        "idPeligrosidad": idPeligrosidad,
        "fecha": fecha?.toIso8601String(),
        "duiTercero": duiTercero,
        "observacion": observacion,
        "creadoEn": creadoEn?.toIso8601String(),
        "creadoPor": creadoPor,
        "modificadoEn": modificadoEn?.toIso8601String(),
        "modificadoPor": modificadoPor,
    };
}
