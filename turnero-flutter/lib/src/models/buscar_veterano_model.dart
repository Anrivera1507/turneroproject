// To parse this JSON data, do
//
//     final buscarVeteranoModel = buscarVeteranoModelFromJson(jsonString);

import 'dart:convert';

BuscarVeteranoModel buscarVeteranoModelFromJson(String str) => BuscarVeteranoModel.fromJson(json.decode(str));

String buscarVeteranoModelToJson(BuscarVeteranoModel data) => json.encode(data.toJson());

class BuscarVeteranoModel {
    BuscarVeteranoModel({
        this.busqueda,
        this.grupoFamiliar,
        this.sobrevivencia,
        this.sondeo,
        this.descuentos,
    });

    String? busqueda;
    bool? grupoFamiliar;
    bool? sobrevivencia;
    bool? sondeo;
    bool? descuentos;

    factory BuscarVeteranoModel.fromJson(Map<String, dynamic> json) => BuscarVeteranoModel(
        busqueda: json["busqueda"],
        grupoFamiliar: json["grupoFamiliar"],
        sobrevivencia: json["sobrevivencia"],
        sondeo: json["sondeo"],
        descuentos: json["descuentos"],
    );

    Map<String, dynamic> toJson() => {
        "busqueda": busqueda,
        "grupoFamiliar": grupoFamiliar,
        "sobrevivencia": sobrevivencia,
        "sondeo": sondeo,
        "descuentos": descuentos,
    };
}
