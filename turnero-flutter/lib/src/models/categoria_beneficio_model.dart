// To parse this JSON data, do
//
//     final categoriaBeneficioModel = categoriaBeneficioModelFromJson(jsonString);

import 'dart:convert';

CategoriaBeneficioModel categoriaBeneficioModelFromJson(String str) => CategoriaBeneficioModel.fromJson(json.decode(str));

String categoriaBeneficioModelToJson(CategoriaBeneficioModel data) => json.encode(data.toJson());

class CategoriaBeneficioModel {
    CategoriaBeneficioModel({
        required this.tipo,
    });

    String tipo;

    factory CategoriaBeneficioModel.fromJson(Map<String, dynamic> json) => CategoriaBeneficioModel(
        tipo: json["tipo"],
    );

    Map<String, dynamic> toJson() => {
        "tipo": tipo,
    };
}
