// To parse this JSON data, do
//
//     final permisosModel = permisosModelFromJson(jsonString);

import 'dart:convert';

PermisosModel permisosModelFromJson(String str) => PermisosModel.fromJson(json.decode(str));

String permisosModelToJson(PermisosModel data) => json.encode(data.toJson());

class PermisosModel {
    PermisosModel({
        this.message,
        this.data,
        this.token,
        this.error,
    });

    String? message;
    List<DatumPermisos>? data;
    String? token;
    String? error;

    factory PermisosModel.fromJson(Map<String, dynamic> json) => PermisosModel(
        message: json["message"],
        data: List<DatumPermisos>.from(json["data"].map((x) => DatumPermisos.fromJson(x))),
        token: json["token"],
        error: json["error"],
    );

    Map<String, dynamic> toJson() => {
        "message": message,
        "data": List<dynamic>.from(data!.map((x) => x.toJson())),
        "token": token,
        "error": error,
    };
}

class DatumPermisos {
    DatumPermisos({
        this.opcion,
        this.transaccion,
        this.opcionTransaccion,
    });

    String? opcion;
    String? transaccion;
    String? opcionTransaccion;
    bool? accion=false;

    factory DatumPermisos.fromJson(Map<String, dynamic> json) => DatumPermisos(
        opcion: json["opcion"],
        transaccion: json["transaccion"],
        opcionTransaccion: json["opcionTransaccion"],
    );

    Map<String, dynamic> toJson() => {
        "opcion": opcion,
        "transaccion": transaccion,
        "opcionTransaccion": opcionTransaccion,
    };
}
