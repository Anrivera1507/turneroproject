// To parse this JSON data, do
//
//     final usuarioResponseModel = usuarioResponseModelFromJson(jsonString);

import 'dart:convert';

UsuarioResponseModel usuarioResponseModelFromJson(String str) => UsuarioResponseModel.fromJson(json.decode(str));

String usuarioResponseModelToJson(UsuarioResponseModel data) => json.encode(data.toJson());

class UsuarioResponseModel {
    UsuarioResponseModel({
        this.message,
        this.data,
        this.token,
    });

    String? message;
    Data? data;
    String? token;

    factory UsuarioResponseModel.fromJson(Map<String, dynamic> json) => UsuarioResponseModel(
        message: json["message"],
        data: Data.fromJson(json["data"]),
        token: json["token"],
    );

    Map<String, dynamic> toJson() => {
        "message": message,
        "data": data?.toJson(),
        "token": token,
    };
}

class Data {
    Data({
        this.id,
        this.idDepartamento,
        this.usuario,
        this.clave,
        this.nombres,
        this.apellidos,
        this.ultimoLogin,
        this.bloqueado,
        this.loginFallido,
        this.nuevaClave,
        this.creadoEn,
        this.creadoPor,
        this.modificadoEn,
        this.modificadoPor,
        this.activo,
        this.idSucursal='-1'
    });

    String? id;
    String? idDepartamento;
    String? usuario;
    String? clave;
    String? nombres;
    String? apellidos;
    DateTime? ultimoLogin;
    bool? bloqueado;
    int? loginFallido;
    bool? nuevaClave;
    DateTime? creadoEn;
    String? creadoPor;
    DateTime? modificadoEn;
    String? modificadoPor;
    bool? activo;
    String? idSucursal;

    factory Data.fromJson(Map<String, dynamic> json) => Data(
        id: json["id"],
        idDepartamento: json["idDepartamento"],
        usuario: json["usuario"],
        clave: json["clave"],
        nombres: json["nombres"],
        apellidos: json["apellidos"],
        ultimoLogin: DateTime.parse(json["ultimoLogin"]),
        bloqueado: json["bloqueado"],
        loginFallido: json["loginFallido"],
        nuevaClave: json["nuevaClave"],
        creadoEn: DateTime.parse(json["creadoEn"]),
        creadoPor: json["creadoPor"],
        modificadoEn: DateTime.parse(json["modificadoEn"]),
        modificadoPor: json["modificadoPor"],
        activo: json["activo"],
        idSucursal: json['idSucursal']
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "idDepartamento": idDepartamento,
        "usuario": usuario,
        "clave": clave,
        "nombres": nombres,
        "apellidos": apellidos,
        "ultimoLogin": ultimoLogin?.toIso8601String(),
        "bloqueado": bloqueado,
        "loginFallido": loginFallido,
        "nuevaClave": nuevaClave,
        "creadoEn": creadoEn?.toIso8601String(),
        "creadoPor": creadoPor,
        "modificadoEn": modificadoEn?.toIso8601String(),
        "modificadoPor": modificadoPor,
        "activo": activo,
        "idSucursal":idSucursal
    };
}
