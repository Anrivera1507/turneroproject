// To parse this JSON data, do
//
//     final usuarioModel = usuarioModelFromJson(jsonString);

import 'dart:convert';

UsuarioModel usuarioModelFromJson(String str) => UsuarioModel.fromJson(json.decode(str));

String usuarioModelToJson(UsuarioModel data) => json.encode(data.toJson());

class UsuarioModel {
    UsuarioModel({
        this.usuario,
        this.clave,
        this.passTheTraffic=true
    });

    String? usuario;
    String? clave;
    bool passTheTraffic;

    factory UsuarioModel.fromJson(Map<String, dynamic> json) => UsuarioModel(
        usuario: json["usuario"],
        clave: json["clave"],
        passTheTraffic: json['passTheTraffic']
    );

    Map<String, dynamic> toJson() => {
        "usuario": usuario,
        "clave": clave,
        "passTheTraffic":passTheTraffic
    };
}
