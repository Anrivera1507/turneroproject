// To parse this JSON data, do
//
//     final areasAtencionModel = areasAtencionModelFromJson(jsonString);

import 'dart:convert';

AreasAtencionModel areasAtencionModelFromJson(String str) => AreasAtencionModel.fromJson(json.decode(str));

String areasAtencionModelToJson(AreasAtencionModel data) => json.encode(data.toJson());

class AreasAtencionModel {
    AreasAtencionModel({
        this.message,
        this.data,
        this.token,
    });

    String? message;
    List<DatumAreas>? data;
    dynamic token;

    factory AreasAtencionModel.fromJson(Map<String, dynamic> json) => AreasAtencionModel(
        message: json["message"],
        data: List<DatumAreas>.from(json["data"].map((x) => DatumAreas.fromJson(x))),
        token: json["token"],
    );

    Map<String, dynamic> toJson() => {
        "message": message,
        "data": List<dynamic>.from(data!.map((x) => x.toJson())),
        "token": token,
    };
}

class DatumAreas {
    DatumAreas({
        this.id,
        this.idRelacion,
        this.tipo,
        this.codigo,
        this.descripcion,
        this.creadoEn,
        this.creadoPor,
        this.modificadoEn,
        this.modificadoPor,
        this.activo,
    });

    String? id;
    String? idRelacion;
    String? tipo;
    String? codigo;
    String? descripcion;
    DateTime? creadoEn;
    String? creadoPor;
    DateTime? modificadoEn;
    String? modificadoPor;
    bool? activo;

    factory DatumAreas.fromJson(Map<String, dynamic> json) => DatumAreas(
        id: json["id"],
        idRelacion: json["idRelacion"],
        tipo: json["tipo"],
        codigo: json["codigo"],
        descripcion: json["descripcion"],
        creadoEn: DateTime.parse(json["creadoEn"]),
        creadoPor: json["creadoPor"],
        modificadoEn: DateTime.parse(json["modificadoEn"]),
        modificadoPor: json["modificadoPor"],
        activo: json["activo"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "idRelacion": idRelacion,
        "tipo": tipo,
        "codigo": codigo,
        "descripcion": descripcion,
        "creadoEn": creadoEn!.toIso8601String(),
        "creadoPor": creadoPor,
        "modificadoEn": modificadoEn!.toIso8601String(),
        "modificadoPor": modificadoPor,
        "activo": activo,
    };
}
