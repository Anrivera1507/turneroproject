import 'package:flutter/material.dart';
import 'package:turnero_flutter_app/src/pages/areas_page.dart';
import 'package:turnero_flutter_app/src/pages/citas_page.dart';
import 'package:turnero_flutter_app/src/pages/home_page.dart';
import 'package:turnero_flutter_app/src/pages/login_page.dart';
import 'package:turnero_flutter_app/src/pages/requisitos_ticket_page.dart';
import 'package:turnero_flutter_app/src/pages/ticket_page.dart';

Map<String, WidgetBuilder> getApplicationRoutes(){
    return {
      'buscar-veterano':(BuildContext context)=>BuscarVeteranoPage(),
      'areas-atencion':(BuildContext context)=>AreasPage(),
      'ticket-page':(BuildContext context)=>TicketPage(),
      'login-page':(BuildContext context)=>LoginPage(),
      'requisitos-page':(BuildContext context)=>RequisitosPage(),
      'citas-page':(BuildContext context)=>CitasPage()


    };
  }