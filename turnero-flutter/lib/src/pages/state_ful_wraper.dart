import 'dart:io';
import 'dart:ui';

import 'package:art_sweetalert/art_sweetalert.dart';
import 'package:flutter/material.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:sn_progress_dialog/progress_dialog.dart';
import 'package:turnero_flutter_app/src/services/preferencias_usuario.dart';
import 'package:turnero_flutter_app/src/services/turnero_service.dart';
import 'dart:isolate';

class StatefulWraper extends StatefulWidget {
  final bool ejecutar;
  final Widget child;
  const StatefulWraper({ Key? key,required this.ejecutar,required this.child }) : super(key: key);

  @override
  State<StatefulWraper> createState() => _StatefulWraperState();
}

class _StatefulWraperState extends State<StatefulWraper> {
  VeteranoService _veteranoService=new VeteranoService();
  ReceivePort _port = ReceivePort();
  late ProgressDialog pr;

  late bool _permissionReady;
  late String _localPath;

  @override
  void initState() {
    super.initState();
    if(widget.ejecutar){
      _bindBackgroundIsolate();
      iniciar();
      
    }
  }

  void _bindBackgroundIsolate(){
    bool isSuccess=IsolateNameServer.registerPortWithName(_port.sendPort, 'downloader_send_port');
    if(!isSuccess){
      _unbindBackgroundIsolate();
      _bindBackgroundIsolate();
      return;
    }
  }

  void _unbindBackgroundIsolate() {
    IsolateNameServer.removePortNameMapping('downloader_send_port');
  }

  Future<void> iniciar() async {
    //await FlutterDownloader.initialize();
    await FlutterDownloader.registerCallback(downloadCallback);
    verificarActualizacion(context);

  }

  static void downloadCallback(String? id, DownloadTaskStatus status, int progress)async{
    
    print("id:$id===== status=======:$status=====progress======:$progress");
    final send=IsolateNameServer.lookupPortByName('©')!;
    send.send([id,status,progress]);
  }

  @override
  Widget build(BuildContext context) {
    return widget.child;
  }

  Future<void> verificarActualizacion(BuildContext context) async {
    if (Platform.isAndroid)
    {
      PackageInfo packageInfo = await PackageInfo.fromPlatform();
      String localVersion = packageInfo.version;
      final pref=PreferenciasUsuario();
      final updateApp = await _veteranoService.verificarActualizacion();

      if (updateApp.data != null) {
        final c = updateApp.data![0].androidVersion!.compareTo(localVersion);
        if (c == 1) {
          //await mostrarActualizacion(context, updateApp.data![0]);
          //await _prepare(context);
          ArtDialogResponse? response= await ArtSweetAlert.show(
                      context: context,
                      barrierDismissible: false,
                      artDialogArgs: ArtDialogArgs(
                          title:
                              "Sivet Turnero - Actualización detectada v.${updateApp.data!.first.androidVersion}",
                          text: "¿Quieres actualizar a la ultima versión?",
                          confirmButtonText: "Aceptar",
                          confirmButtonColor: Colors.green,
                          type: ArtSweetAlertType.warning
                    ),
                  ); 
          if(response==null){
            pref.ultimaPagina='login-page';
            Navigator.of(context).pushNamedAndRemoveUntil('login-page', (route) => false);
            return;
            
          }
          if(response.isTapConfirmButton){
            await _prepare(context);
          }
                  
        }
      }
    }
    else if(Platform.isIOS)
    {
      PackageInfo packageInfo = await PackageInfo.fromPlatform();
      String localVersion = packageInfo.version;
      
      final updateApp = await _veteranoService.verificarActualizacion();

      if (updateApp.data != null) {
        final c = updateApp.data![0].androidVersion!.compareTo(localVersion);
        if (c == 1) {
          //await mostrarActualizacion(context, updateApp.data![0]);
          //await _prepare(context);
          ArtDialogResponse? response= await ArtSweetAlert.show(
                      context: context,
                      barrierDismissible: false,
                      artDialogArgs: ArtDialogArgs(
                          title:
                              "Sivet Docs - Actualización detectada v.${updateApp.data!.first.androidVersion}",
                          text: "Por favor contactar a la Unidad de TICS para la actualización",
                          confirmButtonText: "Aceptar",
                          showCancelBtn: false,
                          confirmButtonColor: Colors.green,
                          type: ArtSweetAlertType.warning)
                  ); 
          if(response==null){
            return;
          }    
          if(response.isTapConfirmButton){
            final pre = PreferenciasUsuario();
            pre.ultimaPagina='login-page';
            Navigator.of(context).pushReplacementNamed('login-page');
          }
        }
      }
    }
  }

  Future<Null> _prepare(BuildContext context) async {
    _permissionReady = await _checkPermission();
    if (_permissionReady) {
      //
      final actualizacionApp = await _veteranoService.verificarActualizacion();
      //_requestDownload(actualizacionApp.data!.first.androidUrl!);
       await _prepareSaveDir(context,actualizacionApp.data!.first.androidUrl!);
     // _tasks!.addAll()
    }
  }

  Future<bool> _checkPermission() async {
    if (Platform.isAndroid) {
      PermissionStatus permission = await Permission.storage.request();
      if (permission != PermissionStatus.granted) {
        await Permission.storage.request();
      }
      if (permission == PermissionStatus.granted) {
        return true;
      }
    } else {
      return true;
    }
    return false;
  }

  _prepareSaveDir(BuildContext context,String url) async{

    _localPath=await _apkLocalPath;
    final saveDir=Directory(_localPath);
    bool hasExisted=await saveDir.exists();
    if(!hasExisted){
      saveDir.create();
    }
    await executeDownload(context,url);



  }

  Future<String> get _apkLocalPath async {
//    final directory = widget.platform == TargetPlatform.android
//        ? await getExternalStorageDirectory()
//        : await getApplicationDocumentsDirectory();

    final directory = await getExternalStorageDirectory();
    _localPath = directory!.path.toString();
    return _localPath;
  }

  Future<void> executeDownload(BuildContext context, String url) async{
    pr=ProgressDialog(context: context);
    if(!pr.isOpen()){
      pr.show(max: 100, msg: "Descargando...",barrierDismissible: false,);
    }
    final path=await _apkLocalPath;
    File file=File(path+'/'+'aplicacion_turnero.apk');
    if(await file.exists())await file.delete();
    final taskId=await FlutterDownloader.enqueue(
      url: url,
      savedDir: path,
      showNotification: true,
      openFileFromNotification: true
    );
    print(taskId);
    
    Future.delayed(Duration(seconds: 10),()async{
      if(pr.isOpen()){
        pr.close();
      }
       await FlutterDownloader.open(taskId: taskId!);
    });


  }

  
}