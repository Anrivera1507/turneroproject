import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';
import 'dart:ui' as ui;

import 'package:art_sweetalert/art_sweetalert.dart';
import 'package:blue_thermal_printer/blue_thermal_printer.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:path_provider/path_provider.dart';
import 'package:provider/provider.dart';
import 'package:turnero_flutter_app/src/models/veterano_model.dart';
import 'package:turnero_flutter_app/src/pages/contenedores/ticket_previa.dart';
import 'package:turnero_flutter_app/src/providers/veterano_provider.dart';
import 'package:turnero_flutter_app/src/services/preferencias_usuario.dart';
import 'package:turnero_flutter_app/src/utils.dart';

class TicketPage extends StatefulWidget {
  const TicketPage({Key? key}) : super(key: key);

  @override
  _TicketPageState createState() => _TicketPageState();
}

class _TicketPageState extends State<TicketPage> {
  String pathImage = "";
  String pathLogo = "";
  String pathImg = "";
  @override
  void initState() {
    super.initState();
    initSaveToPath();
  }

  Future<void> writeToFile(ByteData data, String path) {
    final buffer = data.buffer;
    return new File(path).writeAsBytes(
        buffer.asUint8List(data.offsetInBytes, data.lengthInBytes));
  }

  initSaveToPath() async {
    final filename = "sivetweb-logo-v2.png";
    final fileLogo = "logo-inabve-new.png";
    var bytes = await rootBundle.load("assets/img/$filename");
    var bytesLogo = await rootBundle.load("assets/img/$fileLogo");
    //var bytesCodigo=await rootBundle.load("https://res.cloudinary.com/syscoredevs/image/upload/l_text:Arial_75_bold:C003/v1650470029/Inabve/img-white_df6b96.png");
    String dir = (await getApplicationDocumentsDirectory()).path;
    writeToFile(bytes, '$dir/$filename');
    writeToFile(bytesLogo, '$dir/$fileLogo');
    setState(() {
      pathImage = '$dir/$filename';
      pathLogo = '$dir/$fileLogo';
    });
  }

  regresarPagina(BuildContext context) async {
    ArtDialogResponse? response = await ArtSweetAlert.show(
        context: context,
        barrierDismissible: false,
        artDialogArgs: ArtDialogArgs(
            denyButtonText: 'Cancelar',
            confirmButtonColor: Theme.of(context).primaryColor,
            title: "¿Seguro de continuar?",
            text: '¿Desea salir, se perdera el ticket generado?',
            confirmButtonText: 'Aceptar',
            type: ArtSweetAlertType.info)
    );
    if (response == null) {
      return;
    }
    if (response.isTapConfirmButton) {
      Navigator.of(context).pop();
      Navigator.of(context).pushNamed('areas-atencion');
    }
  }

  @override
  Widget build(BuildContext context) {
    BlueThermalPrinter bluetooth = BlueThermalPrinter.instance;
    final _prefs = new PreferenciasUsuario();
    final veterProvider = Provider.of<VeteranoProvider>(context);
    return Scaffold(
      appBar: myAppBar(titulo: "Ticket", context: context, regresar: true,funcionEjecutar: ()async=>await regresarPagina(context)),
      body: SingleChildScrollView(
        child: Column(
          children: [
            (veterProvider.ticketResponseModel.data != null)
                ? TicketPrevia(
                    emitido:
                        veterProvider.ticketResponseModel.data!.first.creadoEn,
                    nombreCompleto: veterProvider
                        .ticketResponseModel.data!.first.nombreCompleto,
                    area: veterProvider.ticketResponseModel.data!.first.area,
                    codigo:
                        veterProvider.ticketResponseModel.data!.first.codigo,
                  )
                //?Image.memory(base64Decode(veterProvider.ticketResponseModel.data!.first.codigoImagen!))
                : Container(),
            //Divider(),
            Container(
              //color: Theme.of(context).primaryColor,
              padding: EdgeInsets.symmetric(horizontal: 15),
              width: double.infinity,
              child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                      primary: Theme.of(context).primaryColor,
                      side: BorderSide(color: Theme.of(context).primaryColor),
                      padding: const EdgeInsets.symmetric(
                          horizontal: 50, vertical: 10),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10))),
                  child: const Text(
                    'Imprimir ticket',
                    //style: TextStyle(fontSize: 24),
                  ),
                  onPressed: () async {
                    if (_prefs.bluetoothConectado) {
                      await imprimirTicket(
                          context: context,
                          bluetooth: bluetooth,
                          veteSrv: veterProvider,
                          fechaCreacion: veterProvider
                              .ticketResponseModel.data!.first.creadoEn,
                          encabezadoArriba: veterProvider
                              .ticketResponseModel.data!.first.nombres,
                          encabezadoAbajo: veterProvider
                              .ticketResponseModel.data!.first.apellidos,
                          principal: veterProvider
                              .ticketResponseModel.data!.first.codigoImagen,
                          piePagina: veterProvider
                              .ticketResponseModel.data!.first.area
                          );
                    } else {
                      mostrarAlerta(context,
                          titulo: "Bluetooth conectado?",
                          descripcion:
                              "Debe vincular la impresora con el dispositivo");
                    }
                  }),
            )
          ],
        ),
      ),
      floatingActionButton: FittedBox(
        child: FloatingActionButton(
          tooltip: "Generar nuevo ticket",
          onPressed: () {
            veterProvider.veterano = new VeteranoModel();
            veterProvider.cambiarPrioridadTicket(false);
            Navigator.of(context).pop();
            Navigator.of(context).pushReplacementNamed('buscar-veterano');
          },
          child: Icon(
            Icons.post_add,
            size: 40,
          ),
          backgroundColor: Theme.of(context).primaryColor,
        ),
      ),
    );
  }
}
