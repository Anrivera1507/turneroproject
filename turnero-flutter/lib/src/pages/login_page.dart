import 'package:flutter/material.dart';
import 'package:flutter_login/flutter_login.dart';
import 'package:provider/provider.dart';
import 'package:turnero_flutter_app/src/pages/home_page.dart';
import 'package:turnero_flutter_app/src/providers/veterano_provider.dart';
import 'package:turnero_flutter_app/src/services/preferencias_usuario.dart';

// ignore: must_be_immutable
class LoginPage extends StatelessWidget {
  LoginPage({Key? key}) : super(key: key);
  late VeteranoProvider _veteranoProvider;

  final users = const {
  'vic@mail.com': '1234',
  'hunter@gmail.com': 'hunter',
};
  
  Duration get loginTime => Duration(milliseconds: 2250);

 Future<String> _recoverPassword(String name) {
    debugPrint('Name: $name');
    return Future.delayed(loginTime).then((_) {
      if (!users.containsKey(name)) {
        return 'Usuario no existe';
      }
      return '';
    });
  }

  Future<String?> _authUser(LoginData data)async {
    final p=PreferenciasUsuario();
    //debugPrint('Name: ${data.name}, Password: ${data.password}');
    String? s;
    s=await _veteranoProvider.login(data);
    if(s==null){
      p.ultimaPagina="buscar-veterano";
    }
    return s;

  }



  @override
  Widget build(BuildContext context) {
    _veteranoProvider=Provider.of<VeteranoProvider>(context);
    return FlutterLogin(
     // title: 'SivetApp',
     title: "",
      logo: "assets/img/logo-sivetweb.png",
      theme: LoginTheme(
        primaryColor: Theme.of(context).primaryColor,
        textFieldStyle: TextStyle(color: Theme.of(context).primaryColor),
        
      ),
        onSignup:(onSignUp)async{
          return onSignUp.name.toString();
        },
        onLogin: _authUser,
        onRecoverPassword: _recoverPassword,
        onSubmitAnimationCompleted: (){
          Navigator.of(context).pushReplacement(
            MaterialPageRoute(builder: (context)=>BuscarVeteranoPage())
          );
        },
        
    );
  }
}
