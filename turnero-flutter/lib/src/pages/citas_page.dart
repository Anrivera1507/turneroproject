import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:turnero_flutter_app/src/pages/contenedores/contenedor_citas.dart';
import 'package:turnero_flutter_app/src/providers/socket_service.dart';
import 'package:turnero_flutter_app/src/providers/veterano_provider.dart';
import 'package:turnero_flutter_app/src/utils.dart';

class CitasPage extends StatelessWidget {
  const CitasPage({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final veteranoProvider=Provider.of<VeteranoProvider>(context);
    final socketProvider=Provider.of<SocketService>(context);
    TextEditingController citasController=TextEditingController();
    return Scaffold(
      appBar: myAppBar(context: context,titulo: 'Ver citas'),
      body: Container(
        padding: EdgeInsets.all(10),
        child: Column(
          children: [
            Container(
              margin: EdgeInsets.only(bottom: 10),
              child: TextField(
                controller: citasController,
                decoration: InputDecoration(
                  border: OutlineInputBorder(
                  ),
                ),
                onEditingComplete: ()async{
                  FocusManager.instance.primaryFocus?.unfocus();
                  mostrarLoading(context);
                await veteranoProvider.obtenerCitas(citasController.text);
                Navigator.of(context).pop();
                },
              ),
            ),
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                primary: Theme.of(context).primaryColor
              ),
              onPressed:(socketProvider.serverStatus==ServerStatus.Online)? ()async{
                FocusManager.instance.primaryFocus?.unfocus();
                mostrarLoading(context);
                await veteranoProvider.obtenerCitas(citasController.text);
                Navigator.of(context).pop();
            }:null, 
            child: Container(
              width: double.infinity,
              child: Center(child: Text("Buscar citas".toUpperCase(),style: TextStyle(fontSize: 16),))
            )
            ),
            Divider(),
            (veteranoProvider.citas!.isNotEmpty)
            ?Expanded(
              child: ListView.builder(
                itemCount: veteranoProvider.citas!.length,
                itemBuilder: (c,i){
                return GestureDetector(
                  onTap: ()async{
                    //Genera la accion al requisito del ticket
                    mostrarLoading(context);
                    String idArea=veteranoProvider.areasAtencionModel.data!.where((element) => element.codigo == 'S').first.id.toString();
                    await veteranoProvider.obtenerRequisitosTicket(idArea);
                    Navigator.of(context).pop();
                    Navigator.of(context).pushNamed('requisitos-page');
                  },
                  child: ContenedorCitas(
                  area: veteranoProvider.citas![i]!.area,
                  carnet: veteranoProvider.citas![i]!.carnet, 
                  dui: veteranoProvider.citas![i]!.dui,
                  fechaCita: veteranoProvider.citas![i]!.fechaCita,
                  nombreCompleto: veteranoProvider.citas![i]!.nombres.toString()+' '+veteranoProvider.citas![i]!.apellidos.toString(),
                  subArea: veteranoProvider.citas![i]!.subArea.toString(),
                  ),
                );
              }),
            )
            :Container()
          ],
        ),
      ),
    );
  }
}