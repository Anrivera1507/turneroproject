import 'package:date_format/date_format.dart';
import 'package:flutter/material.dart';

// ignore: must_be_immutable
class ContenedorCitas extends StatelessWidget {
  ContenedorCitas({ Key? key,this.nombreCompleto='Nombre completo',this.dui='DUI',this.carnet,this.area='Area',this.subArea='Sub Area',this.fechaCita }) : super(key: key);
  String? nombreCompleto;
  String? dui;
  String? area;
  String? subArea;
  DateTime? fechaCita;
  String? carnet;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(bottom: 10),
      child: Card(
        elevation: 10,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              margin: EdgeInsets.all(5),
              height: 120,
              width: 120,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: Color.fromARGB(37, 158, 158, 158)
              ),
              child: (this.carnet!=null)?ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: FadeInImage(
                  fit: BoxFit.contain,
                  placeholder: AssetImage('assets/img/avatar.jpg'), 
                  image: NetworkImage("https://core.inabve.gob.sv/veteran_photos/api/public/dl/7HOtKaYd/VET_FOTO_"+this.carnet.toString()+".jpg?inline=true")
                ),
              ):ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Image(image: AssetImage('assets/img/avatar.jpg'))
              ),
            ),
            Expanded(
              child: Column(
                crossAxisAlignment:CrossAxisAlignment.start,
                children: [
                  Container(
                    margin: EdgeInsets.only(left: 5,top: 5),
                    child: Text(this.nombreCompleto.toString(),style: TextStyle(fontWeight: FontWeight.bold),)
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 5,top: 5),
                    child: Text(this.dui.toString()),
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 5,top: 5),
                    child: Text(this.area.toString()),
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 5,top: 5),
                    child: Text(this.subArea.toString()),
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 5,top: 5),
                    child: Text("Fecha cita"),
                  ),
                  Table(
                    children: [
                      tableRowCita(fechaCita!=null?formatDate(fechaCita as DateTime, [dd,'/',MM,'/',yyyy],locale: SpanishDateLocale()):"Fecha", fechaCita!=null?formatDate(fechaCita as DateTime, [HH,':',nn,' ',am]):"Hora"),
                    ],
                  ),
                ],
                
              )
            )
          ],
        )
      ),
    );
  }

  TableRow tableRowCita(String titulo,String descripcion) {
    return TableRow(
            children: [
              Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: Color.fromARGB(37, 158, 158, 158)
                ),
                margin: EdgeInsets.all(5),
                child: Text(titulo),
              ),
              Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: Color.fromARGB(37, 158, 158, 158)
                ),
                margin: EdgeInsets.all(5),
                child: Text(descripcion),
                //color: Colors.red,
              )
            ],
          );
  }
}