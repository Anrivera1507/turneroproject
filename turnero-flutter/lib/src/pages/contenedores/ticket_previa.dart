import 'package:date_format/date_format.dart';
import 'package:flutter/material.dart';

class TicketPrevia extends StatelessWidget {
  final DateTime? emitido;
  final String? nombreCompleto;
  final String? codigo;
  final String? area;
  const TicketPrevia({ Key? key,this.emitido,this.nombreCompleto,this.codigo,this.area }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Card(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10)),
                margin: EdgeInsets.all(15),
                elevation: 2,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(10),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.symmetric(vertical: 20),
                        child: FadeInImage(
                          // En esta propiedad colocamos la imagen a descargar
                          image: AssetImage("assets/img/logo-inabve-new.png"),

                          // En esta propiedad colocamos el gif o imagen de carga
                          // debe estar almacenado localmente
                          placeholder: AssetImage('assets/img/avatar.jpg'),

                          // En esta propiedad colocamos mediante el objeto BoxFit
                          // la forma de acoplar la imagen en su contenedor
                          fit: BoxFit.contain,
                          // En esta propiedad colocamos el alto de nuestra imagen
                          height: 100,
                          width: 100,
                        ),
                      ),
                      Container(
                        width: double.infinity,
                        padding: EdgeInsets.all(10),
                        child: Center(
                            child: Column(
                          children: [
                            Text("Emitido: " +
                                formatDate(
                                    this.emitido!=null?this.emitido!:DateTime.now(),
                                    [
                                      dd,
                                      '/',
                                      mm,
                                      '/',
                                      yyyy,
                                      " - ",
                                      hh,
                                      ":",
                                      nn,
                                      " ",
                                      am
                                    ])),
                            SizedBox(
                              height: 10,
                            ),
                            Text("Bienvenido/a"),
                            SizedBox(
                              height: 10,
                            ),
                            Text(
                              this.nombreCompleto
                                  .toString(),
                              style: TextStyle(
                                  fontWeight: FontWeight.w900,
                                  color: Colors.black,
                                  fontSize: 14),
                            ),
                            Divider(),
                            Text(
                              this.codigo
                                  .toString(),
                              style: TextStyle(
                                  fontWeight: FontWeight.w900,
                                  color: Colors.black,
                                  fontSize: 54),
                            ),
                            Divider(),
                            Text("Area de " +
                                this.area
                                    .toString()),
                            Container(
                              padding: EdgeInsets.symmetric(vertical: 20),
                              child: FadeInImage(
                                // En esta propiedad colocamos la imagen a descargar
                                image: AssetImage(
                                    "assets/img/logo-sivetweb_1.png"),

                                // En esta propiedad colocamos el gif o imagen de carga
                                // debe estar almacenado localmente
                                placeholder:
                                    AssetImage('assets/img/avatar.jpg'),

                                // En esta propiedad colocamos mediante el objeto BoxFit
                                // la forma de acoplar la imagen en su contenedor
                                fit: BoxFit.contain,
                                // En esta propiedad colocamos el alto de nuestra imagen
                                height: 40,
                                width: 100,
                              ),
                            ),
                          ],
                        )),
                      )
                    ],
                  ),
                )),
    );
  }
}