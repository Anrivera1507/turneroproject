// ignore: import_of_legacy_library_into_null_safe
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:turnero_flutter_app/src/models/veterano_model.dart';

class ContenedorVeterano extends StatelessWidget {
  final Color colorGris;
  final VeteranoModel veteranoModel;
  ContenedorVeterano({
    Key? key,
    required this.veteranoModel,
    required this.colorGris
  }) : super(key: key);

  

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Container(
       // color: Colors.blue,
        child: ListView(
          children: [
            ClipRRect(
              borderRadius: BorderRadius.circular(10),
              child: Container(
                padding: EdgeInsets.only(top: 20,left: 15,right: 15),
                color: Colors.white,
                
                child: Column(
                  children: [
                    ClipRRect(
                      borderRadius: BorderRadius.circular(8),
                      child: Container(
                        margin: EdgeInsets.only(bottom: 15),
                        color: colorGris,
                        height: 125,
                        width: 125,
                        child: CachedNetworkImage(
                          imageUrl: "https://core.inabve.gob.sv/veteran_photos/api/public/dl/7HOtKaYd/VET_FOTO_"+veteranoModel.data![0].carnet.toString()+".jpg?inline=true",
                          placeholder: (context,url)=>CircularProgressIndicator(),
                          errorWidget: (context,url,error)=>Image(image: AssetImage("assets/img/avatar.jpg")),
                        ),
                      ),
                    ), 
                    ClipRRect(
                      borderRadius: BorderRadius.circular(8),
                      child: Container(
                        color: colorGris,
                        width: double.infinity,
                        child: Container(
                          margin: EdgeInsets.all(5),
                          child: Text(this.veteranoModel.data![0].nombres.toString())
                        ),
                      ),
                    ),
                    Divider(),
                    ClipRRect(
                      borderRadius: BorderRadius.circular(8),
                      child: Container(
                        color: colorGris,
                        width: double.infinity,
                        child: Container(
                          margin: EdgeInsets.all(5),
                          child: Text(this.veteranoModel.data![0].apellidos.toString())
                        ),
                      ),
                    ),
                    Divider(),
                    ClipRRect(
                      borderRadius: BorderRadius.circular(8),
                      child: Container(
                        color: colorGris,
                        width: double.infinity,
                        child: Container(
                          margin: EdgeInsets.all(5),
                          child: Text(this.veteranoModel.data![0].carnet.toString())
                        ),
                      ),
                    ),
                    Divider(),
                    ClipRRect(
                      borderRadius: BorderRadius.circular(8),
                      child: Container(
                        margin: EdgeInsets.only(bottom: 45),
                        color: colorGris,
                        width: double.infinity,
                        child: Container(
                          margin: EdgeInsets.all(5),
                          child: Text(this.veteranoModel.data![0].dui.toString())
                        ),
                      ),
                    ),


                  ],
                ),
                
              ),
            )
          ],
        ),
      ),
    );
  }
}