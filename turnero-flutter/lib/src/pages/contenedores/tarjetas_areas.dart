import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:turnero_flutter_app/src/models/areas_atencion_model.dart';
import 'package:turnero_flutter_app/src/providers/veterano_provider.dart';
import 'package:turnero_flutter_app/src/utils.dart';
class TarjetasAreas extends StatelessWidget {
  final DatumAreas areasAtencionModel;
  final String idVeterano;
  const TarjetasAreas({
    required this.areasAtencionModel,
    required this.idVeterano,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final veterProvider = Provider.of<VeteranoProvider>(context);
    return Container(
      width: MediaQuery.of(context).size.width*.50,
      //height: MediaQuery.of(context).size.height*0.2,
      margin: EdgeInsets.only(left: 4),
      child: GestureDetector(
        onTap: ()async{
          //print(areasAtencionModel.id);
          await mostrarLoading(context);
          //await veterProvider.crearTicket(int.parse(this.areasAtencionModel.id.toString()));
          await veterProvider.obtenerRequisitosTicket(this.areasAtencionModel.id.toString());
          Navigator.of(context).pop();
          Navigator.of(context).pushReplacementNamed('requisitos-page',arguments: this.areasAtencionModel.id);
          
        },
        child: Card(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        margin: EdgeInsets.all(15),
        elevation: 2,
        child: ClipRRect(
          borderRadius: BorderRadius.circular(10),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              FadeInImage(
                
                // En esta propiedad colocamos la imagen a descargar
                image: AssetImage("assets/img/icono-card-a-"+areasAtencionModel.codigo![0].toString()+".png"),
      
                // En esta propiedad colocamos el gif o imagen de carga
                // debe estar almacenado localmente
                placeholder: AssetImage('assets/img/avatar.jpg'),
      
                // En esta propiedad colocamos mediante el objeto BoxFit 
                // la forma de acoplar la imagen en su contenedor
                fit: BoxFit.cover,
      
                // En esta propiedad colocamos el alto de nuestra imagen
                height: 140,
              ),
              Container(
                color: Theme.of(context).primaryColor,
                width: double.infinity,
                padding: EdgeInsets.all(10),
                child: Center(child: Text(areasAtencionModel.descripcion!.toUpperCase(),style: TextStyle(fontWeight: FontWeight.bold,color: Colors.white),)),
              )
            ],
          ),
        )
        ),
      ),
    );
  }
}