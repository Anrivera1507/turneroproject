import 'package:art_sweetalert/art_sweetalert.dart';
import 'package:blue_thermal_printer/blue_thermal_printer.dart';
import 'package:flutter/material.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:provider/provider.dart';
import 'package:turnero_flutter_app/src/pages/state_ful_wraper.dart';
import 'package:turnero_flutter_app/src/providers/socket_service.dart';
import 'package:turnero_flutter_app/src/providers/veterano_provider.dart';
import 'package:turnero_flutter_app/src/services/preferencias_usuario.dart';
import 'package:turnero_flutter_app/src/utils.dart';

import 'contenedores/contenedor_veterano.dart';

// ignore: must_be_immutable
class BuscarVeteranoPage extends StatelessWidget {
  TextEditingController _queryController = new TextEditingController(text: '');
  BlueThermalPrinter bluetooth = BlueThermalPrinter.instance;
  FocusNode myFocus = new FocusNode();
  BuscarVeteranoPage({Key? key}) : super(key: key);
  var maskFormatter = new MaskTextInputFormatter(
      mask: '########-#',
      filter: {"#": RegExp(r'[0-9]')},
      type: MaskAutoCompletionType.lazy);

  @override
  Widget build(BuildContext context) {
    final colorGris = Color.fromRGBO(245, 245, 245, 1);
    final veteranoProvider = Provider.of<VeteranoProvider>(context);
    final socketProvider = Provider.of<SocketService>(context);
    socketProvider.contexto = context;
    final prefs = PreferenciasUsuario();

    return WillPopScope(
      onWillPop: () => salirApp(context),
      child: StatefulWraper(
        ejecutar: true,
        child: Scaffold(
          drawer: myDrawer(context, veteranoProvider, prefs),
          backgroundColor: colorGris,
          appBar: myAppBar(context: context),
          body: (veteranoProvider.permisos!=null)
          ?Container(
            margin: EdgeInsets.all(10),
            padding: EdgeInsets.only(top: 10),
            child: Column(
              children: [
                Container(
                  //color: Colors.red,
                  //height: MediaQuery.of(context).size.height * .31,
                  child: Column(
                    children: [
                      TextField(
                        focusNode: myFocus,
                        cursorColor: Theme.of(context).primaryColor,
                        style: TextStyle(fontSize: 30, letterSpacing: 2.0),
                        inputFormatters: [maskFormatter],
                        controller: _queryController,
                        keyboardType: TextInputType.number,
                        decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderSide: BorderSide(
                                color: Theme.of(context).primaryColor,
                                width: 0.0,
                              ),
                              borderRadius: BorderRadius.circular(20)),
                          labelStyle:
                              TextStyle(color: Theme.of(context).primaryColor),
                          suffix: Icon(
                            Icons.search,
                            color: Theme.of(context).primaryColor,
                          ),
                          hintText: "Ingrese DUI",
                        ),
                        onEditingComplete: () async {
                          if (_queryController.text.length > 0 && socketProvider.serverStatus==ServerStatus.Online) {
                            await buscarVete(context, myFocus, veteranoProvider,
                                _queryController, bluetooth);
                          }
                        },
                      ),
                      //  Divider(),
                      Container(
                        //color: Theme.of(context).primaryColor,
                        width: double.infinity,
                        padding: EdgeInsets.symmetric(vertical: 20),
                        child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                              primary: Theme.of(context).primaryColor,
                              side: BorderSide(
                                  color: Theme.of(context).primaryColor),
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 50, vertical: 10),
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10))),
                          child: Container(
                            padding: EdgeInsets.symmetric(vertical: 5),
                            child: Text(
                              'Buscar veterano'.toUpperCase(),
                              style: TextStyle(fontSize: 16),
                            ),
                          ),
                          onPressed: (socketProvider.serverStatus ==
                                  ServerStatus.Online)
                              ? () async {
                                  if (_queryController.text.length==10) {
                                    await buscarVete(
                                        context,
                                        myFocus,
                                        veteranoProvider,
                                        _queryController,
                                        bluetooth);
                                  } else {
                                    mostrarAlerta(context,
                                        titulo: "Información",
                                        descripcion: "Debe ingresar un DUI"
                                    );
                                  }
                                }
                              : null,
                        ),
                      ),
                      (veteranoProvider.veterano.data==null && true)
                      ?Container(
                        //color: Theme.of(context).primaryColor,
                        width: double.infinity,
                        padding: EdgeInsets.only(top: 10),
                        child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                              primary: Color.fromRGBO(223, 135, 35, 30),
                              side: BorderSide(
                                  color: Color.fromRGBO(223, 135, 35, 30)),
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 50, vertical: 10),
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10))),
                          child: Container(
                            padding: EdgeInsets.symmetric(vertical: 5),
                            child: Text(
                              'Generar ticket invitado'.toUpperCase(),
                              style: TextStyle(fontSize: 16),
                            ),
                          ),
                          onPressed: (socketProvider.serverStatus == 
                                  ServerStatus.Online)
                              ? () async {
                                  PermissionStatus btA = await Permission
                                      .bluetoothAdvertise
                                      .request();
                                  PermissionStatus btCon = await Permission
                                      .bluetoothConnect
                                      .request();
                                  PermissionStatus btScan =
                                      await Permission.bluetoothScan.request();
                                  if (btA.isGranted &&
                                      btCon.isGranted &&
                                      btScan.isGranted) {
                                    ArtDialogResponse? resp =
                                        await ArtSweetAlert.show(
                                            context: context,
                                            barrierDismissible: false,
                                            artDialogArgs: ArtDialogArgs(
                                                denyButtonText: 'Cancelar',
                                                confirmButtonColor: Theme.of(context).primaryColor,
                                                title: "¿Seguro de continuar?",
                                                text:
                                                    '¿Desea crear ticket de información?',
                                                confirmButtonText: 'Aceptar',
                                                type: ArtSweetAlertType.info)
                                    );
                                    if (resp == null) {
                                      return;
                                    }
                                    if (resp.isTapConfirmButton) {
                                      mostrarLoading(context);
                                      await veteranoProvider.obtenerAreas();
                                      Navigator.of(context).pop();
                                      Navigator.of(context).pushNamed('areas-atencion');
                                    }
                                    

                                  } else {
                                    mostrarAlerta(context,
                                        titulo: 'Conexión Bluethoth',
                                        descripcion:
                                            "Habilite los permisos de conexión Bluethooth",
                                        icono: Icons.bluetooth,);
                                  }
                                }
                              : null,
                        ),
                      )
                      :Container()
                    ],
                  ),
                ),
                (veteranoProvider.veterano.data == null)
                    ? (Container())
                    : ContenedorVeterano(
                        veteranoModel: veteranoProvider.veterano,
                        colorGris: colorGris)
              ],
            ),
          )
          :Center(child: CircularProgressIndicator(),),
          floatingActionButton: (veteranoProvider.veterano.data != null)
              ? FloatingActionButton(
                  backgroundColor: Theme.of(context).primaryColor,
                  child: Icon(Icons.arrow_forward),
                  onPressed: () async{
                    mostrarLoading(context);
                    await veteranoProvider.obtenerAreas();
                    Navigator.of(context).pop();
                    Navigator.of(context).pushNamed('areas-atencion');
                  },
                )
              : Container(),
        ),
      ),
    );
  }

  Future<void> buscarVete(
      BuildContext context,
      FocusNode focusNode,
      VeteranoProvider veteranoProvider,
      TextEditingController _queryController,
      BlueThermalPrinter bluetooth) async {
    //FocusManager.instance.primaryFocus?.unfocus();
    final prefs = PreferenciasUsuario();

    focusNode.unfocus();
    mostrarLoading(context);
    await veteranoProvider.buscarVeterano(_queryController.text);
    Navigator.of(context).pop();
    if (veteranoProvider.veterano.data == null) {
      mostrarAlerta(context,
          titulo: "Información",
          descripcion:
              "No se encontro resultado de veterano(a).\nFavor ingresar DUI registrado",
          icono: Icons.question_mark_rounded, 
          funcion: () async {
            mostrarLoading(context);
            await veteranoProvider.obtenerAreas();
            Navigator.of(context).pop();
            Navigator.of(context).pushNamed('areas-atencion');   
      }, titleCallAction: 'Generar ticket info');
    }
  }
}
