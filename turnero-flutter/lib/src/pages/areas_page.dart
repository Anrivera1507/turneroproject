import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:turnero_flutter_app/src/models/areas_atencion_model.dart';
import 'package:turnero_flutter_app/src/models/veterano_model.dart';
import 'package:turnero_flutter_app/src/providers/veterano_provider.dart';
import 'package:turnero_flutter_app/src/utils.dart';

import 'contenedores/tarjetas_areas.dart';

class AreasPage extends StatelessWidget {
  const AreasPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _areasProvider=Provider.of<VeteranoProvider>(context);
    return Scaffold(
      appBar: myAppBar(context: context,titulo: "Seleccionar área"),
      body: Container(
        margin: EdgeInsets.only(top: 24),
        child: (_areasProvider.areasAtencionModel.data!=null)
        ?SingleChildScrollView(
          child: Wrap(  
            runSpacing: -10,
            spacing: -10,
            direction: Axis.horizontal,
            children: cargarAreas(_areasProvider.areasAtencionModel,_areasProvider.veterano)
          ),
        )
        :Center(child: Text("No hay areas disponibles"),),
      ),
    );
  }
  List<Widget> cargarAreas(AreasAtencionModel atencionModel,VeteranoModel veteranoModel ){
    List<Widget> variable=[];
    atencionModel.data!.forEach((element) {
      variable.add(
        TarjetasAreas(areasAtencionModel: element,idVeterano:(veteranoModel.data!=null)?veteranoModel.data![0].id!:'-1')
      );
     });
     //print(veteranoModel.data![0].id);
    return variable;
  }
}



