import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:turnero_flutter_app/src/providers/veterano_provider.dart';
import 'package:turnero_flutter_app/src/utils.dart';

class RequisitosPage extends StatelessWidget {
  const RequisitosPage({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final veteraProvider=Provider.of<VeteranoProvider>(context);
    String? idArea=ModalRoute.of(context)?.settings.arguments as String?;
    return Scaffold(
      appBar: myAppBar(context: context,titulo: 'Requisitos'),
      body: Column(
        children: [
          Container(
            padding: EdgeInsets.only(left: 5,top: 10),
            height: 100,
            width: double.infinity,
            //color: Colors.red,
            child: SwitchListTile(
              value: veteraProvider.esPrioridad, 
              onChanged: (onChanged){
              veteraProvider.cambiarPrioridadTicket(onChanged);
            },title: Text("Es prioridad")
            ),
          ),
          Expanded(
            child: (veteraProvider.listRequisitosTicket.isNotEmpty)?
            ListView.builder(
              itemCount: veteraProvider.listRequisitosTicket.length,
              itemBuilder: (c,i){
                return ListTile(
                  leading: Icon(Icons.add_card_rounded),
                  iconColor: (veteraProvider.listRequisitosTicket[i]!.ticket!.esRequerido!)?Color.fromARGB(255, 208, 29, 29):Color.fromARGB(255, 16, 137, 20),
                  title: Text(veteraProvider.listRequisitosTicket[i]!.ticket!.documento.toString()),
                  subtitle: Text((veteraProvider.listRequisitosTicket[i]!.ticket!.esRequerido!)?"Requerido":"Opcional"),
                  trailing: Checkbox(
                    value: veteraProvider.listRequisitosTicket[i]!.checked, 
                    onChanged: (onChanged){
                      veteraProvider.cambiarRequisitosPorTicket(veteraProvider.listRequisitosTicket[i], onChanged);
                    }
                  ),
                );
              })
            :Container(child: Center(child: Text("No hay requisitos ingresados"),),),
          )
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: ()async{
          bool status=veteraProvider.verificarRequisitosTicket();
          if(status){
            mostrarLoading(context);
            await veteraProvider.crearTicket(int.parse((idArea!=null)?idArea:veteraProvider.listRequisitosTicket[0]!.ticket!.idArea.toString()),prioridad: false);
            Navigator.of(context).pop();
            Navigator.of(context).pushReplacementNamed('ticket-page');
          }else{
            mostrarAlerta(context,titulo: 'Información',descripcion: 'Existen requisitos que son obligatorios');
            
          }
        },
        backgroundColor: Theme.of(context).primaryColor,
        child: Icon(Icons.arrow_forward),
      ),
    );
  }
}