import 'dart:io';

import 'package:animate_do/animate_do.dart';
import 'package:art_sweetalert/art_sweetalert.dart';
import 'package:blue_thermal_printer/blue_thermal_printer.dart';
import 'package:date_format/date_format.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:provider/provider.dart';
import 'package:turnero_flutter_app/src/pages/contenedores/ticket_previa.dart';
import 'package:turnero_flutter_app/src/pages/print_page.dart';
import 'package:turnero_flutter_app/src/providers/socket_service.dart';
import 'package:turnero_flutter_app/src/providers/veterano_provider.dart';
import 'package:turnero_flutter_app/src/services/preferencias_usuario.dart';

mostrarLoading(BuildContext c) {
  showDialog(
      context: c,
      barrierDismissible: false,
      builder: (_) => WillPopScope(
        onWillPop: ()async=>false,
        child: AlertDialog(
              title: Text("Espere..."),
              content: LinearProgressIndicator(),
            ),
      ));
}

mostrarAlerta(BuildContext c,
    {String titulo = '',
    String descripcion = '',
    IconData icono = Icons.info,
    VoidCallback? funcion,
    String titleCallAction = ''}) {
  showDialog(
      context: c,
      barrierDismissible: false,
      builder: (_) => FlipInX(
        duration: Duration(milliseconds: 400),
        child: AlertDialog(
              title: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [Expanded(child: Text(titulo)), Icon(icono)],
              ),
              content: Text(descripcion),
              actions: [
                ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    primary: Theme.of(c).primaryColor,
                  ),
                  onPressed: () {
                    Navigator.of(c).pop();
                  },
                  child: Text("Aceptar"),
                ),
                (funcion != null)
                    ? ElevatedButton(
                        style: ElevatedButton.styleFrom(
                            primary: Color.fromARGB(255, 147, 78, 4)
                        ),
                        onPressed: funcion,
                        child: Text(titleCallAction)
                      )
                    : Container()
              ],
            ),
      ));
}

Future<bool> salirApp(BuildContext context) async {
  return (await showDialog(
        context: context,
        builder: (context) => new AlertDialog(
          title: new Text('Salir de la aplicación?'),
          content: new Text('Desea salir de la aplicación'),
          actions: <Widget>[
            TextButton(
              onPressed: () => Navigator.of(context).pop(false),
              child: new Text('NO', style: TextStyle(color: Colors.grey[800])),
            ),
            TextButton(
              style: ElevatedButton.styleFrom(
                  //primary: Colors.red

                  ),
              onPressed: () => Navigator.of(context).pop(true),
              child: new Text(
                'SI',
                style: TextStyle(color: Colors.grey[800]),
              ),
            ),
          ],
        ),
      )) ??
      false;
}

AppBar myAppBar({String titulo = "", required BuildContext context, bool regresar=false, VoidCallback? funcionEjecutar=null}) {
  final pref = PreferenciasUsuario();
  final socket = Provider.of<SocketService>(context);
  return AppBar(
    leading: (regresar)
    ?IconButton(onPressed: (){
      //print("Clic hacia atras!!");
      if(funcionEjecutar!=null){
        funcionEjecutar();
      }
    }, icon: Icon(Icons.arrow_back)
    ):null,
    backgroundColor: Theme.of(context).primaryColor,
    title: Text(titulo),
    actions: [
      IconButton(
          onPressed: () async {
            // PermissionStatus bt=await Permission.bluetooth.request();
            PermissionStatus btA =
                await Permission.bluetoothAdvertise.request();
            PermissionStatus btCon =
                await Permission.bluetoothConnect.request();
            PermissionStatus btScan = await Permission.bluetoothScan.request();
            if (btA.isGranted && btCon.isGranted && btScan.isGranted) {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (_) => PrintPage(),
                ),
              );
            } else {
              mostrarAlerta(context,
                  titulo: 'Conexión Bluethoth',
                  descripcion: "Habilite los permisos de conexión Bluethooth",
                  icono: Icons.bluetooth);
            }
          },
          icon: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Icon(Icons.print),
              Flexible(
                child: Container(
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color:
                        (!pref.bluetoothConectado) ? Colors.red : Colors.green,
                  ),
                ),
              ),
            ],
          )),
      VerticalDivider(
        color: Colors.white,
        thickness: 1,
      ),
      IconButton(
          onPressed: () {
            mostrarAlerta(context,
            titulo: (socket.serverStatus==ServerStatus.Online)?'Conectado':'Desconectado',
            descripcion: (socket.serverStatus==ServerStatus.Online)?'Conexión con la base de datos de forma correcta':'Verifique la conexión con la base de datos, contacte con depto de TICS',
            icono: (socket.serverStatus==ServerStatus.Online)?Icons.check:Icons.close

          );
          },
          icon: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Icon(Icons.monitor),
              Flexible(
                child: Container(
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: (socket.serverStatus != ServerStatus.Online)
                        ? Colors.red
                        : Colors.green,
                  ),
                ),
              ),
            ],
          )),
    ],
  );
}

Future<void> imprimirTicket(
    {required BuildContext context,
    required BlueThermalPrinter bluetooth,
    required VeteranoProvider veteSrv,
    DateTime? fechaCreacion,
    String? principal,
    String? encabezadoArriba,
    String? encabezadoAbajo,
    String? piePagina}) async {
  final filename = "sivetweb-logo-v2.png";
  final fileLogo = "logo-inabve-new.png";
  String? pathEncabezadoArriba = '';
  String? pathEncabezadoAbajo = '';
  String? pathPrincipal = '';
  String? pathPiePagina = '';
  String pathImage = '';
  String pathLogo = '';
  mostrarLoading(context);
  var bytes = await rootBundle.load("assets/img/$filename");
  var bytesLogo = await rootBundle.load("assets/img/$fileLogo");
  String dir = (await getApplicationDocumentsDirectory()).path;
  writeToFile(bytes, '$dir/$filename');
  writeToFile(bytesLogo, '$dir/$fileLogo');
  pathImage = '$dir/$filename';
  pathLogo = '$dir/$fileLogo';
  pathPrincipal = await veteSrv.decodificarImagen(principal!=null?principal:'');
  // pathEncabezadoArriba = (encabezadoArriba != null)
  //     ? await veteSrv.downloadImg(encabezadoArriba, fontSize: 40.toString())
  //     : '';
  // pathEncabezadoAbajo = (encabezadoAbajo != null)
  //     ? await veteSrv.downloadImg(encabezadoAbajo, fontSize: 40.toString())
  //     : '';
  // pathPiePagina = (piePagina != null)
  //     ? await veteSrv.downloadImg("Área de: " + piePagina.toString(),
  //         fontSize: 30.toString())
  //     : '';

  //Estructura de la impresion del ticket
  bluetooth.printImage(pathLogo);
  bluetooth.printNewLine();
  bluetooth.printCustom(
      "Emitido: " +
          formatDate((fechaCreacion != null) ? fechaCreacion : DateTime.now(),
              [dd, '/', mm, '/', yyyy, " - ", hh, ":", nn, " ", am]),
      1,
      1);
  bluetooth.printCustom("Bienvenido/a", 1, 1);
  // (pathEncabezadoArriba != null)
  //     ? bluetooth.printImage(pathEncabezadoArriba)
  //     :
   bluetooth.printCustom((encabezadoArriba!=null)?encabezadoArriba.toString():'', 2, 1);
  // (pathEncabezadoAbajo != null)
  //     ? bluetooth.printImage(pathEncabezadoAbajo)
  //     :
   bluetooth.printCustom((encabezadoAbajo!=null)?encabezadoAbajo.toString():'', 2, 1);
  bluetooth.printCustom("-----------------------------------------", 3, 1);
  bluetooth.printCustom("Turno".toUpperCase(), 1, 1);
  (pathPrincipal != null)
      ? bluetooth.printImage(pathPrincipal)
      : bluetooth.printCustom(principal as String, 2, 1);
  bluetooth.printCustom("-----------------------------------------", 3, 1);
  // (pathPiePagina != null)
  //     ? bluetooth.printImage(pathPiePagina)
  //     : 
      bluetooth.printCustom("Area de: " + piePagina.toString(), 2, 1);
  bluetooth.printImage(pathImage);
  bluetooth.printNewLine();
  bluetooth.printNewLine();
  //Fin de la estructura de la impresion del ticket
  Navigator.of(context).pop();
}

Future<void> writeToFile(ByteData data, String path) {
  final buffer = data.buffer;
  return new File(path)
      .writeAsBytes(buffer.asUint8List(data.offsetInBytes, data.lengthInBytes));
}

Future show(
  BuildContext context,
  String message, {
  Duration duration: const Duration(seconds: 3),
}) async {
  await new Future.delayed(new Duration(milliseconds: 100));
  ScaffoldMessenger.of(context).showSnackBar(
    new SnackBar(
      content: new Text(
        message,
        style: new TextStyle(
          color: Colors.white,
        ),
      ),
      duration: duration,
    ),
  );
}

Drawer myDrawer(BuildContext context, VeteranoProvider veteranoProvider,
    PreferenciasUsuario prefs) {
  BlueThermalPrinter bluetooth = BlueThermalPrinter.instance;
  return Drawer(
    elevation: 10,
    child: Column(
      children: [
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            children: [
              Stack(
                alignment: AlignmentDirectional.center,
                children: [
                  SafeArea(
                    child: DrawerHeader(
                      padding: EdgeInsets.zero,
                      child: Container(
                        color: Theme.of(context).primaryColor,
                        height: MediaQuery.of(context).size.height * 0.1,
                      ),
                    ),
                  ),
                  CircleAvatar(
                    backgroundColor: Colors.white.withOpacity(0.9),
                    radius: 40,
                    child: Text(
                      (veteranoProvider.usuarioResponseModel.data != null)
                          ? veteranoProvider
                              .usuarioResponseModel.data!.nombres![0].toUpperCase()
                          : prefs.usuario[0].toUpperCase(),
                      style: TextStyle(
                          fontSize: 60, color: Theme.of(context).primaryColor),
                    ),
                  ),
                  Positioned(
                      bottom: 20,
                      child: Text(
                        (veteranoProvider.usuarioResponseModel.data != null)
                            ? veteranoProvider
                                    .usuarioResponseModel.data!.nombres! +
                                " " +
                                veteranoProvider
                                    .usuarioResponseModel.data!.apellidos!
                            : prefs.usuario,
                        style: TextStyle(color: Colors.white),
                      ))
                ],
              ),
              
              Divider(),
              ListTile(
                leading: Icon(Icons.history),
                title: Text("Ultimo ticket generado"),
                iconColor: Theme.of(context).primaryColor,
                onTap: () async {
                  //Crear la funcionalidad de ticket creados
                  final r = await veteranoProvider.obtenerTickets(
                      null, null, null, int.parse(prefs.idUsuario),int.parse(prefs.idSucursal) ,1);
                  if (r != null && r.isNotEmpty) {
                    //Navigator.of(context).pop();
                    ArtDialogResponse? response = await ArtSweetAlert.show(
                        context: context,
                        //barrierDismissible: false,
                        artDialogArgs: ArtDialogArgs(
                            title: 'Ultimo ticket generado para: ' +
                                r.last!.area!,
                            confirmButtonText: 'Imprimir ticket',
                            confirmButtonColor: Theme.of(context).primaryColor,
                            customColumns: [
                              Container(
                                margin: EdgeInsets.only(bottom: 12),
                                child: TicketPrevia(
                                  area: r.last!.area,
                                  codigo: r.last!.codigo,
                                  emitido: r.last!.creadoEn,
                                  nombreCompleto: r.last!.nombreCompleto,
                                ),
                              )
                            ]));
                    if (response == null) {
                      print("Clic hacia atras");
                      return;
                    }
                    if (response.isTapConfirmButton) {
                      if (prefs.bluetoothConectado) {
                        await imprimirTicket(
                          context: context,
                          bluetooth: bluetooth,
                          veteSrv: veteranoProvider,
                          principal: r.last!.codigo,
                          piePagina: r.last!.area,
                        );
                        Navigator.of(context).pop();
                      } else {
                        mostrarAlerta(context,
                            titulo: 'Ticket',
                            descripcion:
                                'Ticket NO fue impreso, debe vincular la impresora Bluetooth');
                      }
                    }
                  }else{
                    Navigator.of(context).pop();
                    mostrarAlerta(context,titulo: '¿Ultimo ticket?',descripcion: 'No tiene ticket generado \ncon fecha: '+formatDate(DateTime.now(), ['dd','-','MM','-','yyyy'],locale: SpanishDateLocale()),icono: Icons.add_card_sharp);
                  }
                },
              ),
              (prefs.idSucursal=='24212')
              ?ListTile(
                leading: Icon(Icons.assignment),
                title: Text("Ver citas"),
                iconColor: Theme.of(context).primaryColor,
                onTap: (){
                  Navigator.of(context).pop();
                  Navigator.of(context).pushNamed('citas-page');
                },
              )
              :Container(),
              ListTile(
                leading: Icon(
                  Icons.logout,
                  color: Theme.of(context).primaryColor,
                ),
                title: Text("Salir de sesión"),
                onTap: () {
                  String page = "login-page";
                  prefs.ultimaPagina = page;
                  Navigator.of(context).pushReplacementNamed(page);
                },
              ),
              Divider(),
            ],
          ),
        ),
        Text(("Creado por Dto de TICS | v${prefs.versionAndroid} | "
                .toUpperCase() +
            ((kDebugMode) ? "DEV" : "PRD")))
      ],
    ),
  );
}
