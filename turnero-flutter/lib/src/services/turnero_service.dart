import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:turnero_flutter_app/src/models/actualizacion_model.dart';
import 'package:turnero_flutter_app/src/models/areas_atencion_model.dart';
import 'package:turnero_flutter_app/src/models/buscar_veterano_model.dart';
import 'package:turnero_flutter_app/src/models/categoria_beneficio_model.dart';
import 'package:turnero_flutter_app/src/models/citas_model.dart';
import 'package:turnero_flutter_app/src/models/permisos_model.dart';
import 'package:turnero_flutter_app/src/models/requisitos_ticket_model.dart';
import 'package:turnero_flutter_app/src/models/ticket_model.dart';
import 'package:turnero_flutter_app/src/models/ticket_response_model.dart';
import 'package:turnero_flutter_app/src/models/usuario_model.dart';
import 'package:turnero_flutter_app/src/models/usuario_response_model.dart';
import 'package:turnero_flutter_app/src/models/veterano_model.dart';
import 'package:turnero_flutter_app/src/services/preferencias_usuario.dart';

const api_URL = (kDebugMode)?"http://192.168.254.3:3001":"https://apis.inabve.gob.sv/v1/api_sivetweb";
final headerJson = {'Content-type': 'application/json'};

class VeteranoService {
  Map<String, String> headerJsonToken(String token) {
    return {
      'Content-type': 'application/json',
      'Authorization': "Bearer " + token
    };
  }

  Future<VeteranoModel> buscarVeterano(String query) async {
    try {
      VeteranoModel _veteranoModel;
      BuscarVeteranoModel buscarVete = new BuscarVeteranoModel(
          busqueda: query,
          descuentos: true,
          grupoFamiliar: true,
          sobrevivencia: true,
          sondeo: true);
      final response = await http.post(
          Uri.parse(api_URL + "/registros/veteranos/buscar"),
          body: buscarVeteranoModelToJson(buscarVete),
          headers: headerJson);
      //print(response.body);
      if (response.statusCode == 200) {
        _veteranoModel = VeteranoModel.fromJson(json.decode(response.body));
        //print(_veteranoModel);
        return _veteranoModel;
      } else {
        return VeteranoModel(message: "message", data: null, token: "");
      }
    } catch (e) {
      print(e);
      return VeteranoModel(message: "message", data: null, token: "");
    }
  }

  Future<AreasAtencionModel> obtenerAreas() async {
    final pref=PreferenciasUsuario();
    try {
      CategoriaBeneficioModel categoriaBeneficioModel =
          new CategoriaBeneficioModel(tipo: "areasAtencion");
      final response = await http.post(
          Uri.parse(api_URL + "/administracion/catalogos/obtener"),
          headers: headerJsonToken(pref.token),
          body: categoriaBeneficioModelToJson(categoriaBeneficioModel)
      );
      //print(response.body);
      if (response.statusCode == 200) {
        final areas=AreasAtencionModel.fromJson(json.decode(response.body));
        //Verificamos de que sucursal, si es CENTRAL aparecen todas las areas, sino solo ventanilla
        if(pref.idSucursal!='24212'){
          final r=areas.data!.where((a) => a.codigo=='VEN').toList();
          areas.data=r;
        }

        return areas;
      } else {
        return AreasAtencionModel();
      }
    } catch (e) {
      return AreasAtencionModel();
    }
  }

  Future<TicketResponseModel> crearTicket(TicketModel ticket) async {
    try {
      final response = await http.post(
          Uri.parse(api_URL + "/ventanillas/tickets/guardar"),
          headers: headerJson,
          body: ticketModelToJson(ticket));
      if (response.statusCode == 201) {
        return TicketResponseModel.fromJson(json.decode(response.body));
      } else {
        return TicketResponseModel();
      }
    } catch (e) {
      return TicketResponseModel();
    }
  }

  Future<TicketResponseModel> obtenerTicket(int? id,int? pendiente,int? idArea,int? idSucursal,int? creadoPor,int? idEstado) async {
    try {
      final response = await http.post(
          Uri.parse(api_URL + "/ventanillas/tickets/obtener"),
          headers: headerJson,
          body:json.encode({
            "id":id,
            "pendiente":pendiente,
            "idArea":idArea,
            "idSucursal":idSucursal,
            "creadoPor":creadoPor,
            "idEstado":idEstado
          }) 
          );
      if (response.statusCode == 200) {
        return TicketResponseModel.fromJson(json.decode(response.body));
      } else {
        return TicketResponseModel();
      }
    } catch (e) {
      return TicketResponseModel();
    }
  }


  Future<http.Response> downloadImg(String texto,
      {String fontSize = '75'}) async {
    try {
      final response = await http.get(Uri.parse(
          "https://res.cloudinary.com/syscoredevs/image/upload/l_text:Arial_" +
              fontSize +
              "_bold:" +
              texto +
              "/v1654802910/Inabve/img-white_jxqftr.png"
          // "https://res.cloudinary.com/syscoredevs/image/upload/l_text:Arial_"+fontSize+"_bold:"+texto+"/v1650470029/Inabve/img-white_df6b96.png"
          ));
      return response;
    } catch (e) {
      return http.Response("", 404);
    }
  }

  Future<UsuarioResponseModel> login(UsuarioModel usuarioModel) async {
    try {
      final response = await http.post(Uri.parse(api_URL + "/seguridad/login"),
          headers: headerJson, body: usuarioModelToJson(usuarioModel));
      if (response.statusCode == 200) {
        return UsuarioResponseModel.fromJson(json.decode(response.body));
      } else {
        return UsuarioResponseModel(data: null, message: "", token: "");
      }
    } catch (e) {
      return UsuarioResponseModel(data: null, message: "", token: "");
    }
  }

  Future<List<DatumRequisitosTicket?>> obtenerRequisitosTicket(
      String idArea) async {
    final p = PreferenciasUsuario();
    try {
      final response = await http.post(
          Uri.parse(api_URL + '/ventanillas/requisitos/obtener'),
          headers: headerJsonToken(p.token),
          body: json.encode({"idArea": idArea})
        );
      if (response.statusCode == 200) {
        return RequisitosTicketModel.fromJson(json.decode(response.body)).data!;
      } else {
        return [];
      }
    } catch (e) {
      return [];
    }
  } 


  Future<ActualizacionAppModel> verificarActualizacion() async{
    final pref=PreferenciasUsuario();
    try {
      final response=await http.post(
        Uri.parse(api_URL+"/administracion/actualizacion/obtener"),
        headers: headerJsonToken(pref.token),
        body: json.encode({"nombre":"Sivet turnero"})
      );
      
      //print(response.body);
      if(response.statusCode==200){
        return ActualizacionAppModel.fromJson(json.decode(response.body));
      }
      return ActualizacionAppModel(data: null,message: "",token: "");
    } catch (e) {
      return ActualizacionAppModel(data: null,message: "",token: "");
    }
  }

  Future<CitasModel> obtenerCitas(String? query) async{
    final pref=PreferenciasUsuario();
    try {
      final response=await http.post(
        Uri.parse(api_URL+"/citas/citas/obtener"),
        headers: headerJsonToken(pref.token),
        body: json.encode({"query":query})
      );
      
      //print(response.body);
      if(response.statusCode==200){
        return CitasModel.fromJson(json.decode(response.body));
      }
      return CitasModel(data: null,message: "",token: "");
    } catch (e) {
      return CitasModel(data: null,message: "",token: "");
    }
  }

  Future<List<DatumPermisos>?> obtenerPermisos()async{
    try {
      final pref=PreferenciasUsuario();
      final response=await http.post(Uri.parse(api_URL+'/seguridad/permisos/obtener'),
      headers: headerJsonToken(pref.token),
      body: json.encode({"modulo":"Menu - tickets"})
      );
      if(response.statusCode==200){
        final listaPermisos=PermisosModel.fromJson(json.decode(response.body));
        final permisos=listaPermisos.data!;
        return permisos;
      }else{
        return [];
      }
    } catch (e) {
      return null;
    }
  }

}
