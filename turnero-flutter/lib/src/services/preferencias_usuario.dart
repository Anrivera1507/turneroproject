import 'package:shared_preferences/shared_preferences.dart';

class PreferenciasUsuario{
  static final PreferenciasUsuario _instancia=new PreferenciasUsuario._internal();

  factory PreferenciasUsuario(){
    return _instancia;
  }
  PreferenciasUsuario._internal();

  late SharedPreferences _prefs;
  initPrefs() async{
    this._prefs=await SharedPreferences.getInstance();
  }

  bool get bluetoothConectado{
    return _prefs.getBool("bluetoothConectado")??false;
  }
  set bluetoothConectado(bool val){
    _prefs.setBool("bluetoothConectado", val);
  }

  String get ultimaPagina{
    return _prefs.getString("ultimaPagina")??'login-page';
  }

  set ultimaPagina(String val){
    _prefs.setString("ultimaPagina", val);
  }

  //usuario logueado
  String get usuario{
    return _prefs.getString("usuario")??'';
  }

  set usuario(String val){
    _prefs.setString("usuario", val);
  }

  String get idUsuario{
    return _prefs.getString("idUsuario")??'-1';
  }

  set idUsuario(String val){
    _prefs.setString("idUsuario", val);
  }

  String get token{
    return _prefs.getString("token")??'';
  }

  set token(String val){
    _prefs.setString("token", val);
  }

  String get versionAndroid{
    return _prefs.getString("versionAndroid")??'';
  }

  set versionAndroid(String val){
    _prefs.setString("versionAndroid", val);
  }

  String get idSucursal{
    return _prefs.getString("idSucursal")??'-1';
  }

  set idSucursal(String val){
    _prefs.setString("idSucursal", val);
  }

}