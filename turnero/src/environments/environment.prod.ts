export const environment = {
  production: true,
  urlApi : "https://apis.inabve.gob.sv/v1/api_sivetweb",
  urlSocket : "https://socket.inabve.gob.sv",
  urlServer : "https://core.inabve.gob.sv",
  urlSocketServer : 'http://192.168.3.24:3001'
};
