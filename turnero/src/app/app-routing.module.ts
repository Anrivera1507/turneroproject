import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ConfigTurnosComponent } from './components/config-turnos/config-turnos.component';
// import { LogueoClienteComponent } from './components/logueo-cliente/logueo-cliente.component';
import { TurneroComponent } from './components/turnero/turnero.component';

const routes: Routes = [
  {
    path: '', component: ConfigTurnosComponent,
    // children: [
    //   // { path: 'logueo-usuario', component: LogueoClienteComponent },
    //   { path: 'monitor-turnos', component: TurneroComponent }
    // ]
  },
  { path: 'turnero', component: ConfigTurnosComponent },
  { path: 'monitor-turnos', component: TurneroComponent },
  { path: 'turnero/monitor-turnos', component: ConfigTurnosComponent },
  
  {
    path: '**', component: ConfigTurnosComponent
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
