export class TicketModel
{
    id              : number
    idEstado        : number
    idArea          : number
    idTecnico       : number 
    idVeterano      : number 
    codigo          : string
    numeroLlamada   : number
    fechaInicio     : Date
    fechaFin        : Date
    escritorio      : number 
    creadoEn        : Date
    creadoPor       : Date 
    modificadoEn    : Date 
    modificadoPor   : number
    activo          : boolean
    nombreCompleto?  : string 
    carnet?          : string =''
    estado          : string
    area            : string 

    constructor(){
        this.id=-1
        this.escritorio=0
        this.activo=true
        this.idArea=0
        this.idTecnico=0
        this.modificadoEn=new Date()
        this.modificadoPor=-1
        this.area=''
        this.carnet=''
        this.codigo=''
        this.idEstado=-1
        this.idVeterano=-1
        this.numeroLlamada=0
        this.fechaInicio=new Date()
        this.fechaFin=new Date()
        this.creadoEn=new Date()
        this.creadoPor=new Date()
        this.estado=''
        
    }
}