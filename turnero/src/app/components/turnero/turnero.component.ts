import Swal from "sweetalert2";
import { DOCUMENT } from "@angular/common";
import {
  Component,
  OnInit,
  OnDestroy,
  ViewChild,
  ElementRef,
  ChangeDetectorRef,
  AfterViewInit,
  DoCheck,
  Inject,
} from "@angular/core";
import { Router } from "@angular/router";
import { AppComponent } from "src/app/app.component";
import { TurnosService } from "../../services/turnos.service";
import { Ticket } from "./interfaces/turnero.interface";
import { Socket } from "ngx-socket-io";
import { TicketModel } from "src/app/models/turnos.model";
import { socket } from "src/app/utils/constants";

import { faExpand } from "@fortawesome/free-solid-svg-icons";
import { faCompress } from "@fortawesome/free-solid-svg-icons";
import { environment } from "src/environments/environment";

@Component({
  selector: "app-turnero",
  templateUrl: "./turnero.component.html",
  styleUrls: ["./turnero.component.css"],
})
export class TurneroComponent implements OnInit, OnDestroy, AfterViewInit {
  faExpand = faExpand;
  faCompress = faCompress;
  cuenta: number = 1;
  cuentaLlamada: any = 0;
  obj: object = {};
  blinkColor: string = "green";
  blinkTurno: boolean = false;
  showInDisplay: boolean = false;
  isLoading: number = 0;
  codigoArea: number = Number(localStorage.getItem("codigoArea"));
  codigoSucursal: number = Number(localStorage.getItem("codigoSucursal"));
  elem: any;
  pantallaCompleta: boolean = false;
  voices: any;
  _enviroment: any = environment.production;
  
  @ViewChild("video") video!: ElementRef;

  constructor(
    public app: AppComponent,
    private turnosServ: TurnosService,
    private router: Router,
    private changeDetection: ChangeDetectorRef,
    @Inject(DOCUMENT) private document: any
  ) // public socket : Socket
  {}

  ngOnInit(): void {
    this.iniciarTurnero();
    this.listaTickets = [];
    this.isLoading = 0;
    this.turnosServ.conectarSocket();
    let socketListener = socket.hasListeners(`speak_new${this.codigoSucursal}`);
    if (!socketListener) {
      this.volverALlamar();
    }
    this.elem = document.documentElement;
  }

  iniciarTurnero() {
    Swal.fire({
      title: "Bienvenido",
      text: "Desea iniciar turnero",
      icon: "info",
      showCancelButton: false,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Iniciar",
    }).then((result) => {
      if (result.isConfirmed) {
        this.video.nativeElement.play();
      } else {
        this.video.nativeElement.play();
      }
    });
  }

  ngOnDestroy(): void {
    socket.disconnect();
  }

  ngAfterViewInit(): void {
    // console.log(`Sockettt URL  ${socket}`);
    this.cuentaLlamada = this.obtenerTicketsSocket(); //Lista de tickets obntenidos por el socket
    this.voices = window.speechSynthesis.getVoices();
  }
  volverALlamar() {
    let sucursal = localStorage.getItem("codigoSucursal");
    // console.log("ENTRAMOS");
    socket.on(`speak_new${sucursal}`, (ticket: TicketModel) => {
      // if (localStorage.getItem("codigoArea") != null) {
      //   //llamar solo los ticket del area
      //   if (Number(localStorage.getItem("codigoArea")) == ticket.idArea) {
      //     this.llamada(ticket);
      //   }
      // } else {
      //   // llamar a todos
      //   // this.llamada(ticket);
      // }
       

        /**
         * Modificacion Temporal para habilitar la ventanilla para todas las areas
         */
         this.llamada(ticket);      

        /**
         * Modificacion Temporal para habilitar la ventanilla para todas las areas
         */
    });
  }

  listaTickets: TicketModel[] = [];
  ticketModel: TicketModel = new TicketModel();
  display: boolean = true;

  llamada(item: TicketModel) {
    this.voices = window.speechSynthesis.getVoices();
    //console.log(this.listaTickets);
    let regex = /(\d+)/g;
    let cadena = item.codigo;
    // console.log(item);
    //console.log(item.descripcion);
    //console.log( cadena.replace(/^(0+)/g, ''))

    // let nombre_completo=(item.nombre_completo==null || item.nombre_completo == undefined)?item.nombre_completo:item.nombreCompleto;

    let nombre_completo = item.nombreCompleto!.toString();
    nombre_completo =
      item.idVeterano == -1 || item.idVeterano == -1
        ? item.codigo.toString()
        : item.nombreCompleto!.toString();
    item.nombreCompleto = this.capitalize(nombre_completo);

    let inicialCodigo = "";
    if (item.idVeterano == -1 || item.idVeterano == -1) {
      inicialCodigo = nombre_completo.slice(0, 3);
      nombre_completo = nombre_completo.slice(3);
    }
    let msg: any;

    if (inicialCodigo != "") {
      if (!nombre_completo.includes("00")) {
        if (inicialCodigo.startsWith("R")) {
          msg = new SpeechSynthesisUtterance(
            //`Número .... ${item.codigo } ,..... pase a ventanilla ..., ${ item.escritorio }`
            `R${inicialCodigo.toLowerCase()},....${+nombre_completo} ,..... en el escritorio ....... ${
              item.escritorio
            }`
          );
          this.changeDetection.detectChanges();
        } else {
          if (inicialCodigo.startsWith("R")) {
            msg = new SpeechSynthesisUtterance(
              //`Número .... ${item.codigo } ,..... pase a ventanilla ..., ${ item.escritorio }`
              `R${inicialCodigo.toLowerCase()},....${nombre_completo} ,..... en el escritorio ....... ${
                item.escritorio
              }`
            );
            this.changeDetection.detectChanges();
          } else {
            msg = new SpeechSynthesisUtterance(
              //`Número .... ${item.codigo } ,..... pase a ventanilla ..., ${ item.escritorio }`
              `${inicialCodigo.toLowerCase()},....${+nombre_completo} ,..... en el escritorio ....... ${
                item.escritorio
              }`
            );
            this.changeDetection.detectChanges();
          }
        }
      } else {
        if (inicialCodigo.startsWith("R")) {
          msg = new SpeechSynthesisUtterance(
            //`Número .... ${item.codigo } ,..... pase a ventanilla ..., ${ item.escritorio }`
            `R${inicialCodigo.toLowerCase()},....${nombre_completo} ,..... en el escritorio ....... ${
              item.escritorio
            }`
          );
          this.changeDetection.detectChanges();
        } else {
        msg = new SpeechSynthesisUtterance(
          //`Número .... ${item.codigo } ,..... pase a ventanilla ..., ${ item.escritorio }`
          `${inicialCodigo.toLowerCase()},....${nombre_completo} ,..... en el escritorio ....... ${
            item.escritorio
          }`
        );
        this.changeDetection.detectChanges();}
      }
    } else {
      msg = new SpeechSynthesisUtterance(
        `${nombre_completo} ,..... en el escritorio ....... ${item.escritorio}`
      );
    }
    msg.lang = "es-MX";
    msg.voice = this.voices.filter(function (voice: any) {
      return voice.name == "Juan";
    })[0];
    // let foundVoices = this.voices.filter(function(v:any)
    // {
    //     return v.name == 'Juan';
    // });

    // if(foundVoices.length === 1){
    //   msg.voice = foundVoices[0];
    // }
    msg.rate = 0.9;
    msg.pitch = 1;
    // window.speechSynthesis.getVoices().filter(function(voice) { return voice.name == "Juan"});
    window.speechSynthesis.speak(msg);

    this.ticketModel = item;
    // if (localStorage.getItem("codigoArea") != null) {
    //   //llamar solo los ticket del area
    //   if (Number(localStorage.getItem("codigoArea")) == item.idArea) {
    //     this.mostrarAlert(this.ticketModel);
    //     // console.log(this.listaTickets);
    //   }
    // } else {
    //   //llamar a todos
    //   this.mostrarAlert(this.ticketModel);
    // }

    /**
     * Modificacion temporal para habilitar turnero para todas las areas
     */
      //llamar a todos
      this.mostrarAlert(this.ticketModel);
    /**
     * Modificacion temporal para habilitar turnero para todas las areas
     */
  }

  blinkColorAndTime() {
    let _this = this;
    if (this.showInDisplay) {
      setTimeout(() => {
        _this.blinkTurno = !_this.blinkTurno;
        this.blinkColorAndTime();
      }, 500);
    }
  }

  // obtenerVentanilla ( codigoArea : string ){
  //   this.turnosServ.obtenerVentanilla( codigoArea ).subscribe(
  //     resp => {
  //       console.log(resp.data);
  //       this.listaTickets = resp.data
  //     },
  //     (err) => {
  //       console.log("Error");
  //     },()=>{

  //     })
  // }
  mostrarAlert(ticket: TicketModel) {
    // this.video.nativeElement.pause();
    this.video.nativeElement.muted = true;
    Swal.fire({
      timer: 6000,
      width: ticket.nombreCompleto!.toString().length < 20 ? 900 : 1300,
      timerProgressBar: true,
      //icon: 'success',
      //title: ticket.nombreCompleto,
      //text: ticket.codigo,
      showConfirmButton: false,
      html:
        '<div class="sidebarGroup" >' +
        '<div class="fw-bold turnoTittle" id="btn_turno" >' +
        '<p style="font-size: 70px !important; color:black;">' +
        ticket.nombreCompleto +
        "</p>" +
        "<br>" +
        '<p style="font-size: 60px !important; color:black;">' +
        ticket.codigo +
        "</p>" +
        "<br>" +
        '<p style="font-size: 50px !important; color:black;">' +
        "ESCRITORIO " +
        ticket.escritorio +
        "</p>" +
        "</div></div>",
      background: "#fff",
      backdrop: `
                    rgba(0,0,0,0.7)
                    left top
                    no-repeat
                  `,
    });
    setTimeout(() => {
      // this.video.nativeElement.play();
      this.video.nativeElement.muted = false;
    }, 6000);
  }

  cerrar() {
    localStorage.clear();
    sessionStorage.clear();
    clearInterval(this.cuentaLlamada);
    socket.disconnect();
    this.router.navigate([""]);
  }

  iniciarSocket() {
    socket.connect();
  }

  obtenerTicketsSocket() {
    let sucursalInfo = {
      sucursal: this.codigoSucursal,
      area: this.codigoArea,
    };
    let sucursal = localStorage.getItem("codigoSucursal");
    let ticketFiltrados: TicketModel[] = [];
    socket.emit("solicitudTickets", sucursalInfo); // emitimos evento de solicitud de tickets por sucursal
    // socket.on(
    //   `f00718c080bce840d24b68ada2c2f3edf842996c64d1b2c63035f3c7a0046a57${this.codigoSucursal}${this.codigoArea}`,
    //   (lista: any) => {
      // this.changeDetection.detectChanges();
      //   if (localStorage.getItem("codigoArea") != null) {
      //     //filtramos los ticket segun el codigo de area
      //     ticketFiltrados = lista.data;
      //     ticketFiltrados = ticketFiltrados.filter(
      //       (x) => x.idArea == Number(localStorage.getItem("codigoArea"))
      //     );
      //   } else {
      //     //Seran todos los tickets de forma global
      //     ticketFiltrados = lista.data;
      //   }
      /**
       * Modificacion temporal para ventanilla que escuche todas las areas
       */
    socket.on(
      `f00718c080bce840d24b68ada2c2f3edf842996c64d1b2c63035f3c7a0046a57${this.codigoSucursal}`,
      (lista: any) => {
        /**
       * Modificacion temporal para ventanilla que escuche todas las areas
       */
       // this.changeDetection.detectChanges();
          //Seran todos los tickets de forma global
          this.changeDetection.detectChanges();
          ticketFiltrados = lista.data;
        if (this.listaTickets.length <= ticketFiltrados.length) {
          this.listaTickets = ticketFiltrados;
          this.changeDetection.detectChanges();
        }
      }
    );
  }

  capitalize(text: string) {
    text = text.toLowerCase().toString();

    const arr = text.split(" ");
    for (let index = 0; index < arr.length; index++) {
      arr[index] = arr[index].charAt(0).toUpperCase() + arr[index].slice(1);
    }
    const str2 = arr.join(" ");
    return str2;
  }
  openFullscreen() {
    if (this.elem.requestFullscreen) {
      this.elem.requestFullscreen();
      this.pantallaCompleta = true;
    } else if (this.elem.mozRequestFullScreen) {
      /* Firefox */
      this.elem.mozRequestFullScreen();
      this.pantallaCompleta = true;
    } else if (this.elem.webkitRequestFullscreen) {
      /* Chrome, Safari and Opera */
      this.elem.webkitRequestFullscreen();
      this.pantallaCompleta = true;
    } else if (this.elem.msRequestFullscreen) {
      /* IE/Edge */
      this.elem.msRequestFullscreen();
      this.pantallaCompleta = true;
    }
  }
  /* Close fullscreen */
  closeFullscreen() {
    if (this.document.exitFullscreen) {
      this.document.exitFullscreen();
      this.pantallaCompleta = false;
    } else if (this.document.mozCancelFullScreen) {
      /* Firefox */
      this.document.mozCancelFullScreen();
      this.pantallaCompleta = false;
    } else if (this.document.webkitExitFullscreen) {
      /* Chrome, Safari and Opera */
      this.document.webkitExitFullscreen();
      this.pantallaCompleta = false;
    } else if (this.document.msExitFullscreen) {
      /* IE/Edge */
      this.document.msExitFullscreen();
      this.pantallaCompleta = false;
    }
  }
}
