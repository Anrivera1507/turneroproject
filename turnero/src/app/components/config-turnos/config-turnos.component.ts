import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TurnosService } from 'src/app/services/turnos.service';
import { AreasAtencion } from '../turnero/interfaces/turnero.interface';
import Swal from 'sweetalert2';
//import { Socket } from 'ngx-socket-io';
import { socket, Sucursales, _Crud } from 'src/app/utils/constants';

@Component({
  selector: 'app-config-turnos',
  templateUrl: './config-turnos.component.html',
  styleUrls: ['./config-turnos.component.css']
})
export class ConfigTurnosComponent implements OnInit {

  constructor(
    private turnoServ : TurnosService,
    private router    : Router,
   // private socket : Socket
  ) { }

  ngOnInit(): void {
    this.cargarAreasAtencion();
    this.obtenerSucursales();
    localStorage.removeItem('codigoSucursal');
    localStorage.removeItem('codigoArea');
  }

  AreasAtencion : AreasAtencion [] = [];
  codigoArea : string = '';
  sucursales = [];
  mostrarAreas = false;
  nombreSucursal:string = ''
  cargarAreasAtencion(){
    this.turnoServ.obtenerAreasAtencion().subscribe(
      (resp) =>{
        this.AreasAtencion = resp
        console.log(this.AreasAtencion);
      },
      (err) => {
        console.log(err);
      }
    )
  }


  guardarSucursal(item:any){
    this.mostrarAreas = true;
    localStorage.setItem('codigoSucursal', item.id)
    this.nombreSucursal = item.descripcion;
  }

  guardarAreaAtencion( item? :any){ 
    if(item==null){
      //Son todas las areas de forma global
      //localStorage.clear()
      socket.disconnect();
      this.redirigirMonitor();
    }else{
      localStorage.setItem('codigoArea', item.id)
      this.codigoArea = localStorage.getItem('codigoArea') !
      if(this.codigoArea != ''){
        socket.disconnect();
        this.redirigirMonitor( item.descripcion )
      }
    }
  }

  redirigirMonitor( descripcion? : string ){
    let _this = this;
    Swal.fire({
      title:'Aviso',
      text: `Seguro de ingresar a turno del area ${ descripcion!=null?descripcion:'TODAS LAS AREAS' }`,
      showCancelButton: true,
      confirmButtonText:'Sí',
      cancelButtonText:'No, cancelar',
      confirmButtonColor: 'darkgreen',
      cancelButtonColor: '#808080',
    }).then(function ( resp )
    {
      if( resp.value ){
        socket.disconnect();
        _this.router.navigate(["/monitor-turnos"]);
        Swal.fire({
          title:'Bienvenid@!',
          icon:'success',
          confirmButtonText:'Entendido',
          confirmButtonColor: 'darkgreen'
        }).then(function ()
        {
          console.log();
              
        })
      }
          
    })
    
  }


  obtenerSucursales() {
    this.turnoServ.obtenerPorTipos(Sucursales).subscribe(sucursal => {
      if (sucursal.message == _Crud.CRUD200) {
        this.sucursales = sucursal.data
      } else {
        this.sucursales = []
      }
    }, err => {
      this.sucursales = []
    })
  }
}
