import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LogueoClienteComponent } from './logueo-cliente.component';

describe('LogueoClienteComponent', () => {
  let component: LogueoClienteComponent;
  let fixture: ComponentFixture<LogueoClienteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LogueoClienteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LogueoClienteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
