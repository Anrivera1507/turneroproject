import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TurneroComponent } from './components/turnero/turnero.component';
import { HttpClientModule} from '@angular/common/http';
import { ConfigTurnosComponent } from './components/config-turnos/config-turnos.component'
import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';
import { environment } from 'src/environments/environment';
import { FormsModule } from '@angular/forms';
import {VgCoreModule} from '@videogular/ngx-videogular/core';
import {VgControlsModule} from '@videogular/ngx-videogular/controls';
import {VgOverlayPlayModule} from '@videogular/ngx-videogular/overlay-play';
import {VgBufferingModule} from '@videogular/ngx-videogular/buffering';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

const config: SocketIoConfig = { url: environment.urlSocket,  options: {transports : ['websocket'], path: '/socket/socket.io'} };

@NgModule({
  declarations: [
    AppComponent,
    TurneroComponent,
    ConfigTurnosComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    SocketIoModule.forRoot(config),
    VgCoreModule,
    VgControlsModule,
    VgOverlayPlayModule,
    VgBufferingModule,
    FontAwesomeModule
  ],
  exports:[
    TurneroComponent,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
