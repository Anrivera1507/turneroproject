import { io } from "socket.io-client"
import { environment } from "src/environments/environment";

export const ImgAreasAtencion = [
    { IDAREA : '14066', CODIGO : 'C', IMGSRC : '../../../assets/images/creditos-img.svg' },
    { IDAREA : '14067', CODIGO : 'S', IMGSRC : '../../../assets/images/sicme-image.svg' },
    { IDAREA : '14068', CODIGO : 'P', IMGSRC : '../../../assets/images/cinabve-img.svg' },
    { IDAREA : '14069', CODIGO : 'V', IMGSRC : '../../../assets/images/becas-img.svg' },
    { IDAREA : '14070', CODIGO : 'R', IMGSRC : '../../../assets/images/registros-imgs.svg' },
]

// export const socket = io('https://apis.inabve.gob.sv/api_sivetweb_tmp',{
//        path: '/socket/socket.io',
//        transports: ['websocket']
//      });
export const socket = io(environment.urlSocket,{
       path: '/socket/socket.io',
       transports: ['websocket']
     });


     export const Sucursales = "sucursal";

     export enum _Crud
{
	CRUD400	= 'NoResult',
	CRUD401	= 'AlreadyExist',
	CRUD402	= 'NotFound',
	CRUD403	= 'NoParams',
	CRUD200	= 'ResultData',
	CRUD201	= 'RecordCreated',
	CRUD202	= 'RecordUpdated',
	CRUD203	= 'RecordDeleted',
	CRUD204	= 'PasswordChanged'
}