import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable,BehaviorSubject } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { AreasAtencion } from '../components/turnero/interfaces/turnero.interface';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';
import { TicketModel } from '../models/turnos.model';
import { socket } from '../utils/constants';

@Injectable({
  providedIn: 'root'
})
export class TurnosService {
  
  constructor( 
    private http : HttpClient,
    private router : Router
    )
    { 
    }
    private _headers = new HttpHeaders({ 'content-type' : 'application/json'})
    
    private _areas : AreasAtencion = {
      id              : '',
      idRelacion      : '',
      tipo            : '',
      codigo          : '',
      descripcion     : '',
      creadoEn        : '',
      creadoPor       : '',
      modificadoEn    : '',
      modificadoPor   : '',
      activo          : ''
    }

  conectarSocket(){
      // console.log(`Sockettt URL`);
      // console.log(socket);
      //let socket = this.socket;
      socket.connect();
      socket.on("connect", () => {
        // console.log(socket.id); //
        // console.log("Conectado al socket nuevo del cliente!!!!!!!"); //
    });
  }  

  desconectarSocket(){
    socket.disconnect();
  }

  cargarDatos(){
    const url = 'http://regres.in/api/users';
    return this.http.get( url );
  }

  public buscar(objParam: any): Observable<any> { 
    return this.http.post<any>(environment.urlApi+'/registros/veteranos/buscar',objParam, {'headers':this._headers})
    .pipe(
            map((resp: any) => { 
                return resp;
            }),
            catchError(err => {
                console.log(err)  
                return err;
            })
        )
  }

  public obtenerVentanilla( codigoArea : string ): Observable<any> { 
    return this.http.post<any>(environment.urlApi + '/ventanillas/tickets/monitor', { "id_area" : codigoArea }, {'headers' : this._headers})
    .pipe(
      map((resp: any) => { 
        return resp;
      }),
      catchError(err => {
        console.log(err)  
          return err;
          })
      )
  }

  obtenerAreasAtencion(){
    return this.http.post(environment.urlApi + "/administracion/catalogos/obtenerAreasAtencion", {"tipo":"areasAtencion"} ,{'headers': this._headers})
      .pipe(
        map((resp: any) => {          
          if (resp.data ) {
            return resp.data;
          } else {
            return null
          }
        }),
        catchError((err) => {
          return err ;
        })
      );
  }
  

  public obtenerPorTipos(tipo?: string): Observable<any> {
    return this.http.post(environment.urlApi + '/administracion/catalogos/obtenerSucursales', {tipo:tipo}, {'headers' : this._headers})
    .pipe(
          map((resp: any) => {
              return resp;
          }),
          catchError(err => {
              console.log(err)
              return err;
          })
        )
  }


}
