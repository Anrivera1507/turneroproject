import { Component, OnInit } from '@angular/core';
import { ConfigService } from '../services/config.service';
import Swal from 'sweetalert2'; 
import { Router } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './app.login.component.html',
})
export class AppLoginComponent implements OnInit {

  userObj = {
    user: '',
    password: ''
  }

  constructor(
    public _configService: ConfigService,
    public router: Router,
  ){

  }

  ngOnInit(): void {
    let authenticated = +localStorage.getItem('k1')
    // console.log('Autenticado', authenticated); 
    if (authenticated == 1) {
      this.router.navigate(["/veteranos"]);
    } 
  }
    

  authenticate(){
    this._configService.authentication(this.userObj.user, this.userObj.password).subscribe(resp => {
      console.log(resp);
      if (resp) {
        this.router.navigate(["/veteranos"]);   
        Swal.fire({
          title:'Bienvenid@!',
          text:'Ya puedes ingresar registros de sobrevivencia en la nueva plataforma', 
          icon:'success',
          confirmButtonText:'Entendido',
          confirmButtonColor: 'darkgreen'
        }).then(function (result) 
        {})
      } else {
          Swal.fire({
            title:'Aviso',
            text:'Usuario o contraseña inválidos', 
            icon:'warning',
            confirmButtonText:'Entendido',
            confirmButtonColor: '#3085D6'
          }).then(function (result) 
          {})
      }
    } );
    
  }
 
}











