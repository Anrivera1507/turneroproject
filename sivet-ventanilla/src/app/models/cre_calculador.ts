export class CreCalculador
{
	importePrestamo: number;
    tasaInteres: number;
    periodo: number;
    fechaInicio: Date;
    fondoPrevencion: number;
    numPagos: number;
    pagoMensual: number;
    prestamoMasInteres: number;
    costoTotal: number;

	constructor()
	{
        this.importePrestamo = 0;
        this.tasaInteres = 0;
        this.periodo = 1;
        this.fechaInicio = new Date();
        this.fondoPrevencion = 0;
        this.numPagos = 0;
        this.pagoMensual = 0;
        this.prestamoMasInteres = 0;
        this.costoTotal = 0;
	}

}