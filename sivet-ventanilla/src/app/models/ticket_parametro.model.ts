export class TicketParametro{
    pendiente:boolean | null 
    idArea:number | null
    idSucursal? : number | null

    constructor() {
        this.idArea = null
        this.pendiente = null
        this.idSucursal = null
    }
}