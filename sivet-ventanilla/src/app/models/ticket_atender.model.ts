export class TicketAtenderModel{
    id:number
    modificadoPor:number
    atendido:boolean | null

    constructor(){
        this.atendido=null,
        this.id=-1,
        this.modificadoPor=-1
    }
}