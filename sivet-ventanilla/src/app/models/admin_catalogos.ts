export class adminCatalogosModel
{
	id : number
	idRelacion : number
	tipo : string
	codigo : string
	descripcion : string
	creadoEn : Date
	creadoPor : number
	modificadoEn : Date
	modificadoPor : number
	activo : boolean

	constructor()
	{
		this.id = -1
		this.idRelacion = -1
		this.tipo = ''
		this.codigo = ''
		this.descripcion = ''
		this.creadoEn = new Date()
		this.creadoPor = -1
		this.modificadoEn = new Date()
		this.modificadoPor = -1
		this.activo = true
	}
}