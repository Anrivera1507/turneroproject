export class VentanillaModel {
    cantidad:    number;
    nombre_area: string;
    ventanillas: string[];
    idArea:number;

    constructor(){
        this.cantidad=0
        this.nombre_area=''
        this.ventanillas=[]
        this.idArea=0
    }
}