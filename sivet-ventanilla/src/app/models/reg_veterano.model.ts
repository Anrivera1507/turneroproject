export class RegVeteranosModel
{
	id 		 			: number
	idSector 			: number
	idEstado 			: number
	carnet 				: string
	nombres 			: string
	apellidos 			: string
	sexo 				: string
	dui 				: string
	nit 				: string | null
	cip 				: string | null
	fechaEmision 		: Date | null
	fechaNacimimento    : Date
	telefono 			: string | null
	celular1 			: string | null
	celular2 			: string | null
	correo	 			: string

	pensionable  		: boolean
	foprolyd 			: boolean
	ipsfa 				: boolean
	afp 				: boolean
	inpep 				: boolean
	universal 			: boolean

	creadoEn 			: Date
	creadoPor 			: number
	modificadoEn 		: Date
	modificadoPor 		: number
	sector?				: string
	estado?				: string
    porcentajeDeuda 	: number 
	grupoFamiliar 		: []
	sobrevivencias 		: []
	sondeos		 		: []
	edad?:number
 
	constructor()
	{
		this.id = -1
		this.idSector = -1
		this.idEstado = -1
		this.carnet = ''
		this.nombres = ''
		this.apellidos = ''
		this.sexo = ''
		this.dui = ''
		this.nit = ''
		this.cip = ''
		this.fechaEmision = new Date()
		this.fechaNacimimento = new Date()
		this.telefono 			= ''
		this.celular1 			= ''
		this.celular2 			= ''
		this.correo   			= ''

		this.pensionable  		= false
		this.foprolyd 			= false
		this.ipsfa 				= false
		this.afp 				= false
		this.inpep 				= false
		this.universal 			= false

		this.creadoEn 			= new Date()
		this.creadoPor 			= -1
		this.modificadoEn 		= new Date()
		this.modificadoPor 		= -1
		this.sector 			= ''
		this.estado 			= ''
		this.porcentajeDeuda 	= -1
	}
}