export class TicketModel
{
    id              : number
    idEstado        : number
    idArea          : number
    idTecnico       : number 
    idVeterano      : number
    idSucursal      : number 
    codigo          : string
    numeroLlamada   : number
    fechaInicio     : Date
    fechaFin        : Date
    escritorio      : number 
    creadoEn        : Date
    creadoPor       : Date 
    modificadoEn    : Date 
    modificadoPor   : number
    activo          : boolean
    nombreCompleto?  : string =''
    carnet?          : string =''
    estado          : string
    area            : string 

    constructor(){
        this.id=-1
        this.escritorio=0
        this.activo=true
        this.idArea=0
        this.idTecnico=0
        this.idSucursal = 0
        this.modificadoEn=new Date()
        this.modificadoPor=-1
        this.area=''
        this.carnet=''
        this.codigo=''
        
    }
}