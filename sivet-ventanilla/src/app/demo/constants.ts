export const VetEstructura = [
    { TIPO_REGISTRO : 'FAES', CODIGO_ESTRUCTURA : 1, DESCRIPCION_ESTRUCTURA : 'Aerea' },
    { TIPO_REGISTRO : 'FAES', CODIGO_ESTRUCTURA : 2, DESCRIPCION_ESTRUCTURA : 'Naval' },
    { TIPO_REGISTRO : 'FAES', CODIGO_ESTRUCTURA : 3, DESCRIPCION_ESTRUCTURA : 'Ejercito' },
    { TIPO_REGISTRO : 'FMLN', CODIGO_ESTRUCTURA : 1, DESCRIPCION_ESTRUCTURA : 'Militar' },
    { TIPO_REGISTRO : 'FMLN', CODIGO_ESTRUCTURA : 2, DESCRIPCION_ESTRUCTURA : 'Politica' },
    { TIPO_REGISTRO : 'FMLN', CODIGO_ESTRUCTURA : 3, DESCRIPCION_ESTRUCTURA : 'De Apoyo' },
    { TIPO_REGISTRO : 'FMLN', CODIGO_ESTRUCTURA : 4, DESCRIPCION_ESTRUCTURA : 'En el Exterior' },
    { TIPO_REGISTRO : 'FMLN', CODIGO_ESTRUCTURA : 5, DESCRIPCION_ESTRUCTURA : 'De Masas' },
    { TIPO_REGISTRO : 'FMLN', CODIGO_ESTRUCTURA : 6, DESCRIPCION_ESTRUCTURA : 'Inteligencia' }
]

export const VetFaesDestacamento = [
    { CODIGO_DESTACAMENTO :1, NOMBRE_DESTACAMENTO  : 'Servicio Territorial (Escolta Militar, Patrulleros, Defensa Civil)' },
    { CODIGO_DESTACAMENTO :2, NOMBRE_DESTACAMENTO  : '2ª. Brigada de Infantería' },
    { CODIGO_DESTACAMENTO :3, NOMBRE_DESTACAMENTO  : '3ª. Brigada de Infantería' },
    { CODIGO_DESTACAMENTO :4, NOMBRE_DESTACAMENTO  : '4ª. Brigada de infantería' },
    { CODIGO_DESTACAMENTO :5, NOMBRE_DESTACAMENTO  : '5ª. Brigada de Infantería' },
    { CODIGO_DESTACAMENTO :6, NOMBRE_DESTACAMENTO  : '6ª. Brigada de Infantería' },
    { CODIGO_DESTACAMENTO :7, NOMBRE_DESTACAMENTO  : 'DM-1' },
    { CODIGO_DESTACAMENTO :8, NOMBRE_DESTACAMENTO  : 'DM-2' },
    { CODIGO_DESTACAMENTO :9, NOMBRE_DESTACAMENTO  : 'DM-3' },
    { CODIGO_DESTACAMENTO :10, NOMBRE_DESTACAMENTO  : 'DM-4' },
    { CODIGO_DESTACAMENTO :11, NOMBRE_DESTACAMENTO  : 'DM-5' },
    { CODIGO_DESTACAMENTO :12, NOMBRE_DESTACAMENTO  : 'DM-6' },
    { CODIGO_DESTACAMENTO :13, NOMBRE_DESTACAMENTO  : 'DM-7' },
    { CODIGO_DESTACAMENTO :14, NOMBRE_DESTACAMENTO  : 'DM-9' },
    { CODIGO_DESTACAMENTO :15, NOMBRE_DESTACAMENTO  : 'Brigada de Artillería' },
    { CODIGO_DESTACAMENTO :16, NOMBRE_DESTACAMENTO  : 'Regimiento de Caballería' },
    { CODIGO_DESTACAMENTO :17, NOMBRE_DESTACAMENTO  : 'CIFA' },
    { CODIGO_DESTACAMENTO :18, NOMBRE_DESTACAMENTO  : 'CATFA' },
    { CODIGO_DESTACAMENTO :19, NOMBRE_DESTACAMENTO  : 'CFE' },
    { CODIGO_DESTACAMENTO :20, NOMBRE_DESTACAMENTO  : 'BESM' },
    { CODIGO_DESTACAMENTO :21, NOMBRE_DESTACAMENTO  : 'DGRR' },
    { CODIGO_DESTACAMENTO :22, NOMBRE_DESTACAMENTO  : 'COSAM' },
    { CODIGO_DESTACAMENTO :23, NOMBRE_DESTACAMENTO  : 'CALFA' },
    { CODIGO_DESTACAMENTO :24, NOMBRE_DESTACAMENTO  : 'EMGFA' },
    { CODIGO_DESTACAMENTO :25, NOMBRE_DESTACAMENTO  : 'EMGFN' },
    { CODIGO_DESTACAMENTO :26, NOMBRE_DESTACAMENTO  : 'BIRIS (ATLACATL, BELLOSO, BRACAMONTE, ARCE, ATONAL)' },
    { CODIGO_DESTACAMENTO :27, NOMBRE_DESTACAMENTO  : 'CUSEP (P.N, GN,PH)' },
    { CODIGO_DESTACAMENTO :28, NOMBRE_DESTACAMENTO  : '1a. BI.' },
    { CODIGO_DESTACAMENTO :29, NOMBRE_DESTACAMENTO  : 'Estado Mayor Conjunto' }
]

export const VetSectorLaboral = [
    { CODIGO_SECTOR_LABORAL : 1, DESCRIPCION : 'PUBLICO' },
    { CODIGO_SECTOR_LABORAL : 2, DESCRIPCION : 'PRIVADO' },
    { CODIGO_SECTOR_LABORAL : 3, DESCRIPCION : 'INFORMAL' }
]
// ---------
export const VetRangoIngresos = [

    { CODIGO_INGRESO: 1, DESCRIPCION: 'Entre $0 y $50' },
    { CODIGO_INGRESO: 2, DESCRIPCION: 'Entre $51 y $100' },
    { CODIGO_INGRESO: 3, DESCRIPCION: 'Entre $101 y $300' },
    { CODIGO_INGRESO: 4, DESCRIPCION: 'Entre $301 y $500' },
    { CODIGO_INGRESO: 5, DESCRIPCION: 'Entre $501 y $700' },
    { CODIGO_INGRESO: 6, DESCRIPCION: 'Arriba de $701' }
]

export const VetRangoRemesas = [
    { CODIGO_REMESA: 6, DESC_RANGO: 'Menos de $100' },
    { CODIGO_REMESA: 6, DESC_RANGO: 'Mas de $100' },
    { CODIGO_REMESA: 6, DESC_RANGO: 'Mas de $200' },
    { CODIGO_REMESA: 6, DESC_RANGO: 'Mas de $300' },
    { CODIGO_REMESA: 6, DESC_RANGO: 'Mas de $400' }
]

export const VetTipoDiscapacidad = [
    { CODIGO_DISCAPACIDAD : 1, NOMBRE_DISCAPACIDAD : "Caminar, moverse, subir o bajar"},
    { CODIGO_DISCAPACIDAD : 2, NOMBRE_DISCAPACIDAD : "Ver, aun usando lentes"},
    { CODIGO_DISCAPACIDAD : 3, NOMBRE_DISCAPACIDAD : "Hablar, comunicarse o conversar"},
    { CODIGO_DISCAPACIDAD : 4, NOMBRE_DISCAPACIDAD : "Oír, aun usando aparato auditivo"},
    { CODIGO_DISCAPACIDAD : 5, NOMBRE_DISCAPACIDAD : "Vestirse, bañarse o comer"},
    { CODIGO_DISCAPACIDAD : 6, NOMBRE_DISCAPACIDAD : "Poner atención o aprender cosas sencillas"},
    { CODIGO_DISCAPACIDAD : 7, NOMBRE_DISCAPACIDAD : "Tiene alguna limitación mental"},
    { CODIGO_DISCAPACIDAD : 8, NOMBRE_DISCAPACIDAD : "Otro"}
]

export const VetTenenciasVivencias = [
    { CODIGO_TENENCIA : 1, DESCRIIPCION_TENENCIA : "Inquilino (a)" },
    { CODIGO_TENENCIA : 2, DESCRIIPCION_TENENCIA : "Propietario(a) de la vivienda pero la está pagando a plazos" },
    { CODIGO_TENENCIA : 3, DESCRIIPCION_TENENCIA : "Propietario(a)" },
    { CODIGO_TENENCIA : 4, DESCRIIPCION_TENENCIA : "Propietario(a) de la vivienda en terreno público " },
    { CODIGO_TENENCIA : 5, DESCRIIPCION_TENENCIA : "Propietario(a) de la vivienda en terreno  privado" },
    { CODIGO_TENENCIA : 6, DESCRIIPCION_TENENCIA : "Colono(a)" },
    { CODIGO_TENENCIA : 7, DESCRIIPCION_TENENCIA : "Guardián de la vivienda" },
    { CODIGO_TENENCIA : 8, DESCRIIPCION_TENENCIA : "Ocupante gratuito" },
    { CODIGO_TENENCIA : 9, DESCRIIPCION_TENENCIA : "Otros" }
]

export const VetTiposViviendas = [
    { CODIGO_VIVIENDA : 1, DESCRIPCION: "Casa privada o independiente" },
    { CODIGO_VIVIENDA : 2, DESCRIPCION: "Apartamento" },
    { CODIGO_VIVIENDA : 3, DESCRIPCION: "Condominio" },
    { CODIGO_VIVIENDA : 4, DESCRIPCION: "Pieza en casa" },
    { CODIGO_VIVIENDA : 5, DESCRIPCION: "Pieza en mesón" },
    { CODIGO_VIVIENDA : 6, DESCRIPCION: "Casa improvisada" },
    { CODIGO_VIVIENDA : 7, DESCRIPCION: "Rancho" },
    { CODIGO_VIVIENDA : 8, DESCRIPCION: "Temporal (Desastres naturales)" },
    { CODIGO_VIVIENDA : 9, DESCRIPCION: "Temporal (Desastres naturales)" },
    { CODIGO_VIVIENDA : 1, DESCRIPCION: "Otros" }
]

export const VetTiposTechos = [
    { CODIGO_TECHO : 1, DESCRIPCION_TECHO : "Losa de concreto" },
    { CODIGO_TECHO : 2, DESCRIPCION_TECHO : "Lámina de asbesto o fibra cemento" },
    { CODIGO_TECHO : 3, DESCRIPCION_TECHO : "Lámina de fibrocemento" },
    { CODIGO_TECHO : 4, DESCRIPCION_TECHO : "Teja de barro o cemento" },
    { CODIGO_TECHO : 5, DESCRIPCION_TECHO : "Lámina metálica" },
    { CODIGO_TECHO : 6, DESCRIPCION_TECHO : "Paja o palma" },
    { CODIGO_TECHO : 7, DESCRIPCION_TECHO : "Materiales de desecho" },
    { CODIGO_TECHO : 8, DESCRIPCION_TECHO : "Otros materiales" }
]
//----
export const VetAlumbrado = [
    { CODIGO_ALUMBRADO : 1, DESCRIPCION_ALUMBRADO : 'Electricidad (Conexión propia)' },
    { CODIGO_ALUMBRADO : 2, DESCRIPCION_ALUMBRADO : 'Conexión eléctrica del vecino' },
    { CODIGO_ALUMBRADO : 3, DESCRIPCION_ALUMBRADO : 'Conexión eléctrica de u familiar' },
    { CODIGO_ALUMBRADO : 4, DESCRIPCION_ALUMBRADO : 'Kerosene (gas)' },
    { CODIGO_ALUMBRADO : 5, DESCRIPCION_ALUMBRADO : 'Candela' },
    { CODIGO_ALUMBRADO : 6, DESCRIPCION_ALUMBRADO : 'Panel solar' },
    { CODIGO_ALUMBRADO : 7, DESCRIPCION_ALUMBRADO : 'Generador eléctrico' },
    { CODIGO_ALUMBRADO : 8, DESCRIPCION_ALUMBRADO : 'Otra clase' }
]

export const VetTituloVivienda = [
    { CODIGO_TITULO: 1, DESCRIPCION_TITULO: 'Registrada' },
    { CODIGO_TITULO: 2, DESCRIPCION_TITULO: 'No Registrada' },
    { CODIGO_TITULO: 3, DESCRIPCION_TITULO: 'En Proceso' },
    { CODIGO_TITULO: 4, DESCRIPCION_TITULO: 'Sin Posibilidad de Registro' }
]

export const VetAbastecimentoAgua = [
    { CODIGO_ABASTECIMIENTO: 1, DESCRIPCION_ABASTECIMIENTO: 'Cañería del vecino(a)' },
    { CODIGO_ABASTECIMIENTO: 2, DESCRIPCION_ABASTECIMIENTO: 'Pila, chorro público o cantarera' },
    { CODIGO_ABASTECIMIENTO: 3, DESCRIPCION_ABASTECIMIENTO: 'Camión carreta o pipa' },
    { CODIGO_ABASTECIMIENTO: 4, DESCRIPCION_ABASTECIMIENTO: 'Pozo con tubería privado' },
    { CODIGO_ABASTECIMIENTO: 5, DESCRIPCION_ABASTECIMIENTO: 'Pozo protegido (Cubierto)' },
    { CODIGO_ABASTECIMIENTO: 6, DESCRIPCION_ABASTECIMIENTO: 'Pozo no protegido ' },
    { CODIGO_ABASTECIMIENTO: 7, DESCRIPCION_ABASTECIMIENTO: 'Ojo de agua, río o quebrada' },
    { CODIGO_ABASTECIMIENTO: 8, DESCRIPCION_ABASTECIMIENTO: 'Manantial protegido' },
    { CODIGO_ABASTECIMIENTO: 9, DESCRIPCION_ABASTECIMIENTO: 'Manantial no protegido' },
    { CODIGO_ABASTECIMIENTO: 10, DESCRIPCION_ABASTECIMIENTO: 'Colecta agua lluvia' },
    { CODIGO_ABASTECIMIENTO: 11, DESCRIPCION_ABASTECIMIENTO: 'Agua envasada' },
    { CODIGO_ABASTECIMIENTO: 12, DESCRIPCION_ABASTECIMIENTO: 'Chorro común' },
    { CODIGO_ABASTECIMIENTO: 13, DESCRIPCION_ABASTECIMIENTO: 'Acarreo de cañería del vecino(a), de rio o poz' },
    { CODIGO_ABASTECIMIENTO: 14, DESCRIPCION_ABASTECIMIENTO: 'Otros medios' }
]

export const VetNivelEstudio = [
    { CODIGO_NIVEL: 0, NOMBRE_NIVEL_ESTUDIO: 'Educación Inicial' },
    { CODIGO_NIVEL: 1, NOMBRE_NIVEL_ESTUDIO: 'Parvularia (1º  a  3º)' },
    { CODIGO_NIVEL: 2, NOMBRE_NIVEL_ESTUDIO: 'Básica  (1º  a  9º)' },
    { CODIGO_NIVEL: 3, NOMBRE_NIVEL_ESTUDIO: 'Media  (10º  a  13º)' },
    { CODIGO_NIVEL: 4, NOMBRE_NIVEL_ESTUDIO: 'Superior universitario (1º  a  15º)' },
    { CODIGO_NIVEL: 4.1, NOMBRE_NIVEL_ESTUDIO: 'Curso de Nivelación (1°)' },
    { CODIGO_NIVEL: 5, NOMBRE_NIVEL_ESTUDIO: 'Superior no universitario (1º  a  3º)' },
    { CODIGO_NIVEL: 6, NOMBRE_NIVEL_ESTUDIO: 'Educación especial (ciclos I,II,III,IV)' },
    { CODIGO_NIVEL: 7, NOMBRE_NIVEL_ESTUDIO: 'Otros' },
    { CODIGO_NIVEL: 98, NOMBRE_NIVEL_ESTUDIO: 'Desconocido' },
    { CODIGO_NIVEL: 99, NOMBRE_NIVEL_ESTUDIO: '-' }
]
export const VetFuentesIngresos = [
    { CODIGO_FUENTES: 1, NOMBRE_FUENTE_INGRESO: 'Salario y otros ingresos del sector público' },
    { CODIGO_FUENTES: 2, NOMBRE_FUENTE_INGRESO: 'Salario y otros ingresos del sector privado' },
    { CODIGO_FUENTES: 3, NOMBRE_FUENTE_INGRESO: 'Sector agropecuario' },
    { CODIGO_FUENTES: 4, NOMBRE_FUENTE_INGRESO: 'Sector cooperativo' },
    { CODIGO_FUENTES: 5, NOMBRE_FUENTE_INGRESO: 'Negocio del hogar' },
    { CODIGO_FUENTES: 6, NOMBRE_FUENTE_INGRESO: 'Pensión' },
    { CODIGO_FUENTES: 7, NOMBRE_FUENTE_INGRESO: 'Remesa familiar del exterior' },
    { CODIGO_FUENTES: 8, NOMBRE_FUENTE_INGRESO: 'Ayuda familiar dentro del país' },
    { CODIGO_FUENTES: 9, NOMBRE_FUENTE_INGRESO: 'Transferencia del Gobierno' },
    { CODIGO_FUENTES: 10, NOMBRE_FUENTE_INGRESO: 'Transferencia de Alcaldía' },
    { CODIGO_FUENTES: 11, NOMBRE_FUENTE_INGRESO: 'Transferencia de ONG´s' },
    { CODIGO_FUENTES: 12, NOMBRE_FUENTE_INGRESO: 'Otros(renta de propiedades,intereses,dividendos,entre otros)' },
    { CODIGO_FUENTES: 98, NOMBRE_FUENTE_INGRESO: 'No Responde' },
    { CODIGO_FUENTES: 99, NOMBRE_FUENTE_INGRESO: 'No Sabe' },
]