import { Injectable } from '@angular/core'
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpResponse } from '@angular/common/http'
import { Observable } from 'rxjs'
import { map } from 'rxjs/operators'

@Injectable()
export class JwtInterceptor implements HttpInterceptor {

    constructor() {}

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>>
    {
        if (localStorage.getItem('token') || localStorage.getItem('token') !== null) {
            request = request.clone({
                setHeaders: {
                    Authorization: `Bearer ${localStorage.getItem('token')}`
                }
            })
        }
        return next.handle(request).pipe(map( (val: HttpEvent<any>) => {
            if (val instanceof HttpResponse)
            {
                // TODO: You can do anything with body response
                const body = val.body
            }
            return val
        }))
    }
}
