import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private router: Router){

  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): any {

        if(localStorage.getItem('token') || localStorage.getItem('token') !== null)
        {
            if (state.url == '/login') {
                this.router.navigate(['/'])
            }
            return true
        }
        else
        {
            localStorage.clear()
            this.router.navigate(['/login'])
        }
    }
}
