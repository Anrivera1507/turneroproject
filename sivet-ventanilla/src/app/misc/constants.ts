import { HttpHeaders } from "@angular/common/http";

export const header = new HttpHeaders({
    'content-type'                : 'application/json',
    'Authorization'               : "Bearer "+localStorage.getItem('token')
  })


export function convertirFechaReal(s) {
  let fecha = new Date(new Date(s).getTime() + (new Date(s).getTimezoneOffset() * 60 * 1000));
    return fecha
}

export const defaultDateTime: Date = convertirFechaReal('1900-01-01T00:00:00.000Z');

export function RetornarRuta(ruta) {
  return ruta + `/#/token/${localStorage.getItem('token')}/idUser/${localStorage.getItem('idUser')}/user/${localStorage.getItem('user')}`
}

export enum _Crud
{
	CRUD400	= 'NoResult',
	CRUD401	= 'AlreadyExist',
	CRUD402	= 'NotFound',
	CRUD403	= 'NoParams',
	CRUD200	= 'ResultData',
	CRUD201	= 'RecordCreated',
	CRUD202	= 'RecordUpdated',
	CRUD203	= 'RecordDeleted',
	CRUD204	= 'PasswordChanged'
}

export enum _Auth
{
	AUTH400	= "YouAreLost",
	AUTH401	= 'EmailPassRequired',
	AUTH402	= 'EmailPassWrong',
	AUTH403	= 'OldNewPassRequired',
	AUTH404	= 'SomethingIsWrong',
	AUTH405	= 'OldPwdDoesntMatch',
	AUTH406	= 'PendingApproval',
	AUTH407	= 'MissingDocumentation',
	AUTH408	= 'TemporarilySuspended',
	AUTH409	= 'BlockedOut',
	AUTH410 = 'EmailNotRegistered',
	AUTH411 = 'ConfirmPwdDoesntMarch',
	AUTH412 = 'EmailRegistered',
	AUTH201	= "SuccessLogin",
	AUTH202	= "SuccessLogout",
	AUTH203	= 'PassChanged',
	AUTH413 = 'ActiveLogin'
}

export function NotSubtractHoursDate(s: string) {
    let fecha = new Date(new Date(s).getTime() + (new Date(s).getTimezoneOffset() * 60 * 1000));
    return fecha
    // let b: any = s.split(/\D+/);
    // console.log(b);
    // return new Date(Date.UTC(b[0], --b[1], b[2], b[3], b[4], b[5], b[6]));
}

export function SendDateHours(fecha: Date) {
	let fechaAux = new Date(fecha)
	let dia, mes, anio, hora, minuto, segundo
	dia = fechaAux.getDate()
	mes = fechaAux.getMonth()
	anio = fechaAux.getFullYear()
	hora = fechaAux.getHours()
	minuto = fechaAux.getMinutes()
	segundo = fechaAux.getSeconds()
	let fechaEnviar = new Date(Date.UTC(anio, mes, dia, hora, minuto, segundo))
	return fechaEnviar
}
