import { HttpClient } from '@angular/common/http';
import { ElementRef, Inject, Injectable, ViewChild } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { Observable, Subject } from 'rxjs';
import { environment } from "src/environments/environment";
import { header } from "./misc/constants";
import { catchError, map } from 'rxjs/operators';
import { DOCUMENT } from '@angular/common';


@Injectable()
export class MenuService {

    private menuSource = new Subject<string>();
    private resetSource = new Subject();

    menuObj: any[] = [];
    // currentRoute: SafeResourceUrl = localStorage.getItem('route') || environment.initialRoute
    //currentRoute: SafeResourceUrl = this.sanitizer.bypassSecurityTrustResourceUrl('http://192.168.3.12:8090/#/veteranos/')

    menuSource$ = this.menuSource.asObservable();
    resetSource$ = this.resetSource.asObservable();

    constructor(
        private sanitizer: DomSanitizer,
        private _http: HttpClient,
        @Inject(DOCUMENT) private document: HTMLDocument
    ){
        // this.getMenu().subscribe((menu)=>{
        //     this.menuObj = []
        //     // this.menuObj.push(
        //     //     {
        //     //         label: 'Inicio',
        //     //         icon: 'pi pi-home',
        //     //         // urlOpt: environment.initialRoute,
        //     //         descripcion: 'Inicio'
        //     //     }
        //     // )
        //     menu.data.forEach(option => {
        //         this.menuObj.push(
        //             {
        //                 label: option.nombre,
        //                 icon: option.icono,
        //                 urlOpt: option.url,
        //                 descripcion: option.descripcion
        //             }
        //         )
        //     });

        //     },error=>{})

        // let url = localStorage.getItem('route') || environment.initialRoute
        // this.currentRoute = this.sanitizer.bypassSecurityTrustResourceUrl(url)
    }

    onMenuStateChange(newUrl: string) {
        //this.currentRoute = this.sanitizer.bypassSecurityTrustResourceUrl(newUrl)
        localStorage.setItem("route", newUrl);
        this.menuSource.next(newUrl)
        this.refreshIframe(newUrl)
    }

    public getMenu() {
        return this._http.post<any>(environment.urlAPI+'/seguridad/menu/obtener', { "modulo" : "principal" }, {
            'headers':header}).pipe(
                map((resp: any) => {
                    //localStorage.setItem('token', resp.token)
                    return resp;
                }),
                catchError(err => {
                    console.log(err)
                    return err;
                })
            )
    }

    refreshIframe(url: string)
    {
        let iframe : any = this.document.getElementById('ifventanilla')
        if (isNaN(Number(url)))
        {
            iframe.src = url
            let ifr = iframe.contentWindow || iframe.contentDocument
            iframe.addEventListener('load', function(){
                setTimeout(() => {
                    ifr.postMessage(JSON.stringify({token: localStorage.getItem('token'), user: localStorage.getItem('user'), idUser: localStorage.getItem('idUser')}), url);
                }, 1000);
                
            })
        }
    }

    reset() {
        this.resetSource.next();
    }
}
