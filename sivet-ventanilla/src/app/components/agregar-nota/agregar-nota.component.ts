import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { BitacoraService } from 'src/app/services/bitacora.service';
import Swal from 'sweetalert2';
import { regBitacoraModel } from 'src/app/models/reg_bitacora_model';
import { NotSubtractHoursDate, SendDateHours } from 'src/app/misc/constants';
import { NgxSpinnerService } from 'ngx-spinner';
import { PermisosService } from 'src/app/services/permisos.service';

/**
 * Interfaz de selector
 * 
 * Interfaz genérica para desplegar cualquier tipo de selector
 */
interface item
{
  name: string,
  code: string
}

@Component({
  selector: 'app-agregar-nota',
  templateUrl: './agregar-nota.component.html',
  styleUrls: ['./agregar-nota.component.scss']
})

export class AgregarNotaComponent implements OnInit {

  @Output() close: EventEmitter<any> = new EventEmitter();
  veterano:any
  becario:any

  /** Formulario */
  notaForm: FormGroup;

  departamentos: item[] = [];
  servicios: item[] = [];
  estadosBitacora: item[] = [];
  estadosPorDefecto: item[] = [];
  canalesAtencion: item[] = [];
  grupoFamiliar: item[] = [];
  veteranos: any[] = [];

  actividades: any[];

  canalAtencion: string;

  /** Intervalo: Referencia para poder cerrar el intervalo cuando se cambian manualmente los valores */
  public interval: any;

  visibleEditor: boolean;

  actividad: any;

  foto_veterano: string;

  tabPanelActivo: number;

  persona: any;

  // Estado durante el cual se está enviando el formulario, para desactivar el botón de guardar.
  enviando: boolean;
  // Label del botón guardar
  guardar_label: string;

  //Fecha y hora actual del servidor
  fechaActual: Date;

  tipoEstado: string;

  familiar: any;

  id_departamento: number;

  constructor(
    private formBuilder:FormBuilder,
    private bitacoraService:BitacoraService,
    public _spinner:NgxSpinnerService,
    public _permisos: PermisosService)
    {
      this.veterano = {
        nombres:"",
        apellidos:""
      }
      this.becario = {
        nombres:"",
        apellidos:""
      }
      this.interval = null;
      this.actividad = {
        fecha: '',
        horaInicio: '',
        horaFin: '',
        descripcion: ''
      }
      this.foto_veterano = 'assets/img/blank_avatar.png';
      this.canalAtencion = "1";
      this.visibleEditor = false;
      this.tabPanelActivo = 0;

      this.enviando = false;
      this.guardar_label = "Guardar";

      this.fechaActual = new Date();

      this.tipoEstado = 'veterano';

      this.familiar = null;

      this.id_departamento = parseInt(localStorage.getItem("idDepartment"));
    }

  ngOnInit(): void {
    // Cargar los departamentos del catálogo
    this.bitacoraService.obtenerCatalogo("unidadAreaDepto", null).subscribe({
      next: this.gestionarDepartamentos.bind(this),
      error: () => {
        console.log("Error");
    }});

    // Cargar los estados por defecto de la bitácora
    this.bitacoraService.obtenerEstados(-1).subscribe({
      next: this.gestionarEstados.bind(this),
      error: ()=> {
        console.log("Error");
    }});

    // Canales de atención
    this.bitacoraService.obtenerCatalogo("canalesAtencion", -1).subscribe({
      next: this.gestionarCanalesAtencion.bind(this),
      error: ()=> {
        console.log("Error");
    }});

    // Crear el formulario
    this.crearFormulario();

    // Obtener los permisos si no se han obtenido antes desde otro componente
    if(this._permisos.permisos == null)
    {
      this._permisos.obtenerPermisos().subscribe(resp => {
      }, (error) => {
          console.log("error al obtener los permisos ", error);
      })
    }
  }

  /**
   * Inicialización de datos del veterano
   * 
   * A diferencia de ngOnInit, este método se ejecuta cada vez que se hae visible la ventana modal.
   */
  public init()
  {
    // Verificar tipo de estado a utilizar
    const ruta:string = localStorage.getItem("route");
    if(ruta != null && (ruta.indexOf("inabve.gob.sv/becas") >= 0 || ruta.indexOf("localhost:4208") >= 0))
    {
      this.tipoEstado = "becario";
    }
    else
    {
      this.tipoEstado = "veterano";
    }

    this.actividades = [];
    this.persona = null;

    this.notaForm.get("duiParticular").setValue('');
    this.notaForm.get("nombresParticular").setValue('');
    this.notaForm.get("apellidosParticular").setValue('');
    this.notaForm.get("telefonoParticular").setValue('');
    this.notaForm.get("comentariosParticular").setValue('');

    this.tabPanelActivo = 0;

    this.notaForm.get("fecha").setValue(new Date());
    this.notaForm.get("horaInicio").setValue(new Date());
    this.notaForm.get("idEstado").setValue('77316');
    const id_regionales = [98223,98224,98225,98226];
    if(id_regionales.indexOf(parseInt(localStorage.getItem("idDepartment"))) == -1)
    {
      this.notaForm.get("idUnidad").setValue(localStorage.getItem("idDepartment"));
    }
    this.notaForm.get("idServicio").setValue('');
    this.notaForm.get("descripcion").setValue('');
    this.interval = setInterval(() => {
      let nueva_hora= new Date((new Date(this.fechaActual)).getTime() + 1000);
      this.notaForm.get("horaFin").setValue(nueva_hora);
      this.fechaActual = nueva_hora;
    }, 1000);
    this.obtenerBitacora();
    this.cargarServicios();
  }

  async obtenerVeterano()
  {
    const userId:number = parseInt(localStorage.getItem("idUser"));
    await this.bitacoraService.obtenerState(userId).subscribe({
      next: this.gestionarStatus.bind(this),
      error: ()=> {
        console.log("Error");
      }});
  }

  async obtenerBitacora()
  {
    let bitacoraObj:regBitacoraModel = new regBitacoraModel();
    let solicitud:any = {...bitacoraObj};
    solicitud.tipoEstado = this.tipoEstado;
    solicitud.idUsuario = parseInt(localStorage.getItem("idUser"));
    if(this.modoBitacora() == "familiar")
    {
      solicitud.idFamiliar = parseInt(this.notaForm.get("idBeneficiario").value);
    }
    await this.bitacoraService.obtenerBitacora(solicitud).subscribe({
      next: this.gestionarBitacora.bind(this),
      error: ()=> {
        Swal.fire({
          title: "Error al cargar datos",
          html: "Ha ocurrido un error a la hora de cargar datos debido a su conexión de red. Por favor, intente de nuevo.",
          icon: "error",
          didClose: () => {
            this.close.emit(null);
          }
        });
      }});
  }

  crearFormulario()
  {
    const fechaActual = new Date();
    this.notaForm = this.formBuilder.group({
      "fecha": [fechaActual],
      "horaInicio": [fechaActual],
      "horaFin": [fechaActual],
      "idUnidad": ['', Validators.required],
      "idServicio": ['', Validators.required],
      "descripcion": ['', Validators.required],
      "idEstado": ['', Validators.required],
      "idCanalAtencion": ['', Validators.required],
      "canalAtencion": ["1"],
      "idParticular": ['-1'],
      "duiParticular": [''],
      "nombresParticular": [''],
      "apellidosParticular": [''],
      "telefonoParticular": [''],
      "comentariosParticular": [''],
      "idBeneficiario": ['']
    });
  }

  modoBitacora()
  {
    if(this.tipoEstado == 'becario')
    {
      if(this.tabPanelActivo == 0)
      {
        return 'becario';
      }
      else
      {
        return 'particular';
      }
    }
    else
    {
      if(this.tabPanelActivo == 0)
      {
        return 'veterano';
      }
      else if(this.tabPanelActivo == 1 && this.veterano.grupoFamiliar.length > 0)
      {
        return 'familiar';
      }
      else
      {
        return 'particular';
      }
    }
  }

  guardar()
  {
    const fechaDate: Date = this.notaForm.get("fecha").value;
    const inicioDate: Date = this.notaForm.get("horaInicio").value;
    const finDate: Date = this.notaForm.get("horaFin").value;
    const tipoBitacora = this.modoBitacora();

    if(inicioDate >= finDate)
    {
      Swal.fire("Error", "La hora de inicio no puede ser igual o posterior a la hora de finalización", "error");
      return;
    }

    if(tipoBitacora == "familiar" && this.familiar == null)
    {
      Swal.fire("Error", "No ha seleccionado un familiar", "error");
      return;
    }

    let bitacoraObj:regBitacoraModel = {
      id : -1,
      idVeterano : -1,
      idBeneficiario : -1,
      idParticular: -1,
      idUnidad : this.notaForm.get("idUnidad").value,
      idUnidadUsuario : parseInt(localStorage.getItem("idDepartment")),
      idActividad : this.notaForm.get("idServicio").value,
      idCanalAtencion: this.notaForm.get("idCanalAtencion").value,
      idEstatus : this.notaForm.get("idEstado").value,
      tipoLlamada : false,
      fecha : SendDateHours(fechaDate),
      descripcion : this.notaForm.get("descripcion").value,
      horaInicio : SendDateHours(inicioDate),
      horaFin : SendDateHours(finDate),
      creadoEn : new Date(),
      creadoPor : parseInt(localStorage.getItem("idUser")),
      modificadoEn : new Date(),
      modificadoPor : parseInt(localStorage.getItem("idUser")),
      activo : true,
      particular: null
    };

    if(tipoBitacora == "particular")
    {
      if(this.persona == null)
      {
        this.persona = {
          idVeterano: -1,
          idBeneficiario: -1,
          idParticular: -1
        }
      }
      bitacoraObj.idVeterano = this.persona.idVeterano;
      bitacoraObj.idBeneficiario = this.persona.idBeneficiario;
      bitacoraObj.idParticular = this.persona.idParticular;
      bitacoraObj.particular =
      {
        id: this.persona.idParticular,
        dui: this.notaForm.get("duiParticular").value,
        nombres : this.notaForm.get("nombresParticular").value,
        apellidos : this.notaForm.get("apellidosParticular").value,
        telefono : this.notaForm.get("telefonoParticular").value,
        comentarios : this.notaForm.get("comentariosParticular").value,
        creadoEn : new Date(),
        creadoPor : parseInt(localStorage.getItem("idUser")),
        modificadoEn : new Date(),
        modificadoPor : parseInt(localStorage.getItem("idUser")),
        activo : true,
      }

      if(bitacoraObj.idVeterano == -1 && bitacoraObj.idBeneficiario == -1 && bitacoraObj.particular.dui.trim().length != 10)
      {
        Swal.fire("Error", "El DUI es requerido", "error");
        bitacoraObj.particular = null;
        return;
      }
    }
    else
    {
      if(tipoBitacora == 'becario')
      {
        bitacoraObj.idVeterano = this.becario.idBeneficiario == -1 ? this.becario.idVeterano : -1;
        bitacoraObj.idBeneficiario = this.becario.idBeneficiario;
      }
      else if(tipoBitacora == "familiar")
      {
        bitacoraObj.idVeterano = -1;
        bitacoraObj.idBeneficiario = this.familiar.id;
      }
      else
      {
        bitacoraObj.idVeterano = this.veterano.id;
        bitacoraObj.idBeneficiario = -1;
      }
      bitacoraObj.idParticular = -1;
    }

    this._spinner.show();
    this.enviando = true;
    this.guardar_label = "Guardando...";

    this.bitacoraService.guardarBitacora(bitacoraObj).subscribe({
      next: this.actualizarLista.bind(this),
      error: ()=> {
        console.log("Error al guardar")
        this._spinner.hide();
        this.enviando = false;
        this.guardar_label = "Guardar";
      }      
  });
    Swal.fire({
      title: 'Éxito',
      text: 'Se ha registrado una nueva actividad.',
      icon: 'success',
      didDestroy: () => {
        this.notaForm.get("fecha").setValue(new Date());
        this.notaForm.get("horaInicio").setValue(new Date());
        this.notaForm.get("idEstado").setValue('77316');
        this.notaForm.get("idUnidad").setValue(localStorage.getItem("idDepartment"));
        this.notaForm.get("idCanalAtencion").setValue('');
        this.notaForm.get("idServicio").setValue('');
        this.notaForm.get("descripcion").setValue('');
      }
    });
  }

  cancelar()
  {
    this.close.emit(null);
  }

  detenerIntervalo()
  {
    console.log("Detener");
    clearInterval(this.interval);
  }

  cargarServicios()
  {
    const id_relacion = this.notaForm.get("idUnidad").value;
    this.bitacoraService.obtenerCatalogo("actividadesPorUnidad", id_relacion).subscribe({
      next: this.gestionarServicios.bind(this),
      error: ()=> {
        console.log("Error");
      }});
  }

  cargarEstados()
  {
    let id_relacion = this.notaForm.get("idServicio").value;
    if(isNaN(id_relacion))
    {
      id_relacion = -1;
    }
    this.bitacoraService.obtenerEstados(id_relacion).subscribe({
      next: this.gestionarEstados.bind(this),
      error: ()=> {
        console.log("Error");
    }});
  }

  editarNota(item:any)
  {
    this.actividad = item;
    this.visibleEditor = true;
  }

  /**
   * ****** MÉTODOS PARA GESTIONAR RESPUESTAS DE SERVICIOS ******
   */
  gestionarDepartamentos(res:any)
  {
    const id_regionales = [98223,98224,98225,98226];
    res.data.forEach(item => {
      //Comprobación de ID de regionales para no incluirlas en el selector
      if(id_regionales.indexOf(parseInt(item.id)) == -1)
      {
        this.departamentos.push({
          code: item.id,
          name: item.descripcion
        });
      }
    });
  }

  gestionarEstados(res:any)
  {
    this.estadosBitacora = [];
    res.data.forEach(item => {
      this.estadosBitacora.push({
        code: item.id,
        name: item.descripcion
      });
    });
    if(this.estadosPorDefecto.length == 0)
    {
      this.estadosPorDefecto = [... this.estadosBitacora];
    }
    else if(this.estadosBitacora.length == 0)
    {
      this.estadosBitacora = [... this.estadosPorDefecto];
    }
  }

  gestionarCanalesAtencion(res:any)
  {
    res.data.forEach(item => {
      this.canalesAtencion.push({
        code: item.id,
        name: item.descripcion
      });
    });
  }

  gestionarBitacora(res)
  {
    //console.log(res);
    if(res.estados)
    {
      this.gestionarStatus(res);
    }
    if(res.bitacora == null)
    {
      this.actividades = [];
    }
    else
    {
      this.actividades = res.bitacora.data;
      this.actividades.sort((a, b) => {
        if(a.fecha < b.fecha)
        {
          return 1;
        }
        if(a.fecha > b.fecha)
        {
          return -1;
        }
        if(a.horaInicio < b.horaInicio)
        {
          return 1;
        }
        if(a.horaInicio > b.horaInicio)
        {
          return -1;
        }
        return 0;
      });
      const datepipe: DatePipe = new DatePipe('en-US')
      this.actividades.forEach((item:any) => {
        item.fecha = datepipe.transform(NotSubtractHoursDate(item.fecha), 'dd/MM/YYYY');
        item.horaInicio = datepipe.transform(NotSubtractHoursDate(item.horaInicio), 'hh:mm:ss a');
        item.horaFin = datepipe.transform(NotSubtractHoursDate(item.horaFin), 'hh:mm:ss a');
        item.descripcion = item.descripcion.replaceAll('<', "&lt;").replaceAll('>', "&gt;");
        item.descripcion = item.descripcion.replaceAll('\n', "<br>");
      })
    }
    if(res.fechaActual)
    {
      this.fechaActual = res.fechaActual;
    }
  }

  gestionarStatus(res:any)
  {
    if(res.estados)
    {
      if(this.tipoEstado == 'becario')
      {
        this.becario = res.estados.becarioObj;
      }
      else
      {
        this.veterano = res.estados.veteranObj;
        this.foto_veterano = ((this.veterano.fotografia)==null)? 'https://dev.core.inabve.gob.sv/veteran_photos/api/public/dl/7HOtKaYd/VET_FOTO_' + this.veterano.carnet + '.jpg?inline=true' : 'data:image/jpeg;base64,' + this.veterano.fotografia;
        this.grupoFamiliar = [];
        this.veterano.grupoFamiliar.forEach(item => {
          this.grupoFamiliar.push({
            code: item.id,
            name: item.nombres + " " + item.apellidos
          })
        });
      }
      if(res.estados.date)
      {
        this.notaForm.get("horaInicio").setValue(new Date(res.estados.date));
      }
    }
  }

  gestionarServicios(res:any)
  {
    this.servicios = [];
    const id_relacion = this.notaForm.get("idUnidad").value;
    res.data.forEach(item => {
      if(item.idRelacion == id_relacion)
      {
        this.servicios.push({
          code: item.id,
          name: item.descripcion.substring(0, 100)
        });
      }
    });
  }

  buscarDUI()
  {
    const dui = this.notaForm.get("duiParticular").value;

    //Comprobar DUI
    const digitos:string[] = {...dui};
    let resumen = 0;
    for(let i = 0; i < 8; i++)
    {
      resumen += (parseInt(digitos[i]) * (9 - i));
    }
    resumen = (10 - (resumen % 10)) % 10;
    if(parseInt(digitos[9]) != resumen)
    {
      Swal.fire("Error", "El DUI ingresado es inválido", "error");
      this.notaForm.get("duiParticular").setValue("");
      this.notaForm.get("idParticular").setValue("-1");
      this.notaForm.get("nombresParticular").setValue('');
      this.notaForm.get("apellidosParticular").setValue('');
      this.notaForm.get("telefonoParticular").setValue('');
      this.notaForm.get("comentariosParticular").setValue('');
      this.actividades = [];
      return;
    }

    this.bitacoraService.obtenerPersonaPorDUI(dui).subscribe({
      next: this.gestionarPersona.bind(this),
      error: ()=> {
        console.log("Error");
      }});
  }

  gestionarPersona(res:any)
  {
    if(res.persona == null)
    {
      this.notaForm.get("nombresParticular").setValue('');
      this.notaForm.get("apellidosParticular").setValue('');
      this.notaForm.get("telefonoParticular").setValue('');
      this.notaForm.get("comentariosParticular").setValue('');
      this.persona = null;
    }
    else
    {
      const persona:any = res.persona;
      this.notaForm.get("nombresParticular").setValue(persona.nombres);
      this.notaForm.get("apellidosParticular").setValue(persona.apellidos);
      this.notaForm.get("telefonoParticular").setValue(persona.telefono);
      this.notaForm.get("comentariosParticular").setValue(persona.comentarios);
      this.persona = persona;
    }
    if(res.veteranos != null && res.veteranos.data != null && res.veteranos.data.length > 0)
    {
      this.veteranos = res.veteranos.data;
    }
    else
    {
      this.veteranos = [];
    }
    this.gestionarBitacora(res);
  }

  cambiarTab(e)
  {
    let index = e.index;
    this.actividades = [];
    this.persona = null;
    this.veteranos = [];
    this.familiar = null;
    this.notaForm.get("duiParticular").setValue("-1");
    this.notaForm.get("idParticular").setValue("-1");
    this.notaForm.get("nombresParticular").setValue('');
    this.notaForm.get("apellidosParticular").setValue('');
    this.notaForm.get("telefonoParticular").setValue('');
    this.notaForm.get("comentariosParticular").setValue('');
    this.notaForm.get("idBeneficiario").setValue('');
    if(index == 0)
    {
      this.obtenerBitacora();
    }
  }

  actualizarLista(res)
  {
    this._spinner.hide();
    this.enviando = false;
    this.guardar_label = "Guardar";
    if(res.data[0])
    {
      const item = res.data[0];
      const datepipe: DatePipe = new DatePipe('en-US');
      if(this.tabPanelActivo == 1 && this.persona == null)
      {
        this.persona = {
          id: item.idParticular,
          dui: this.notaForm.get("duiParticular").value,
          nombres: this.notaForm.get("nombresParticular").value,
          apellidos: this.notaForm.get("apellidosParticular").value,
          telefono: this.notaForm.get("telefonoParticular").value,
          comentarios: this.notaForm.get("comentariosParticular").value
        }
        this.notaForm.get("idParticular").setValue(item.idParticular);
      }
      item.fecha = datepipe.transform(NotSubtractHoursDate(item.fecha), 'dd/MM/YYYY');
      item.horaInicio = datepipe.transform(NotSubtractHoursDate(item.horaInicio), 'hh:mm:ss a');
      item.horaFin = datepipe.transform(NotSubtractHoursDate(item.horaFin), 'hh:mm:ss a');
      item.descripcion = item.descripcion.replaceAll('<', "&lt;").replaceAll('>', "&gt;");
      item.descripcion = item.descripcion.replaceAll('\n', "<br>");
      this.actividades.unshift(item);
    }
  }

  seleccionarBeneficiario()
  {
    this.obtenerBitacora();
    const idBeneficiario = parseInt(this.notaForm.get("idBeneficiario").value);
    this.familiar = this.veterano.grupoFamiliar.find(x => x.id == idBeneficiario);
    console.log(this.familiar);
  }

  eliminarNota(actividad: regBitacoraModel)
  {
    const id_actividad = actividad.id
    console.log("Eliminar actividad: " + id_actividad);
    Swal.fire({
      title: 'Confirmar',
      text: '¿Confirma que desea eliminar esta entrada de la bitácora?',
      icon: 'question',
      showDenyButton: true,
      confirmButtonText: 'Sí',
      denyButtonText: 'No',
      denyButtonColor: 'gray',
      confirmButtonColor: 'red',
      focusDeny: true
    }).then((result) => {
      if(result.isConfirmed)
      {
        this.bitacoraService.eliminarBitacora(id_actividad).subscribe({
          next: () => {
            actividad.activo = false;
            Swal.fire({
              title: 'Eliminado',
              text: 'Entrada eliminada con éxito'
            })
          },
          error: () => {
            console.log("Error al eliminar");
          }});
          }
    });
  }
}
