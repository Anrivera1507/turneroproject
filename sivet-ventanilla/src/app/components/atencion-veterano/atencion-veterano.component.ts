import { Component, OnInit, AfterViewInit } from '@angular/core';
import { PrimeNGConfig } from 'primeng/api';
import { AppComponent } from 'src/app/app.component';
import { MenuService } from 'src/app/app.menu.service';
import { DomSanitizer } from '@angular/platform-browser';
import { environment } from 'src/environments/environment';
import { UserService } from 'src/app/services/user.service';
import { Router } from '@angular/router';
import { VentanillaService } from '../../services/ventanilla.service';
import { TicketModel } from '../../models/tickets.model';
import { NotSubtractHoursDate } from 'src/app/utils/constantes';
import { AreasAtencion } from 'src/app/interfaces/areas.interface';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-atencion-veterano',
  templateUrl: './atencion-veterano.component.html',
  styleUrls: ['./atencion-veterano.component.scss']
})
export class AtencionVeteranoComponent implements OnInit, AfterViewInit  {
  urlRegistro = this.sanitizer.bypassSecurityTrustResourceUrl('')
  
  itemBtn : object [] = []

  idArea        : string = ''
  numVentanilla : string = ''
  listaTicket   : TicketModel [] = []
  codigoArea    : AreasAtencion [] = [];
  idAreaSelect  : AreasAtencion
  display       : boolean = false
  lsVentanillas : any = []
  constructor(
    public app        : AppComponent, 
    private sanitizer : DomSanitizer,
    private _user     : UserService,
    private router    : Router,
    private ventanilla : VentanillaService
    ) { }
    param : object = {
      'pendiente' : false,
      'idArea'    : localStorage.getItem('IdArea')
    }

    ngOnInit(): void {
      this.cargarAreasAtencion()
      this.itemBtn = [
      {label: 'Salir', icon: 'pi pi-sign-out', command: ( resp=> 
        {
          localStorage.clear();
          sessionStorage.clear();
          this.router.navigate(["/login"]);
        }) 
      }
    ]
  }
  
  ngAfterViewInit(): void {
    this.urlRegistro = this.sanitizer.bypassSecurityTrustResourceUrl('')
    setTimeout(() => {
      this._user.refreshIframe('', "ifRegistro" )
    }, 1000);
    if(localStorage.getItem('IdArea')){
      this.cargarTurnos(this.param)
    }
  }
  
  llamarSinRespuesta(ticket : TicketModel){
    if( this.listaTicket.length > 0){
      let parametros = {
        "id" : ticket.id,
        "idTecnico": localStorage.getItem('idUser'),
        "escritorio": 5,
        "modificadoEn": new Date(),
        "modificadoPor": localStorage.getItem('idUser'),
        "activo": true
      }
      this.ventanilla.asignarTicketVentanilla( parametros ).subscribe(
        resp => {
        },
        (err) => {
          console.log('Error:');
          console.log(err);
        }
      );
    }
  }

  contador : number=0;
  cargarTurnos( param ){
    this.contador+=1;
    this.ventanilla.obtenerTurnos( param ).subscribe(
    resp => {
          // console.log('Lista de turnos pendientes'+this.contador);
          // console.log(resp.data);
          if( resp.data != null && resp.data.length > 0){
            this.listaTicket = resp.data
            this.listaTicket.forEach(ticket => {
              ticket.creadoEn = NotSubtractHoursDate(ticket.creadoEn.toString())
            });
          }
        },
        (err) => {
          console.log('Error:');
          console.log(err);
        }
    )
  }

    encontrarSiguiente(){
      if( this.listaTicket.length > 0){
        // console.log( this.listaTicket );
        let parametros = {
          "id" : this.listaTicket[0].id,
          "idTecnico": localStorage.getItem('idUser'),
          "escritorio": 5,
          "modificadoEn": new Date(),
          "modificadoPor": localStorage.getItem('idUser'),
          "idArea" : localStorage.getItem('IdArea'),
          "activo": true
        }
        this.ventanilla.asignarTicketVentanilla( parametros ).subscribe(
          ticket => {
            // console.log(ticket);
            // if( ticket.numeroLlamada == 3 ){
              this.cargarTurnos(this.param)
            // }
          },
          (err) => {
            console.log('Error:');
            console.log(err);
          }
        );
      }else{

      }
    }
    
    finalizarTicket( tictet : TicketModel ){
        let parametros = {
          "id" : this.listaTicket[0].id,
          "idTecnico": localStorage.getItem('idUser'),
          "escritorio": 5,
          "modificadoEn": new Date(),
          "modificadoPor": localStorage.getItem('idUser'),
          "idArea" : localStorage.getItem('IdArea'),
          "activo": true
        }
        this.ventanilla.asignarTicketVentanilla( parametros ).subscribe(
          resp => {
            // console.log(resp);
            if( resp[0].numeroLlamada == 3 ){
              this.cargarTurnos(this.param)
            }
          },
          (err) => {
            console.log('Error:');
            console.log(err);
          }
        );
      
    }

    cargarAreasAtencion(){
      if( !localStorage.getItem('IdArea')){
        this.ventanilla.cargarAreasAtencion().subscribe(
           resp => {
            this.codigoArea = resp
            this.display = true
          },
          (err) => {
            console.log('Error:');
            console.log(err);
          }
        )
      }
    }

    cargarVentanillasArea(){
      this.ventanilla.cargarAreasVentanillas().subscribe(
        (lista) =>{
          this.lsVentanillas = lista
        },
        (err) =>{
          console.log(err);
          
        }
      )
    }

    guardarVentanillaArea(numVentanilla : string, areaSelect : AreasAtencion){
      if( areaSelect.id != '' ){
        localStorage.setItem('IdArea', areaSelect.id);
        localStorage.setItem('numVentanilla', numVentanilla);
        this.param = {
          'pendiente' : false,
          'idArea'    : localStorage.getItem('IdArea')
        }
        
        this.cargarTurnos(this.param)
        this.display = false;
      }else{
        Swal.fire({
          title:'Aviso',
          text:'Por favor seleccione un area de atencion',
          icon:'warning',
          confirmButtonText:'Entendido',
          confirmButtonColor: '#3085D6'
        }).then(function (result)
        {})
      }
    }
}
