import { RegVeteranosModel } from './../../models/reg_veterano.model';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { ConfigService } from 'src/app/services/config.service';
import { environment } from 'src/environments/environment';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})
export class VeteranoBuscarService {
    regVeteranosModel: RegVeteranosModel=new RegVeteranosModel();
    existeVeterano:boolean=false;
    edad:number=0;
  idVeterano:number=0
  constructor(
    private _http   :    HttpClient,
    private _config :    ConfigService
  ) { }
  public buscar(objParam: any): Observable<any> { 
    return this._http.post<any>(this._config.urlAPI+'/registros/veteranos/buscar',objParam, {'headers':this._config.headers})
    .pipe(
            map((resp: any) => { 
                return resp;
            }),
            catchError(err => {
                console.log(err)  
                return err;
            })
        )
  }
  
  public obtener(id?: number): Observable<any> { 
    return this._http.post<any>(this._config.urlAPI+'/registros/veteranos/obtener',{id:id}, {'headers':this._config.headers})
    .pipe(
            map((resp: any) => {
                if(resp.data !=null && resp.data.length > 0){
                    this.regVeteranosModel=resp.data[0];
                    if(this.regVeteranosModel.carnet.length>0){
                        this.regVeteranosModel.edad = (this.regVeteranosModel.fechaNacimimento == null) ? 0 : moment().diff(this.regVeteranosModel.fechaNacimimento, 'years');
                        this.existeVeterano=true;
                    }
                }
                
                
                return resp;
            }),
            catchError(err => {
                console.log(err)
                return err;
            })
        )
  }

  public guardarVeterano(objVeterano: any): Observable<any> {  
    return this._http.post<any>(this._config.urlAPI+'/registros/veteranos/guardar', objVeterano, {
        'headers':this._config.headers}).pipe(
            map((resp: any) => { 
                return resp;
            }),
            catchError(err => {
                console.log(err)
                return err; 
            })
        )
  }  


  public getOperativoByCarnet(objOperCarnet: any): Observable<any> {   
    return this._http.post<any>(this._config.urlAPI+'/vet_opercarnet/getByCarnet', objOperCarnet, {
        'headers':this._config.headers}).pipe(
            map((resp: any) => { 
                return resp;
            }),
            catchError(err => {
                console.log(err)
                return err; 
            })
        )
  }  

  
}

