import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import * as moment from 'moment';
import { ObjState } from 'src/app/models/obj_status.model';
import { RegVeteranosModel } from 'src/app/models/reg_veterano.model';
import { ConfigService } from 'src/app/services/config.service';
import { VeteranoBuscarService } from './veterano-buscar.service';
import { AppMainComponent } from '../../app.main.component';
import { PermisosService } from 'src/app/services/permisos.service';

@Component({
  selector: 'app-veterano-buscar',
  templateUrl: './veterano-buscar.component.html',
  styleUrls: ['./veterano-buscar.component.scss']
})
export class VeteranoBuscarComponent implements OnInit {
  searching             : string  = "" 
  filteredVeteran       : any[]
  existeVeterano        : boolean = false
  duiSearch             : string  = ""
  sexo                  : string  = ""
  celular               : string  = ""
  nombreCompleto        : string  = ""
  edad                  : number  = 0
  veteranObj            = new RegVeteranosModel()  
  objState  : ObjState  = new ObjState(); 
  idUser                : number;
  verVeteranoModal      : boolean = false  
  veteranoModel:RegVeteranosModel=new RegVeteranosModel();
  //@Output() veterano = new EventEmitter<RegVeteranosModel>();
  
  //@ViewChild(FrmVeteranoComponent)  formularioVeteranoChild   : FrmVeteranoComponent;
  //@ViewChild(VeteranoComponent)     veteranoChild             : VeteranoComponent;

  constructor(
    public _veterano        : VeteranoBuscarService ,
    public _configService   : ConfigService,
    public _permisos: PermisosService,
    public app        : AppMainComponent
  ) { }

  ngOnInit(): void {


  }

  obtenerVeterano(){
    this._veterano.obtener()
    .subscribe(
      (data)=>{
        //console.log(data);
        
      }
    )

  }

}
