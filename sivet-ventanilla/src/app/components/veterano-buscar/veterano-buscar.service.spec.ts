import { TestBed } from '@angular/core/testing';

import { VeteranoBuscarService } from './veterano-buscar.service';

describe('VeteranoBuscarService', () => {
  let service: VeteranoBuscarService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(VeteranoBuscarService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
