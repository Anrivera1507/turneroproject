import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VeteranoBuscarComponent } from './veterano-buscar.component';

describe('VeteranoBuscarComponent', () => {
  let component: VeteranoBuscarComponent;
  let fixture: ComponentFixture<VeteranoBuscarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VeteranoBuscarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VeteranoBuscarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
