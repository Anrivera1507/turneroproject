import { TicketModel } from 'src/app/models/tickets.model';
import { Component, OnInit } from '@angular/core';
import { VentanillaService } from 'src/app/services/ventanilla.service';
import { NotSubtractHoursDate } from 'src/app/utils/constantes';

@Component({
  selector: 'app-lista-ticket',
  templateUrl: './lista-ticket.component.html',
  styleUrls: ['./lista-ticket.component.scss']
})
export class ListaTicketComponent implements OnInit {
  
  //variable para obtener ticket pendiente del dia
  listaTicketPendiente:TicketModel[]=[]
  idArea = Number(localStorage.getItem("idArea"))

  constructor(
    private _turnoSer: VentanillaService,
  ) { }

  ngOnInit(): void {
    this.obtenerTicketPendiente();
    // if () {

    // }
  }

  obtenerTicketPendiente() {
    let ticketP : any = {}
    ticketP.pendiente = false;
    ticketP.idArea = this.idArea
    ticketP.idSucursal = Number(localStorage.getItem('idSucursal'))
    this._turnoSer.obtenerTurnos(ticketP).subscribe((tickets) => {
      tickets.data.forEach(ticke => {
        ticke.creadoEn = NotSubtractHoursDate(ticke.creadoEn.toString())
      });
      this.listaTicketPendiente = tickets.data
      this.listaTicketPendiente = this.listaTicketPendiente.filter( x => x.idArea == this.idArea && x.idSucursal == Number(localStorage.getItem('idSucursal')));
    })
  }

}
