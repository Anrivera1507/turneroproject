import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MenuService } from 'src/app/app.menu.service';
import { UserService } from 'src/app/services/user.service';
import {DialogModule} from 'primeng/dialog';
import Swal from 'sweetalert2';
import { AreasAtencion } from 'src/app/interfaces/areas.interface';
import { _Auth, _Crud } from 'src/app/misc/constants';
import { PermisosService } from 'src/app/services/permisos.service';
import { SegUsuariosModel } from 'src/app/models/seg_user_model';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  
  userObj = {
    user: '',
    password: '',
    codigoSeguridad : ''
}

userAux : SegUsuariosModel = new SegUsuariosModel()

cargando : boolean = false

banderaReestablecerClave : boolean = false
banderaRecuperarClave : boolean = false
banderaCodigoSeguridad : boolean = false
banderaLogin : boolean = true

tamanioPass: boolean = false;
letrasPass: boolean = false;
simbolosPass: boolean = false;
numerosPass: boolean = false;
banderapassword: boolean = false;
msgConfirmacion: string;

areas         : any[];

constructor(
    public _menu: MenuService,
    public router: Router,
    public _user: UserService,) { }

ngOnInit(): void {

}

authenticate() {
    if (this.userObj.user == null || this.userObj.user.trim() == '') {
        Swal.fire("¡Alerta!", "No ha ingresado el Usuario", "warning")
        return
    }
    if (this.userObj.password == null || this.userObj.password.trim() == '') {
        Swal.fire("¡Alerta!", "No ha ingresado la Contraseña", "warning")
        return
    }
    let _this=this;
    this.cargando = true
    this._user.authentication(this.userObj.user, this.userObj.password).subscribe(resp => {
        // console.log(resp);
        this.cargando = false
        if (resp.data != null && resp.message != _Auth.AUTH203) {
            this.iniciarSesion(resp)

        } else if (resp.message==_Auth.AUTH413) {
            Swal.fire({
                title:'Aviso',
                text:'Estimado usuario, posee una sesión activa',
                icon:'info',
                confirmButtonText:'Entendido',
                confirmButtonColor: '#3085D6',
                allowOutsideClick:false
            }).then(function (result)
            {
                if (result.isConfirmed) {
                Swal.fire({
                    icon:'question',
                    title:"Administrador de sesión",
                    text:"¿Desea cerrar la sesión anterior?, e iniciar sesión aqui",
                    allowOutsideClick:false,
                    confirmButtonText:"Aceptar",
                    cancelButtonText:"Cancelar",
                    showCancelButton:true,

                }).then((r)=>{
                    if(r.isConfirmed){
                    _this._user.logout(_this.userObj.user)
                    .subscribe((data:any)=>{
                        Swal.fire({
                        icon:'success',
                        title:"Administrador de sesión",
                        text:"Sesión cerrada con exito, vuelva a iniciar de nuevo",
                        allowOutsideClick:false,
                        confirmButtonText:"Aceptar",

                        })
                    })
                    }
                })
                }
            })
        } else if (resp.message==_Auth.AUTH402) {
            Swal.fire({
                title:'Aviso',
                text:'La Contraseña que ha Ingresado es Incorrecta',
                icon:'warning',
                confirmButtonText:'Entendido',
                confirmButtonColor: '#3085D6'
            }).then(result =>
            {})
        } else if (resp.message==_Auth.AUTH410) {
            Swal.fire({
                title:'Aviso',
                text:'El Usuario o Correo Ingresado es Incorrecto',
                icon:'warning',
                confirmButtonText:'Entendido',
                confirmButtonColor: '#3085D6'
            }).then(result =>
            {})
        } else if (resp.message==_Auth.AUTH409) {
            Swal.fire({
                title:'Aviso',
                text:'El Usuario se encuentra Bloqueado',
                icon:'error',
                confirmButtonText:'Entendido',
                confirmButtonColor: '#3085D6'
            }).then(result =>
            {})
        } else if (resp.message==_Auth.AUTH203) {
            Swal.fire({
                title:'Reestablecer Contraseña',
                text:'Debe cambiar su contraseña para poder inciar sesión',
                icon:'info',
                confirmButtonText:'Entendido',
                confirmButtonColor: '#3085D6',
            })
            .then(resultado => {
                this.userAux = new SegUsuariosModel()
                this.userAux.id = resp.data.id
                this.resetBanderas()
                this.banderaReestablecerClave = true
            })
        } else {
            Swal.fire("¡Alerta!", "Ocurrió un problema, Por favor recargue la página o póngase en contacto con TIC´S", "warning")
        }

    }, err => {
      this.cargando = false
      Swal.fire("¡Alerta!", "No hay conexión con el servidor, Por favor recargue la página o póngase en contacto con TIC´S", "error")
    })

}

iniciarSesion(resp) {
    setTimeout(() => {
        // this._menu.refreshIframe(url)
    }, 500);
    localStorage.setItem('conSonido', 'true')
    localStorage.setItem('conMensaje', 'true')
    this.router.navigate(["/"]);
    this.obtenerAreasAtencion()
    this._menu.menuObj = [];
    this._menu.getMenu().subscribe((menu)=>{
        //console.log(menu);
        menu.data.forEach(option => {
            this._menu.menuObj.push(
                {
                    label: option.nombre,
                    icon: option.icono,
                    urlOpt: option.url,
                    descripcion: option.descripcion
                }
            )
        });

        },error=>{}
    )

    Swal.fire({
    title:'Bienvenid@!',
    text: resp.data.nombres + ' ' + resp.data.apellidos,
    icon:'success',
    confirmButtonText:'Entendido',
    confirmButtonColor: 'darkgreen'
    }).then((result) =>
    {

    })
}

obtenerAreasAtencion(){
    this._user.obtenerAreasAtencion().subscribe(
      (resp) =>{
        this.areas = resp
        // console.log(this.areas);
      },
      (err) => {
        console.log(err);
      }
    )
  }

recuperarCuenta() {
    this.resetBanderas()
    this.banderaRecuperarClave = true
}

resetBanderas() {
    this.banderaLogin = false
    this.banderaRecuperarClave = false
    this.banderaReestablecerClave = false
    this.banderaCodigoSeguridad = false
}

cancelar() {
    this.resetBanderas()
    this.banderaLogin = true
    this.userAux = new SegUsuariosModel()
}

cambiarClaveEIniciarSesion() {
    this.cargando = true
    this._user.reestablecerClave(this.userAux).subscribe({
        next: resp => {
            this.cargando = false
            if (resp.message == _Auth.AUTH201) {
                this.iniciarSesion(resp)
            } else {
                if (resp.error == null || resp.error.trim() == '') {
                    Swal.fire("¡Alerta!", "Ocurrió un problema", "warning")
                } else {
                    Swal.fire("¡Alerta!", resp.error, "warning")
                }
            }
        }, error: err => {
            this.cargando = false
            Swal.fire("¡Alerta!", "Ocurrió un problema, Por favor recargue la página o póngase en contacto con TIC´S", "error")
        }
    })
}

cambioCodigoSeguridad() {
    this.resetBanderas()
    this.banderaCodigoSeguridad = true
    this.userObj.codigoSeguridad = ''
}

enviarCodigoRecuperacion() {
    this.cargando = true
    this._user.codigoSeguridad(this.userObj.user).subscribe({
        next: resp => {
            this.cargando = false
            if (resp.message == _Crud.CRUD200) {
                Swal.fire("Código de Seguridad", "Se ha enviado un código de seguridad a su correo", "success")
                this.userObj.codigoSeguridad = ''
                this.resetBanderas()
                this.banderaCodigoSeguridad = true
            } else {
                if (resp.error == null || resp.error.trim() == '') {
                    Swal.fire("¡Alerta!", "Ocurrió un problema", "warning")
                } else {
                    Swal.fire("¡Alerta!", resp.error, "warning")
                }
            }
        }, error: err => {
            this.cargando = false
            Swal.fire("¡Alerta!", "Ocurrió un problema, Por favor recargue la página o póngase en contacto con TIC´S", "error")
        }
    })
}

verificarCodigoSeguridad() {
    this.cargando = true
    this._user.verificarCodigoSeguridad(this.userObj).subscribe({
        next: resp => {
            this.cargando = false
            if (resp.message == _Crud.CRUD200) {
                Swal.fire({
                    title:'Reestablecer Contraseña',
                    text:'Debe cambiar su contraseña para poder inciar sesión',
                    icon:'info',
                    confirmButtonText:'Entendido',
                    confirmButtonColor: '#3085D6',
                })
                .then(resultado => {
                    this.userAux = new SegUsuariosModel()
                    this.userAux.id = resp.data[0].id
                    this.resetBanderas()
                    this.banderaReestablecerClave = true
                })

            } else {
                if (resp.error == null || resp.error.trim() == '') {
                    Swal.fire("¡Alerta!", "Ocurrió un problema", "warning")
                } else {
                    Swal.fire("¡Alerta!", resp.error, "warning")
                }
            }
        }, error: err => {
            this.cargando = false
            Swal.fire("¡Alerta!", "Ocurrió un problema, Por favor recargue la página o póngase en contacto con TIC´S", "error")
        }
    })
}

validar_password2() {
    let msg = '';
    if (this.userAux.clave != '') {
        if (
            this.userAux.clave != this.userAux.claveRepetida
        ) {
            msg = 'Las contraseñas no coinciden';
            this.banderapassword = false;
        }
        if (
            this.userAux.clave == this.userAux.claveRepetida
        ) {
            msg = 'Las contraseñas coinciden';
            this.banderapassword = true;
        }
    }

    if (
        this.userAux.claveRepetida != '' &&
        this.userAux.clave != ''
    ) {
        if (
            this.userAux.clave != this.userAux.claveRepetida
        ) {
            msg = 'Las contraseñas no coinciden';
            this.banderapassword = false;
        }
        if (
            this.userAux.clave == this.userAux.claveRepetida
        ) {
            msg = 'Las contraseñas coinciden';
            this.banderapassword = true;
        }
    }

    this.msgConfirmacion = msg;
    this.analizarSeguridadCadena();
}

validar_password3() {
    let msg = '';
    if (this.userAux.claveRepetida != '') {
        if (
            this.userAux.clave != this.userAux.claveRepetida
        ) {
            msg = 'Las contraseñas no coinciden';
            this.banderapassword = false;
        }
        if (
            this.userAux.clave == this.userAux.claveRepetida
        ) {
            msg = 'Las contraseñas coinciden';
            this.banderapassword = true;
        }
    }

    if (
        this.userAux.claveRepetida != '' &&
        this.userAux.clave != ''
    ) {
        if (
            this.userAux.clave != this.userAux.claveRepetida
        ) {
            msg = 'Las contraseñas no coinciden';
            this.banderapassword = false;
        }
        if (
            this.userAux.clave == this.userAux.claveRepetida
        ) {
            msg = 'Las contraseñas coinciden';
            this.banderapassword = true;
        }
    }

    this.msgConfirmacion = msg;
}

analizarSeguridadCadena() {
    this.letrasPass = false;
    this.numerosPass = false;
    this.tamanioPass = false;
    this.simbolosPass = false;
    let objAnalizado = analizarSeguridadCadena(
        this.userAux.clave
    );
    this.letrasPass = objAnalizado.tieneLetras;
    this.numerosPass = objAnalizado.tieneNumeros;
    this.tamanioPass = objAnalizado.tamanioCadena;
    this.simbolosPass = objAnalizado.tieneSimbolos;
}

}

export function analizarSeguridadCadena(cadena: string) {
let tieneLetras = false
let tieneNumeros = false
let tieneSimbolos = false
let tamanioCadena = false

if (cadena.length >= 8) {
    tamanioCadena = true
} else {
    tamanioCadena = false
}

for (const caracter of cadena) {
    if (/[a-zA-Z]/.test(caracter)) {
        tieneLetras = true
    } else if (/[0-9]/.test(caracter)) {
        tieneNumeros = true
    } else {
        tieneSimbolos = true
    }
}

return {tieneLetras, tieneNumeros, tieneSimbolos, tamanioCadena}
}