import { NgxSpinnerService } from 'ngx-spinner';
import { MenuService } from 'src/app/app.menu.service';
import { defaultDateTime, RetornarRuta } from './../../misc/constants';
import { state } from '@angular/animations';
import { Router } from '@angular/router';
import { VentanillaModel } from './../../models/ventanilla_model';
import { ObjState } from './../../models/obj_status.model';

import { adminCatalogosModel } from './../../models/admin_catalogos';
import { filter } from 'rxjs/operators';
import { Socket } from 'ngx-socket-io';
import { Component, OnInit, AfterViewInit, ViewChild, OnDestroy, ChangeDetectorRef} from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { PrimeNGConfig, SelectItem } from 'primeng/api';
import { PrimeTemplate } from 'primeng/api';
import { PrimeIcons } from 'primeng/api';
import { AreasAtencion } from 'src/app/interfaces/areas.interface';
import { TicketModel } from 'src/app/models/tickets.model';
import { TicketParametro } from 'src/app/models/ticket_parametro.model';
import { UserService } from 'src/app/services/user.service';
import { VentanillaService } from 'src/app/services/ventanilla.service';
import { NotSubtractHoursDate, busquedaVeterano } from 'src/app/utils/constantes';
import { environment } from 'src/environments/environment';
import Swal from 'sweetalert2';
import { TicketAtenderModel } from 'src/app/models/ticket_atender.model';
import { DialogService, DynamicDialogRef } from "primeng/dynamicdialog";
import { ListaTicketComponent } from '../lista-ticket/lista-ticket.component';
import { VeteranoBuscarComponent } from '../veterano-buscar/veterano-buscar.component';
import { VeteranoBuscarService } from '../veterano-buscar/veterano-buscar.service';
import { RegVeteranosModel } from 'src/app/models/reg_veterano.model';
import { convertirFechaReal } from 'src/app/misc/constants';
import { SegUsuariosModel } from 'src/app/models/seg_user_model';
//import { socket } from 'src/app/utils/socket-constant';




@Component({
  selector: 'app-dashboard-atencion',
  templateUrl: './dashboard-atencion.component.html',
  styleUrls: ['./dashboard-atencion.component.scss'],
  providers: [DialogService],
})
export class DashboardAtencionComponent implements OnInit,OnDestroy ,AfterViewInit {
  
  @ViewChild(VeteranoBuscarComponent)  buscarVeterano   : VeteranoBuscarComponent;
  @ViewChild("drVentanillas", { static: false }) drVentanillas: any;

  constructor(
    public _spinner:NgxSpinnerService,
    private sanitizer: DomSanitizer,
    private _user: UserService,
    private _turnoSer: VentanillaService,
    public dialogService: DialogService,
    public _veterano:VeteranoBuscarService,
    private socket : Socket,
    private _router:Router,
    public _menu:MenuService,
    private changeDetection: ChangeDetectorRef
  ) { }

  
  

  escritorio: number = Number(localStorage.getItem("escritorio"))
  idArea: number = Number(localStorage.getItem("idArea"))
  idSucursal: number = Number(localStorage.getItem("idSucursal"))
  user:String=String(localStorage.getItem("user"));
  // urlRegistro =  environment.urlRegistro;
  urlRegistro: any = ''
  urlBecas: any = ''
  urlCreditos: any = ''
  urlPagos: any = ''
  display: boolean = false
  dataVentanillas: any[] = [];
  ventanillas: any[]
  ticketLlamada: TicketModel
  //Contador de llamadas
  contadorLlamada:number=3
  idUser=Number(localStorage.getItem("idUser"))

  //objeto de estado
  objState:ObjState=new ObjState();
  paramTickets: object = {
    "pendiente": true,
    "idArea": localStorage.getItem('IdArea')
  }

  //Modelo que servira para obtener los ticket que son pendientes
  ticketP: TicketParametro = new TicketParametro()
  //Modelo de asignacion para el ticket
  ticketAsignacionModel: TicketModel=new TicketModel();

  listaTicketsSinRespuesta: TicketModel[] = []

  //listado de ticket en cola
  listaTicketPendiente: TicketModel[] = []
  //listado de ticket en Gestion
  listaTicketGestion: TicketModel[] = []
  //modelo para utilizar ticket para atender
  ticketAtenderModel:TicketAtenderModel;
  //variable para ocultar el boton de finalizar
  esFinalizado:boolean;
  //variable para desplegar el componente en modal
  ref: DynamicDialogRef;
  //listado de catalogo de atencion para ticket
  listCatalogoAtencion:adminCatalogosModel[]=[]
  //listado para mostrar en el dropdown
  listItemAtencion:SelectItem[]=[]
  //objeto para seleccionar el dropDown
  itemAtencion:SelectItem;
  //variable de modal de ventanilla
  displayVentanilla:boolean=false;
  //variable para setear el modelo de ventanilla
  listVentanillaModel:VentanillaModel[]=[];
  //variable para agregar el modelo a un dropdown
  areaItem:SelectItem[]=[]
  //variable de seleccion para el modelo del dropdown
  selectAtencionItem:SelectItem;
  //variable del selectedButton
  ventanillaItem:SelectItem[]=[]
  //variable para seleccion del button
  selectedVentanillaItem:SelectItem;
  //variable para asignar el area seleccionada
  areaSelecionada:string=''
  //variable para asignar la ventanilla seleccionada
  ventanillaSeleccionada:string=''
  //variable para utilizar en el html la fecha por defecto
  defaultDate=defaultDateTime.toString()
  //variable de ruta para sanitizar la url
  ruta:any=this.sanitizer.bypassSecurityTrustResourceUrl('')
  //variable para invalidar ticket
  ticketInvalido:boolean=false;
  //variable para asignar el modelo de usuario sesion
  usuarioModel:SegUsuariosModel
  //variable bandera para mostrar el modal
  session:boolean=false;

  objAreaEnCola:any

  iframeOculto : boolean = true

  bloquearAtencion: boolean = false

  ngOnInit(): void {

    this.idArea = Number(localStorage.getItem("idArea"))
    if (this.idArea == null || this.idArea < 0) {
      this.bloquearAtencion = true
    }
    this.escritorio = Number(localStorage.getItem("escritorio"))
    this.areaSelecionada = localStorage.getItem("area") != null ? localStorage.getItem("area") : ''
    this.ventanillaSeleccionada = localStorage.getItem("ventanilla") != null ? localStorage.getItem("ventanilla") : ''
    this.obtenerTicketState();
    this.obtenerVentanilla();
    //this.idArea = 14070
    this.esFinalizado=false;
    //this.obtenerTicketGestion();
    this.cargarTurnosVentanillaSinRespuesta();
    this.obtenerAtencion();
    this.socket.connect();

   
    this.objAreaEnCola = { boton: true, idArea: this.idArea, idSucursal: this.idSucursal}
    this.verificarStatusBoton();
    this.verificarSesion();
  }

  ngOnDestroy(): void {
    this.socket.disconnect();
    if (this.ref) {
      this.ref.close();
    }
  }
  verificarSesion() {
    let usr=localStorage.getItem("user")
    this.session=false;
    this.socket.emit('sesionActiva',usr);
    this.socket.on(`estadoSesion${usr}`,(estadoSesion:any)=>{
        // console.log("ESTADOO SESION ****************");
        this.usuarioModel=estadoSesion.data[0];
        //console.log(this.usuarioModel);
        if(!this.usuarioModel.estadoSesion){
            
            if(this.session){
                return;
            }else{
                this.session=true;
            }
            
        }
        if(this.session){
            let _this=this;
            localStorage.clear();
            sessionStorage.clear();
            Swal.fire({
                // imageUrl:"https://c.tenor.com/FKCEOxoRpiIAAAAC/alf-tux.gif",
                icon:'info',
                allowOutsideClick:false,
                title:"Administrador de sesión",
                text:"Estimado "+usr+" se ha desactivo la sesión al sistema.",
                showConfirmButton:false,
                timer:4000,
                timerProgressBar:true
            })
            .then((r)=>{
                if(r.dismiss===Swal.DismissReason.timer){
                    location.reload();
                }
            })
           
        }

        
    })
    
    
    
  //  Swal.fire('Prueba','Conexion perdida','warning')
}

  ngAfterViewInit(): void {
   this.socket.connect();
   this.consultarTicketsER();
  }

  //esta habilitado el boton?
  verificarStatusBoton(){
    this.socket.on(`estadoBoton${this.idSucursal}`,(AreaEnCola)=>{
      // if(AreaEnCola.idArea == this.idArea && AreaEnCola.idSucursal == this.idSucursal){
      /**
       * Modificacion temporal para habilitar ventanilla para todas las areas
       */
      if(AreaEnCola.idSucursal == this.idSucursal){
        // console.log(AreaEnCola);
        this.habilitarLlamadaLocal = AreaEnCola.boton;
        setTimeout(() => {
          this.habilitarLlamadaLocal = false
        }, 6000);
      }
      
    })
  }

  convertirTicketFechaReal(ticket:TicketModel){
    // ticket.creadoEn=convertirFechaReal(ticket.creadoEn.toString());
    // ticket.modificadoEn=convertirFechaReal(ticket.modificadoEn.toString());
    // ticket.fechaInicio=convertirFechaReal(ticket.fechaInicio.toString());
    // ticket.fechaFin=convertirFechaReal(ticket.fechaFin.toString());
    return ticket;
  }

  //funcion guardar state para asignar el ticket en la apiState
  guardarTicketState(ticket:any){
    this.objState.date=new Date()
    this.objState.idUser=this.idUser
    this.objState.ticket=ticket
    this._turnoSer.guardarState(this.objState).subscribe((state)=>{
      // console.log(state);
    });
  }

  //metodo para eliminar un ticket cuando venga un resultado de -1
  eliminarTicket(){
    this.listaTicketGestion=[]
    
  }

  obtenerTicketState() {
    this._turnoSer.obtenerState(this.idUser).subscribe((ticket:any)=>{
      this.listaTicketGestion=[]
      if(ticket.states)
      {
        this.objState = ticket.states;
      }
      if(ticket.states != null && ticket.states.ticket != null && ticket.states.ticket.id != null){
        
        if(ticket.states.ticket.escritorio){
          this.ticketAsignacionModel=ticket.states.ticket;
          this.contadorLlamada=this.ticketAsignacionModel.numeroLlamada;
          if (ticket.states.ticket.idVeterano != null && ticket.states.ticket.idVeterano != '-1') {
            this._veterano.obtener(ticket.states.ticket.idVeterano).subscribe((veteranoData)=>
            {
              if(veteranoData.data[0] != null)
              {
                this.objState.veteranObj = veteranoData.data[0]
                this._turnoSer.guardarState(this.objState).subscribe((state)=>{
                  // console.log(state);
                });
              }
              console.log(veteranoData)
            })
          }
          
          this.listaTicketGestion.push(this.convertirTicketFechaReal(ticket.states.ticket));
          if(this.ticketAsignacionModel.estado=='Gestionando'){
            this.esFinalizado=true;

          }
          
        }else{
          this.ticketInvalido=true;
          //console.log("C");
          
        }
      }
    })
  }
  //metodo para cerrar la sesion de ventanilla
  cerrarSesion(){
    let _this=this;
    Swal.fire({
      title: 'Información',
      text: "¿Desea salir la sesión?",
      icon: 'info',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Salir',
      cancelButtonText: 'Cancelar'
    }).then((res)=>{
        if (res.isConfirmed) {
          let usr=localStorage.getItem("user")
          // let socketListener = _this.socket.hasListeners(`estadoSesion${usr}`);
          // if(socketListener){
          //     socket.disconnect()
          // }
          _this._user.logout(_this.user).subscribe((data:any)=>{});
          localStorage.clear();
          sessionStorage.clear();
          _this.idArea = 0;
          _this.escritorio = 0;
          
          location.reload();
          _this._router.navigateByUrl('/login')
        }
    })
  }
  //metodo para guardar el area y la ventanilla de atencion
  guardarAreayVentanilla(){
    let _this=this;
    Swal.fire({
      title: 'Información',
      text: "¿Desea guardar los cambios?",
      icon: 'info',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, adelante',
      cancelButtonText: 'Cancelar'
    }).then((res)=>{
        if(res.value){
          //localStorage.setItem("idArea",_this.selectAtencionItem.value);
          localStorage.setItem("escritorio", _this.selectedVentanillaItem.value);
          _this.displayVentanilla = false;
          _this.idArea = Number(localStorage.getItem("idArea"))
          _this.escritorio = Number(localStorage.getItem("escritorio"))
          Swal.fire('Información','El escritorio se ha guardado correctamente','success')
        }
      }
    )
  }
  //cuando se cambia el area para seleccionar el numero de ventanilla
  onChangeArea(area){
    this.ventanillaItem=[]
    if(area!=null){
      this.listVentanillaModel.forEach((v,i)=>{
        if(v.nombre_area==area){
          localStorage.setItem("area",v.nombre_area);
          this.areaSelecionada=localStorage.getItem("area");
          v.ventanillas.forEach((x,j)=>{
            this.ventanillaItem.push({label:x,value:j+1})
          })
          
        }
      })
      
    }
  }
  onchangeVentanilla(evt:any){
    this.ventanillaSeleccionada=evt;
    localStorage.setItem("ventanilla",evt);
    this.ventanillaSeleccionada=localStorage.getItem("ventanilla");
  }
  //metodo para obtener la ventanilla 
  obtenerVentanilla(opt = false) {
    this.ventanillaItem = []
    if (opt) {
      this.displayVentanilla=true;
      this._turnoSer.obtenerVentanilla({ventanilla:1}).subscribe((ventanilla) => {
          this.listVentanillaModel = ventanilla.data.find(x => x.idArea == Number(localStorage.getItem('idArea')))
          let aux = ventanilla.data.find(x => x.idArea == Number(localStorage.getItem('idArea')))
          //console.log(this.listVentanillaModel);
          aux.ventanillas.forEach((x,j)=>{
            this.ventanillaItem.push({label:x,value:j+1})
          })
          //console.log(this.areaItem);  
        })
    } else {
      if(this.escritorio == 0 && localStorage.getItem("escritorio") == null){
        Swal.fire('Información','Por favor seleccione el escritorio','info')
        this.displayVentanilla=true;
        this._turnoSer.obtenerVentanilla({ventanilla:1}).subscribe((ventanilla) => {
            this.listVentanillaModel = ventanilla.data.find(x => x.idArea == Number(localStorage.getItem('idArea')))
            let aux = ventanilla.data.find(x => x.idArea == Number(localStorage.getItem('idArea')))
            //console.log(this.listVentanillaModel);
            aux.ventanillas.forEach((x,j)=>{
              this.ventanillaItem.push({label:x,value:j+1})
            })
            //console.log(this.areaItem);  
          })
      } else {
        this.displayVentanilla=false;
      }
    }
  }

  //metodo para obtener la gestion
  // obtenerTicketGestion() {
  //   let listaTicket: TicketModel[] = []
  //   this.listaTicketGestion = []
    
  //   this._turnoSer.obtenerTurnos({idSucursal : this.idSucursal}).subscribe((tickets) => {
  //       listaTicket = tickets.data;
  //       let ticket = listaTicket.filter(x => x.escritorio == this.escritorio && x.idArea == this.idArea && x.estado == 'En Cola' && x.idSucursal == this.idSucursal)[0]
  //       if(ticket!=null){
  //         // console.log('******* TICKET A LLAMAR ********');
  //         // console.log(listaTicket);
  //         this.listaTicketGestion.push(this.convertirTicketFechaReal( ticket))
  //       }
  //     }
  //   )
  // }

  refrescar(e) {
    if (e.index > 0) {
      this.iframeOculto = false
      this._menu.menuObj.forEach((menu, i)=>{
        if (i == (e.index - 1)) {
          this.iframe(menu.urlOpt)
        }
      })
    } else {
      this.iframeOculto = true
    }
    
    // this.urlCreditos = this.sanitizer.bypassSecurityTrustResourceUrl(environment.urlCreditos)
  }

  iframe(ruta) {
    this.ruta = this.sanitizer.bypassSecurityTrustResourceUrl(ruta);
    localStorage.setItem("route", ruta);
    this._menu.refreshIframe(ruta)
  }

  cargarTurnosVentanillaSinRespuesta() {
    let ticketP : any = {}
    ticketP.pendiente = true;
    ticketP.idArea = this.idArea
    ticketP.idSucursal = this.idSucursal
    this._turnoSer.obtenerTurnos(ticketP).subscribe(resp => {
        //console.log(resp.data);
        if (resp.data != null && resp.data.length > 0) {
          this.listaTicketsSinRespuesta = resp.data
          this.listaTicketsSinRespuesta=this.listaTicketsSinRespuesta.filter(x=>x.idArea==this.idArea);
          this.listaTicketsSinRespuesta.forEach(ticket => {
            ticket.creadoEn = NotSubtractHoursDate(ticket.creadoEn.toString())
          });
        }
      },
      (err) => {
        console.log('Error:');
        console.log(err);
      }
    )
  }

  cargarVentanillasAreas() {
    this._turnoSer.cargarAreasVentanillas().subscribe(
      resp => {
        // console.log(resp);
        this.dataVentanillas = resp.data
        this.display = true
      },
      (err) => {
       // console.log('Error:');
       // console.log(err);
      }
    )
  }

  
  habilitarLlamadaLocal:boolean=false;
  //Metodo para llamar el siguiente de la cola
  llamarAlSiguiente() {

    this._spinner.show();
    if(this.listaTicketGestion.length==0){
      this.habilitarLlamadaLocal=false;
        //Verificamos que tengamos listado ticket en cola
        this.ticketP.pendiente = false;
        this.ticketP.idArea = this.idArea
        this.ticketP.idSucursal = this.idSucursal
        this._turnoSer.obtenerTurnos(this.ticketP).subscribe((tickets) => {

              if(tickets.data != null && tickets.data.length > 0){

                this.listaTicketPendiente = tickets.data;
                // this.listaTicketPendiente.forEach((t,i)=>{
                //   t.fechaInicio=convertirFechaReal(t.fechaInicio)
                //   t.fechaFin=convertirFechaReal(t.fechaFin)
                // })
              }else{
                this.listaTicketPendiente=[]
              }
              
              this._spinner.hide();
            }, (e) => {
              this._spinner.hide();

            }, () => {
             // this._spinner.hide();
              this.listaTicketPendiente=this.listaTicketPendiente.filter(x=>x.idArea==this.idArea);
              if (this.listaTicketPendiente.length == 0) {
                Swal.fire(
                  'Información',
                  'Ya no hay ticket(s) pendiente(s)',
                  'info'
                )
              } else {
                this.socket.emit("habilitarBoton", this.objAreaEnCola);
                //Asignamos el primero el ticket a la ventanilla
                this.ticketAsignacionModel = new TicketModel();
                this.ticketAsignacionModel.id = Number((this.listaTicketPendiente[0].id))
                this.ticketAsignacionModel.escritorio = this.escritorio
                this.ticketAsignacionModel.idArea = this.idArea
                this.ticketAsignacionModel.idTecnico = this.idUser
                this._turnoSer.asignarTicketVentanilla(this.ticketAsignacionModel).subscribe((ticket) => {
                      
                      this.ticketAsignacionModel =ticket.data[0];
                      if(this.ticketAsignacionModel!=null){
                        this.contadorLlamada=this.ticketAsignacionModel.numeroLlamada;
                        this.listaTicketGestion.push(this.convertirTicketFechaReal( this.ticketAsignacionModel));
                        // console.log(this.listaTicketGestion);
                        this.socket.emit("volverALlamar",this.ticketAsignacionModel)
                        //guardamos el ticket en la api state
                        //*** */
                        this.guardarTicketState(this.ticketAsignacionModel);

                      }
                    }, (e) => {

                    }, () => {
                      
                    }
                  )
              }
            }
          )
    }else{
      Swal.fire('Información','Estimado usuario, usted tiene un ticket en proceso','info');
      this._spinner.hide();
    }
    

  }

  habilitarLlamadaSiguiente:boolean=false;
  //metodo para volver a llamar
  volverALlamar(ticketLlamar:TicketModel) {
    this.habilitarLlamadaLocal=true;
    
    //vamos a ejecutar el metodo del componente de formulario buscar veterano
    let ticket = ticketLlamar;
    this.socket.emit("volverALlamar",ticket);
   
    this.socket.emit("habilitarBoton", this.objAreaEnCola);
    this._turnoSer.asignarTicketVentanilla(ticket).subscribe((ticket) => {
          //console.log(ticket);
          
          if(ticket.data!=null && ticket.data.length>0){
            
            this.ticketAsignacionModel = ticket.data[0];
           
            this.contadorLlamada=this.ticketAsignacionModel.numeroLlamada;
            if(Number(this.ticketAsignacionModel.numeroLlamada)==4 && this.ticketAsignacionModel.estado=='Sin Respuesta'){
              this.listaTicketGestion = []
              this.listaTicketsSinRespuesta = []
              this.guardarTicketState(null);
              
            }else{
              this.guardarTicketState(this.ticketAsignacionModel);
            }
          }
        }, (e) => {

        }, () => {
        //  this.listaTicketGestion.push(this.ticketAsignacionModel);
        let ticketP : any = {}
        ticketP.pendiente = true;
        ticketP.idArea = this.idArea
        ticketP.idSucursal = this.idSucursal
        this._turnoSer.obtenerTurnos(ticketP).subscribe((tickets)=> {
          if (tickets.data != null && tickets.data.length > 0) {
            tickets.data.forEach(ticke => {
              ticke.creadoEn = NotSubtractHoursDate(ticke.creadoEn.toString())
            })
            this.listaTicketsSinRespuesta =tickets.data.filter(x => x.idArea == this.idArea && x.idSucursal == this.idSucursal)
          }
        });
        setTimeout(() => {
          this.habilitarLlamadaLocal=false
        }, 6000);
        }

      )


  }
  //metodo para atender al veterano
  atenderTicket(idTicket:number,atender:boolean=false){
    
    let _this=this;
    
    if(atender==false){
      _this.esFinalizado=true
    }else{
      _this.esFinalizado=false
    }
    let pTicket=new TicketAtenderModel();
    pTicket.id=idTicket;
    pTicket.atendido=atender;
    _this._turnoSer.AtenderTicket(pTicket).subscribe(
      (data)=>{
        //guardamos el ticket con el nuevo estado en la apiState
       
        if(data.data!=null && data.data.length>0){
          let estado=data.data[0].estado;
          _this.ticketAsignacionModel.estado=estado;
          _this.ticketAsignacionModel.fechaInicio=convertirFechaReal(data.data[0].fechaInicio);
          _this.listaTicketGestion=[]
          _this.listaTicketGestion.push(this.convertirTicketFechaReal( _this.ticketAsignacionModel));
          if (!atender) {
            _this.guardarTicketState(_this.ticketAsignacionModel);
          }
          //funcion del servicio del veterano
          if (data.data[0].idVeterano != '-1') {
            busquedaVeterano.grupoFamiliar = true;
            busquedaVeterano.idVeterano = data.data[0].idVeterano;
            busquedaVeterano.busqueda = data.data[0].dui;
            _this._veterano.buscar(busquedaVeterano).subscribe((veteranoData)=>{
              if(veteranoData.data[0] != null)
              {
                this.objState.veteranObj = veteranoData.data[0]
                this._turnoSer.guardarState(this.objState).subscribe((state)=>{
                  // console.log(state);
                });
              }
              console.log(veteranoData)
            })
          }
        }
      },(e)=>{

      },()=>{
        if (!_this.esFinalizado) {
          Swal.fire({
            title: '¿Desea continuar?',
            text: "¿Desea finalizar el ticket?",
            icon: 'info',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, adelante',
            cancelButtonText: 'Aun no'
          }).then((r)=>{
            if(r.isConfirmed){
              //_this.obtenerTicketGestion();
              _this._veterano.regVeteranosModel=new RegVeteranosModel()
              _this._veterano.existeVeterano=false;
              _this.display=true;
              _this.guardarTicketState(null);
              _this.listaTicketGestion=[];
            }else{
              _this.esFinalizado=true
              _this.ticketAsignacionModel.estado='Gestionando'
            }
          })
        }
        
      }
    )
  }

  //Metodo para llamar a un ticket sin respuesta
  llamarSinRespuesta(ticket:TicketModel){
    this.contadorLlamada=3;
    ticket.escritorio=this.escritorio;
    ticket.idArea=this.idArea;
    
    this._turnoSer.asignarTicketVentanilla(ticket).subscribe((ticket)=>{
        //console.log(ticket);
        if (ticket.data != null && ticket.data.length > 0) {
          this.ticketAsignacionModel=ticket.data[0];
          this.guardarTicketState(this.ticketAsignacionModel)
          this.listaTicketGestion.push(this.convertirTicketFechaReal( this.ticketAsignacionModel))
          this.cargarTurnosVentanillaSinRespuesta();
        }
        
      }
    )
  }

  //mostrar componente de listado de ticket pendientes
  mostrarTicketPendiente(){
    let ticketP : any = {}
    ticketP.pendiente = false;
    ticketP.idArea = this.idArea
    ticketP.idSucursal = this.idSucursal
    this._turnoSer.obtenerTurnos(ticketP).subscribe((tickets)=>{
        if (tickets.data.length > 0){
          this.ref = this.dialogService.open(ListaTicketComponent, {
            data: {
              listado: tickets.data,
              moduloSeleccionado: 'modulo'
            },
            header: "Listado de ticket pendiente",
            width: "80%",
            contentStyle: { "max-height": "80vh", overflow: "auto" },
            baseZIndex: 10000,
            modal:true
            
          });
      
          this.ref.onClose.subscribe((bandera: boolean) => {
            //Lo que viene despues del cierre, la data
          });
        }else{
          Swal.fire(
            'Ticket?',
            'No hay ticket pendiente',
            'info'
          )
        }
      }
    )
  }

  //metodo para obtener el listado de atencion
  obtenerAtencion(){
    this._turnoSer.obtenerAtencion({tipo:"logAtencion"})
    .subscribe(
      (logAtencion)=>{
        if(logAtencion.data.length>0){
          this.listCatalogoAtencion=logAtencion.data;
          this.listCatalogoAtencion.forEach((v,i)=>{
            this.listItemAtencion.push({label:v.descripcion,value:v.id})
          })
        }
      }
    )
  }

  //Metodo que ocupa socket para actualizar la lista de tickets sin respuesta y con prioridad
  consultarTicketsER(){
    let sucursalInfo = {
      sucursal : this.idSucursal,
      area : this.idArea
    }
    this.socket.emit("consultarTicketsER", sucursalInfo );
    // this.socket.on(`ticketsER${this.idSucursal}${this.idArea}`, (ticketsER)=>{
      /**
       * Modificacion temporal para que ventanilla cubra todas las areas de atencion
       */
    this.socket.on(`ticketsER${this.idSucursal}`, (ticketsER)=>{
      /**
       * Modificacion temporal para que ventanilla cubra todas las areas de atencion
       */
      // this.changeDetection.detectChanges();
      let ticketsES = ticketsER.data;
      ticketsES = ticketsES.filter(ticketEspecial => ticketEspecial.idArea == this.idArea)
      if(ticketsES.length != this.listaTicketsSinRespuesta.length){
          this.listaTicketsSinRespuesta = ticketsES;
          this.changeDetection.detectChanges();
      }
    })
  }
}
