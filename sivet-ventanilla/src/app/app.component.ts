import { MenuService } from './app.menu.service';
import {Component, OnInit} from '@angular/core';
import { PrimeNGConfig } from 'primeng/api';
import { environment } from 'src/environments/environment';
import { PermisosService } from './services/permisos.service';
import { ApiStateService } from './services/api-state.service';
import { ENTORNO } from './utils/constantes';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
})
export class AppComponent implements OnInit{

    //topbarColor = 'layout-topbar-blue';
    topbarColor = 'layout-topbar-inabve';

    menuColor = 'layout-menu-light';

    themeColor = 'blue';

    layoutColor = 'blue';

    topbarSize = 'large';

    horizontal = true;

    inputStyle = 'outlined';

    ripple = true;

    compactMode = false;

    constructor(private primengConfig: PrimeNGConfig,
        public _menu:MenuService,
        public _permisos:PermisosService,
        private _apiState : ApiStateService) { }

    ngOnInit() {

        this.primengConfig.ripple = true;
        localStorage.setItem('entorno', ENTORNO);
        if (localStorage.getItem("token") && localStorage.getItem("token") != null) {
            //this._apiState.obtenerYGuardarState(ENTORNO)
            this._menu.getMenu().subscribe((menu)=>{
                this._menu.menuObj = []
                menu.data.forEach(option => {
                    this._menu.menuObj.push(
                        {
                            label: option.nombre,
                            icon: option.icono,
                            urlOpt: option.url,
                            descripcion: option.descripcion
                        }
                    )
                });
                 
                },error=>{}
            );
            // this._permisos.obtenerPermisos().subscribe(resp => {
            // }, (error) => {
            //     console.log("error al obtener los permisos ", error);
            // })
        }
    }
}
