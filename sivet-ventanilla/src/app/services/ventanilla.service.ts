import { TicketModel } from 'src/app/models/tickets.model';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { TicketAtenderModel } from '../models/ticket_atender.model';
//import { socket } from '../utils/socket-constant';



@Injectable({
  providedIn: 'root'
})
export class VentanillaService {

  constructor( private http : HttpClient) { }

  private _headers = new HttpHeaders({ 'content-type' : 'application/json' })
  //private _urlVentanilla : string = 'http://192.168.254.3:3001'


  public obtenerTurnos( params? : object ): Observable<any> { 
    return this.http.post<any>(environment.urlAPI + "/ventanillas/tickets/obtener",params , {'headers':this._headers})
    .pipe(
      map((resp: any) => { 
        return resp;
      }),
      catchError(err => {
        console.log(err)
          return err;
          })
      )
  }

  public cargarAreasVentanillas() : Observable<any>{
      return this.http.post(environment.urlAPI + "/administracion/configuraciones/obtenerVentanilla", {"id":"ventanillaCategoria"} ,{'headers': this._headers})
      .pipe(
          map((resp: any) => {
          if (resp) {
            // console.log(resp);
            return resp.data;
          } else {
            return null
          }
      }),
      catchError((err) => {
          return err ;
      })
    );
  }
  
  public cargarAreasAtencion() : Observable<any>{
      return this.http.post(environment.urlAPI + "/administracion/catalogos/obtener", {"tipo":"areasAtencion"} ,{'headers': this._headers})
      .pipe(
          map((resp: any) => {
          if (resp) {
            // console.log(resp);
            return resp.data;
          } else {
            return null
          }
      }),
      catchError((err) => {
          return err ;
      })
    );
  }

  public asignarTicketVentanilla( params : any) : Observable<any>{
    return this.http.post(environment.urlAPI + "/ventanillas/tickets/asignar", params ,{'headers': this._headers})
      .pipe(
        map((resp: any) => { 
            return resp;
        }),
        catchError(err => {
            console.log(err)  
            return err;
        })
    )
  }
  
  public AtenderTicket(p_ticket:TicketAtenderModel) : Observable<any>{
    return this.http.post(environment.urlAPI + "/ventanillas/tickets/atender", p_ticket ,{'headers': this._headers})
      .pipe(
          map((resp: any) => {          
          return resp;
      }),
      catchError((err) => {
          return err ;
      })
    );
  }

  public obtenerAtencion( params : any) : Observable<any>{
    return this.http.post(environment.urlAPI + "/administracion/catalogos/obtener", params ,{'headers': this._headers})
      .pipe(
        map((resp: any) => { 
            return resp;
        }),
        catchError(err => {
            console.log(err)  
            return err;
        })
    )
  }

  public guardarState(obj: any): Observable<any> {
    return this.http.post<any>(environment.urlAPI+'/states/create', obj, {
        'headers':this._headers}).pipe(
            map((resp: any) => { 
                return resp;
            }),
            catchError(err => {
                console.log(err)
                return err; 
            })
        )
    }

    public obtenerState(idUser: number): Observable<any> {
      return this.http.post<any>(environment.urlAPI+'/states/getByUser', {idUser : idUser}, {
          'headers':this._headers}).pipe(
              map((resp: any) => { 
                  return resp;
              }),
              catchError(err => {
                  console.log(err)
                  return err; 
              })
          )
      }

  public obtenerVentanilla(param:any):Observable<any>{
    return this.http.post<any>(environment.urlAPI+'/administracion/configuraciones/obtenerVentanilla',param,{
      'headers':this._headers
    }).pipe(
      map((resp:any)=>{
        return resp;
      }),
      catchError(err=>{
        return err;
      })
    )
  }



  /* 
  =================================================
  Funciones del socket
  =================================================
  */
    
//   conectarSocket(){
//     socket.connect();  
//     console.log(`Sockettt URL  ${socket}`);
//     //let socket = this.socket;
//     socket.on("connect", () => {
//       console.log(socket.id); //
//       console.log("Conectado al socket nuevo del cliente!!!!!!!"); //
//   });
// }


}
