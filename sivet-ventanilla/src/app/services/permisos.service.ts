import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { catchError, map } from 'rxjs/operators';
import { headers, MenuPermisosRegistro } from '../utils/constantes';
import { ResponseModel } from '../models/responseModel';

@Injectable({
  providedIn: 'root'
})
export class PermisosService {

  listaPermisos: any[] = []
  permisos: any = null 
  errorPermisos: boolean = false

  constructor(
    private _http   :    HttpClient,
  ) { }


  public obtenerPermisos(): Observable<ResponseModel> {
    return this._http.post<ResponseModel>(environment.urlAPI + '/seguridad/permisos/obtener', { "modulo" : MenuPermisosRegistro }, {'headers' : headers})
    .pipe(
          map((resp: ResponseModel) => {
            //console.log(resp);
              if (resp != null && resp.data.length > 0) {
                this.permisos = {}
                
                resp.data.forEach(permiso => {
                  this.permisos[permiso.opcionTransaccion] = true
                });
                this.errorPermisos = false
              } else {
                this.permisos = null
                this.errorPermisos = true
              }
              return this.permisos;
          }),
          catchError(err => {
              console.log(err)
              this.permisos = null
              this.errorPermisos = true
              return err;
          })
        )
  }

}
