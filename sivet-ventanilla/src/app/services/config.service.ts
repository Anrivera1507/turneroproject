import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {

  obj           : any
  carnet        : number
  registroType  : string
 
//   token = localStorage.getItem('token')

  headers = new HttpHeaders({ 'content-type' : 'application/json' })     
  urlAPI: string=environment.urlAPI;
//   headers2 = new HttpHeaders({ 'content-type' : 'text/csv' })         

  constructor(private http: HttpClient) { 
    
  } 
  // setCarnet(carnet: number) { console.log('???-',this.obj);
  // this.carnet = this.obj.carnet }
  getCarnet() { return this.obj }

  authentication(_user, _password) {
    console.log(_user, ' ', _password);
    
    return this.http.post(environment.urlAPI + "/auth/login", {"usuario": _user, "clave": _password},{'headers':this.headers})
      .pipe(
        map((resp: any) => {
          console.log(resp);
          if (resp.data.length) {
            localStorage.setItem('k1', '1');
            return resp.data;
          } else {
            return null
          }
        }),
        catchError((err) => {
          // swal('Error.', err.error.exception.Message);
          //console.log(err);
          
          return err // Observable.throw(err);
        })
      );
  }

  // public grupofamiliarGetAll(carnet: string): Observable<any> {
  //   return this._http.post<any>(this._config.urlAPI+'/vet_grupofamiliar/getAll',{ 'carnet': carnet }, {'headers':this._config.headers})
  //   .pipe(
  //           map((resp: any) => {
  //               return resp.data;
  //           }),
  //           catchError(err => {
  //               console.log(err)
  //               return err;
  //           })
  //       )
  // } 








}
