import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { ResponseModel } from '../models/responseModel';
import { ConfigService } from './config.service';

@Injectable({
  providedIn: 'root'
})
export class SimuladorService {  

  constructor(
    private _http   :    HttpClient,
    private _config :    ConfigService
  ) { }

  public obtenerConfigiracionCreditos(): Observable<ResponseModel> {
    return this._http.get<ResponseModel>(environment.urlAPI + '/creditos/planificador-configs/obtener', {'headers' : this._config.headers})
    .pipe(
          map((resp: ResponseModel) => {
              return resp.data;
          }),
          catchError(err => {
              console.log(err)
              return err;
          })
        )
  }
}
