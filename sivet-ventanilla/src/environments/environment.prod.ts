export const environment = {
  production: true,    
  urlAPI : "https://apis.inabve.gob.sv/v1/api_sivetweb",
  urlSocket : "https://socket.inabve.gob.sv",
  urlServer : "https://core.inabve.gob.sv",
  urlRegistro:"https://core.inabve.gob.sv/registro"
};
